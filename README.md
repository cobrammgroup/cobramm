[![SWH](https://archive.softwareheritage.org/badge/origin/https://gitlab.com/cobrammgroup/cobramm/)](https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/cobrammgroup/cobramm)


## About COBRAMM

COBRAMM is a computational chemistry program package interfacing widely known commercial & academic softwares for molecular modeling (spanning from electronic structure to molecular mechanics computations). It allows ground and excited state electronic structure computations within a combined quantum mechanical/molecular mechanical (QM/MM) framework to bridge the atomistic to the nano-scale. The user can perform all necessary steps to simulate chemical reactions in gas phase or complex environments (ranging from solvents to bio-polymers) and model the interaction of those systems with light to simulate their spectroscopy, as well as their photoinduced dynamics.

A detailed description of the QM/MM methodology and the capabilities of the code is available in the following publication:
O. Weingart, A. Nenov, P. Altoè, I. Rivalta, J. Segarra-Martí, I. Dokukina, M. Garavelli, COBRAMM 2.0 -- A software interface for tailoring molecular electronic structure calculations and running nanoscale (QM/MM) simulations, J. Mol. Model. 24, 271 (2018).



The code is provided under the GNU General Public License (Version 3, 29 June 2007).
A full text of the license can be found in the file LICENCE.

The documentation (work-in-progress) can be found in the [wiki page](https://gitlab.com/cobrammgroup/cobramm/-/wikis/home).

# Installation

## Requirements and third-party software

to install and run COBRAMM, the following software is mandatory:

*  a working [Gaussian](https://gaussian.com/) version, both Gaussian 16 and Gaussian 09 are supported at present
*  [Python](https://www.python.org/), version 3.x (3.6 and 3.7 have been both tested)

A full list of the required python modules can be found in the [installation guide](https://gitlab.com/cobrammgroup/cobramm/-/wikis/Installation-Guide)

To run QM/MM calculations, the Amber suite is also required:

*  [Amber](http://ambermd.org/), including both the molecular mechanics code and the force-fields

At present COBRAMM has been tested with Amber version 12 and 18.

To run molecular dynamics with intersystem crossing between states of different spin multiplicity using Gaussian for the electronic calculation, the pysoc code is also required:

*  [pysoc](https://github.com/gaox-qd/pysoc)


To install COBRAMM, get the latest version from the GitLab repository:

```
git clone https://gitlab.com/cobrammgroup/cobramm/
```

# Running the code

copy the [cobramm_profile](https://gitlab.com/cobrammgroup/cobramm/-/tree/master/cobramm_profile) file from the main directory of COBRAMM to 
the hidden file `.cobramm_profile` in your home directory.
This file will be sourced at the beginning of COBRAMM execution.
In this way, the environmental variable pointing to 
the third-party software are automatically defined when COBRAMM starts.

```
cd cobramm/
cp cobramm_profile ~/.cobramm_profile
```

edit `$HOME/.cobramm_profile` and define all the system-specific environmental variables.

please note that instead of providing the `$HOME/.cobramm_profile` file, 
the environment can be alternatively set by sourcing a configuration script with
the same variables of `cobramm_profile` before running COBRAMM.

`cobram.py`, the executable main file of COBRAMM, is available in the [cobramm/](https://gitlab.com/cobrammgroup/cobramm/-/tree/master/cobramm) directory.
The analysis tools are stored in the [util/](https://gitlab.com/cobrammgroup/cobramm/-/tree/master/util/) directory. 
  So we suggest to include these directories in the `PATH` environmental variable:

```
export COBRAM_PATH=/path/to/COBRAMM
export PATH=$COBRAM_PATH/cobramm:$COBRAM_PATH/util:$PATH
```

Please note that the definition of the COBRAM_PATH environmental variable **is mandatory** for running the analysis tool contained in the `$COBRAM_PATH/util` directory.

## COBRAMM test suite

The directory `$COBRAM_PATH/test` contains a collection of test cases that are 
intended to be used to test the normal execution of COBRAMM.

To test the correct installation of the code, please use
the run_tests script that is located in the root directory
of COBRAMM:

    cd $COBRAM_PATH  
    ./run_tests

Each test will be executed in a directory named `test/QMCODE_N`, where
`QMCODE` is the third-party software used for QM computation and `N` is 
an ordinal number labeling the test case.

The available tests are described in more detail in the 
test suite [README file](https://gitlab.com/cobrammgroup/cobramm/-/tree/master/test/README.md) 