#!/usr/bin/env python3
# coding=utf-8

#    COBRAMM
#    Copyright (c) 2019 ALMA MATER STUDIORUM - Università di Bologna

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#####################################################################################################

# import statements of module from python standard library

import re
import sys
import numpy as np # numpy library for scientific computation
import scipy.linalg as sp
import copy
import time
import math  # mathematical functions
import os # filesystem utilities
import shutil
import argparse
import pandas as pd
from sklearn.preprocessing import normalize
import logging


start_time = time.time()
# set it 'True' to activate time-steps prints
time_stamps = False

# hijack $PYTHONPATH (sys.path) to find cobramm local modules
try:
    sys.path.append(os.path.join(os.environ['COBRAM_PATH'], 'cobramm'))
except KeyError:
    raise RuntimeError('cannot import cobramm module.\n'
                       'Please check if $COBRAM_PATH is defined in your environment, if not run:\n'
                       'export COBRAM_PATH=/path/to/COBRAMM')

from orbitals import Orbitals, AtomicOrbital  # object to parse and store orbital information from QM output files

PTE = {"1": "H", "2": "He", "3": "Li", "4": "Be", "5": "B", "6": "C", "7": "N", "8": "O", "9": "F", "10": "Ne"}

# stores the number of atomic orbitals for each atom and for each basis set
# is filled in readAOfromBSE()
AOnumber = {}

# store args (and some other stuff) in a dictionary for easy callability
data = {}

# the basis in which the matrices are reordered ('NAT' is the default, but can be changed)
data['basis_order'] = 'NAT'

###########################################################################################

def main():

    # this new class defines a style for the parser message that join the two predefined styles
    class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter, argparse.RawDescriptionHelpFormatter):
        pass

###########################################################################################

    def readOccfromGaussianOutput(gauFileName):
 
        output = open(gauFileName, 'r')

        #extract MOs occupation
        MOoccupation = []
        
        for iline in output:
            if re.findall("occ.", iline):
                for i in range(4,len(iline.split())):
                    MOoccupation.append("O")
            if re.findall("virt.", iline):
                for i in range(4,len(iline.split())):
                    MOoccupation.append("V")

        return MOoccupation

###########################################################################################

    def initializeAtomicOrders(bs1, bs2):
        '''Fills the AO/MO order dictionaries but only for the basis sets 
        given in input. To be fair, the main purpose of converting this
        mega-initialization in a routine is so it is possible to fold the text =).'''

        ##-----Standard/NAT order for the Atomic Orbitals------
        '''
        [S] = spherical
        [C] = cartesian
        --1s--
        00
        1s

        --2s--
        00 01
        1s 2s

        --3s--
        00 01 02
        1s 2s 3s

        --2s.1p--
        00 01 02  03  04 
        1s 2s 2px 2py 2pz

        --3s.2p.1d [S]--
        00 01 02  03  04  05  06  07  08  09  10  11 12  13  
        1s 2s 2px 2py 2pz 3s  3px 3py 3pz d-2 d-1 d0 d+1 d+2

        --3s.2p.1d [C]--
        00 01 02  03  04  05  06  07  08  09 10 11 12 13 14 
        1s 2s 2px 2py 2pz 3s  3px 3py 3pz xx xy xz yy yz zz

        --4s.3p.1d [C]--
        00 01 02  03  04  05 06  07  08  09 10  11  12  13 14 15 16 17 18
        1s 2s 2px 2py 2pz 3s 3px 3py 3pz 4s 4px 4py 4pz xx xy xz yy yz zz 

        --4s.3p.2d.1f [C(d) C(f)]--
        00 01 02  03  04  05 06  07  08  09  10  11  12  13  14  15 16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34
        1s 2s 2px 2py 2pz 3s 3px 3py 3pz 3xx 3xy 3xz 3yy 3yz 3zz 4s 4px 4py 4pz 4xx 4xy 4xz 4yy 4yz 4zz xxx xxy xxz xyy xyz xzz yyy yyz yzz zzz
        '''

        ##----------------------------cc-pvdz---------------------------------------------
        # C, O, N atoms: 3s.2p.1d (15 [C], 14 [S])
        # H atoms: 2s.1p (5)

        #----NWCHEM----
        AOorder['NWC'].update({'cc-pvdz': {}})
        # NWC AO order (15): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx xy xz yy yz zz
        AOorder['NWC']['cc-pvdz']['15'] = "00 01 05 02 03 04 06 07 08 09 10 11 12 13 14"
        # NWC AO order (5): 1s 2s 2px 2py 2pz
        AOorder['NWC']['cc-pvdz']['5'] = "00 01 02 03 04"

        MOorder['NWC'].update({'cc-pvdz': {}})
        # NWC MO order (15): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3)
        MOorder['NWC']['cc-pvdz']['15'] = "00 01 05 02 03 04 06 07 08 09 12 14 10*sqrt(0.33333) 11*sqrt(0.33333) 13*sqrt(0.33333)"
        # NWC MO order (5):
        MOorder['NWC']['cc-pvdz']['5'] = "00 01 02 03 04"
        #--------

        #----MOLCAS----
        AOorder['MOL'].update({'cc-pvdz': {}})
        # MOLCAS AO order (14): 1s 2s 3s 2px 3px 2py 3py 2pz 3pz d-2 d-1 d0 d+1 d+2
        AOorder['MOL']['cc-pvdz']['14'] = "00 01 05 02 06 03 07 04 08 09 10 11 12 13"
        # MOLCAS AO order (15): 1s 2s 3s 2px 3px 2py 3py 2pz 3pz xx*sqrt(3) xy*sqrt(3) xz*sqrt(3) yy*sqrt(3) yz*sqrt(3) zz*sqrt(3)
        AOorder['MOL']['cc-pvdz']['15'] = "00 01 05 02 06 03 07 04 08 09*sqrt(3) 10*sqrt(3) 11*sqrt(3) 12*sqrt(3) 13*sqrt(3) 14*sqrt(3)"
        # MOLCAS AO order (5): 1s 2s 2px 2py 2pz
        AOorder['MOL']['cc-pvdz']['5'] = "00 01 02 03 04"

        MOorder['MOL'].update({'cc-pvdz': {}})
        # MOLCAS MO order (14): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        MOorder['MOL']['cc-pvdz']['14'] = "00 01 05 02 03 04 06 07 08 11 12 10 13 09"
        # MOLCAS MO order (15): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3)
        MOorder['MOL']['cc-pvdz']['15'] = "00 01 05 02 03 04 06 07 08 09 12 14 10*sqrt(0.33333) 11*sqrt(0.33333) 13*sqrt(0.33333)"
        # MOLCAS MO order (5): 1s 2s 2px 2py 2pz
        MOorder['MOL']['cc-pvdz']['5'] = "00 01 02 03 04"
        #--------

        #----BSE/GAUSSIAN----
        AOorder['BSE'].update({'cc-pvdz': {}})
        # BasisSetExchange AO order (14): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        AOorder['BSE']['cc-pvdz']['14'] = "00 01 05 02 03 04 06 07 08 11 12 10 13 09"
        # BasisSetExchange AO order (15): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx yy zz xy*sqrt(3) xz*sqrt(3) yz*sqrt(3)
        AOorder['BSE']['cc-pvdz']['15'] = "00 01 05 02 03 04 06 07 08 09 12 14 10*sqrt(3) 11*sqrt(3) 13*sqrt(3)"
        # BasisSetExchange AO order (5): 1s 2s 2px 2py 2pz
        AOorder['BSE']['cc-pvdz']['5'] = "00 01 02 03 04"

        MOorder['GAU'].update({'cc-pvdz': {}})
        # GAU MO order (14): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz d0 d+1 d-1 d+2 d-2 
        MOorder['GAU']['cc-pvdz']['14'] = "00 01 05 02 03 04 06 07 08 11 12 10 13 09"
        # GAU MO order (15): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3) 
        MOorder['GAU']['cc-pvdz']['15'] = "00 01 05 02 03 04 06 07 08 09 12 14 10*sqrt(0.33333) 11*sqrt(0.33333) 13*sqrt(0.33333)"
        # GAU MO order (5): 1s 2s 2px 2py 2pz
        MOorder['GAU']['cc-pvdz']['5'] = "00 01 02 03 04"
        #--------

        #----Natural AO/MO order----
        AOorder['NAT'].update({'cc-pvdz': {}})
        atom_numbers = (5, 14, 15)
        for an in atom_numbers:
            s = ''
            for num in range(an):
                s += '{:02.0f} '.format(num)
            # string from 00 to 04/13/14
            AOorder['NAT']['cc-pvdz']['{}'.format(an)] = s
        MOorder['NAT'] = copy.deepcopy(AOorder['NAT'])
        #--------
        
        '''
        ##----------------------------cc-pvtz---------------------------------------------
        # !! AO orders are done, but there are present (but slightly negligible numerical errors in comparing AO matrices)
        # C, O, N atoms: 4s.3p.2d.1f (35 [C,C])
        # H atoms: 3s.2p.1d (15 [C])

        # GAU MO order: 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3) 
        # MOorder['GAU']['cc-pvtz']['35'] = ""
        # MOorder['GAU']['cc-pvtz']['15'] = ""

        # MOLCAS AO order (C,N,O): 1s 2s 3s 4s 2px 3px 4px 2py 3py 4py 2pz 3pz 4pz 3xx 4xx 3xy 4xy 3xz 4xz 3yy 4yy 3yz 4yz 3zz 4zz \
        #   xxx xxy xxz xyy xyz xzz yyy yyz yzz zzz    
        # MOLCAS AO order (H): 1s 2s 3s 2px 3px 2py 3py 2pz 3pz xx*sqrt(3) xy*sqrt(3) xz*sqrt(3) yy*sqrt(3) yz*sqrt(3) zz*sqrt(3)
        AOorder['MOL']['cc-pvtz']['35'] = "00 01 05 15 02 06 16 03 07 17 04 08 18 09*sqrt(3) 19*sqrt(3) 10*sqrt(3) 20*sqrt(3) 11*sqrt(3) 21*sqrt(3) 12*sqrt(3) 22*sqrt(3) 13*sqrt(3) 23*sqrt(3) 14*sqrt(3) 24*sqrt(3) 25*sqrt(15) 26*sqrt(3) 27*sqrt(3) 28*sqrt(3) 29*sqrt(3) 30*sqrt(3) 31*sqrt(15) 32*sqrt(3) 33*sqrt(3) 34*sqrt(15)"
        AOorder['MOL']['cc-pvtz']['15'] = "00 01 05 02 06 03 07 04 08 09*sqrt(3) 10*sqrt(3) 11*sqrt(3) 12*sqrt(3) 13*sqrt(3) 14*sqrt(3)"
        # MOLCAS MO order: 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3)
        # MOorder['MOL']['cc-pvtz']['35'] = ""
        # MOorder['MOL']['cc-pvtz']['15'] = ""

        # BasisSetExchange AO order (C,N,O): 1s 2s 3s 4s 2px 2py 2pz 3px 3py 3pz 4px 4py 4pz 3xx 3yy 3zz 3xy 3xz 3yz 4xx 4yy 4zz 4xy 4xz 4yz \
        #   xxx yyy zzz xyy xxy xxz xzz yzz yyz xyz
        AOorder['BSE']['cc-pvtz']['35'] = "00 01 05 15 02 03 04 06 07 08 16 17 18 09 12 14 10*sqrt(3) 11*sqrt(3) 13*sqrt(3) 19 22 24 20*sqrt(3) 21*sqrt(3) 23*sqrt(3) 25 31 34 28 26 27 30 33 32 29*sqrt(3)"
        # BasisSetExchange AO order (H): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx yy zz xy*sqrt(3) xz*sqrt(3) yz*sqrt(3)
        AOorder['BSE']['cc-pvtz']['15'] = "00 01 05 02 03 04 06 07 08 09 12 14 10*sqrt(3) 11*sqrt(3) 13*sqrt(3)"

        # Natural AO/MO order
        atom_numbers = (15, 35)
        for an in atom_numbers:
            s = ''
            for num in range(an):
                s += '{:02.0f} '.format(num)
            # string from 00 to 14/34
            AOorder['NAT']['cc-pvtz']['{}'.format(an)] = s
        #--------
        '''
        '''
        ##-----------------------------!!!6-311G*!!!----------------------------------------
        # !!! Waiting until I can launch OM calculation with it !!!
        # C, O, N atoms: 4s.3p.1d (19 [C])
        # H atoms: 3s (3)

        # GAU order (19): 1s 2s 2px 2py 2pz 3s 3px 3py 3pz 4s 4px 4py 4pz xx yy zz xy*sqrt(3) xz*sqrt(3) yz*sqrt(3)
        AOorder['BSE']['6-311g*']['19'] = "00 01 02 03 04 05 06 07 08 09 10 11 12 13 16 18 14*sqrt(3) 15*sqrt(3) 17*sqrt(3)"
        AOorder['BSE']['6-311g*']['3'] = "00 01 02"

        # Natural AO/MO order
        AOorder['NAT']['6-311g*']['19'] = "00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18"
        AOorder['NAT']['6-311g*']['3'] = "00 01 02"
        #--------
        '''

        ##-----------------------------6-31G*---------------------------------------
        # C, O, N atoms: 3s.2p.1d (15 [C], 14 [S])
        # H atoms: 2s (2)

        #----NWChem----
        MOorder['NWC'].update({'6-31g*': {}})
        # NWC MO order (15): 1s 2s 2px 2py 2pz 3s 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3)
        MOorder['NWC']['6-31g*']['15'] = "00 01 02 03 04 05 06 07 08 09 12 14 10*sqrt(0.33333) 11*sqrt(0.33333) 13*sqrt(0.33333)"
        # NWC MO order (5):
        MOorder['NWC']['6-31g*']['2'] = "00 01"
        #--------

        #----MOLCAS----
        AOorder['MOL'].update({'6-31g*': {}})
        # MOLCAS AO order (14): 1s 2s 3s 2px 3px 2py 3py 2pz 3pz d-2 d-1 d0 d+1 d+2
        AOorder['MOL']['6-31g*']['14'] = "00 01 05 02 06 03 07 04 08 09 10 11 12 13"
        # MOLCAS AO order (15): 1s 2s 3s 2px 3px 2py 3py 2pz 3pz xx*sqrt(3) xy*sqrt(3) xz*sqrt(3) yy*sqrt(3) yz*sqrt(3) zz*sqrt(3) 
        AOorder['MOL']['6-31g*']['15'] = "00 01 05 02 06 03 07 04 08 09*sqrt(3) 10*sqrt(3) 11*sqrt(3) 12*sqrt(3) 13*sqrt(3) 14*sqrt(3)"
        # MOLCAS AO order (2): 1s 2s
        AOorder['MOL']['6-31g*']['2'] = "00 01"

        MOorder['MOL'].update({'6-31g*': {}})
        # MOLCAS MO order (15): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        MOorder['MOL']['6-31g*']['14'] = "00 01 05 02 03 04 06 07 08 11 12 10 13 09"
        # MOLCAS MO order (15): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3)
        MOorder['MOL']['6-31g*']['15'] = "00 01 05 02 03 04 06 07 08 09 12 14 10*sqrt(0.33333) 11*sqrt(0.33333) 13*sqrt(0.33333)"
        # MOLCAS MO order (2): 1s 2s
        MOorder['MOL']['6-31g*']['2'] = "00 01"
        #--------

        #----BSE/GAUSSIAN----
        AOorder['BSE'].update({'6-31g*': {}})
        # BasisSetExchange AO order (14): 1s 2s 2px 2py 2pz 3s 3px 3py 3pz d0 d+1 d-1 d+2 d-2 
        AOorder['BSE']['6-31g*']['14'] = "00 01 02 03 04 05 06 07 08 11 12 10 13 09"
        # BasisSetExchange AO order (15): 1s 2s 2px 2py 2pz 3s 3px 3py 3pz xx yy zz xy*sqrt(3) xz*sqrt(3) yz*sqrt(3)
        AOorder['BSE']['6-31g*']['15'] = "00 01 02 03 04 05 06 07 08 09 12 14 10*sqrt(3) 11*sqrt(3) 13*sqrt(3)"
        # BasisSetExchange AO order (2): 1s 2s 
        AOorder['BSE']['6-31g*']['2'] = "00 01"

        MOorder['GAU'].update({'6-31g*': {}})
        # GAU MO order (14): 1s 2s 2px 2py 2pz 3s 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        MOorder['GAU']['6-31g*']['14'] = "00 01 02 03 04 05 06 07 08 11 12 10 13 09"
        # GAU MO order (15): 1s 2s 2px 2py 2pz 3s 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3)
        MOorder['GAU']['6-31g*']['15'] = "00 01 02 03 04 05 06 07 08 09 12 14 10*sqrt(0.33333) 11*sqrt(0.33333) 13*sqrt(0.33333)"
        # GAU MO order (2): 1s 2s 
        MOorder['GAU']['6-31g*']['2'] = "00 01"
        #--------

        #----Natural AO/MO order----
        AOorder['NAT'].update({'6-31g*': {}})
        atom_numbers = (2, 14, 15)
        for an in atom_numbers:
            s = ''
            for num in range(an):
                s += '{:02.0f} '.format(num)
            # string from 00 to 01/13/14
            AOorder['NAT']['6-31g*']['{}'.format(an)] = s
        MOorder['NAT'] = copy.deepcopy(AOorder['NAT'])
        #--------


        ##-----------------------------6-31G**---------------------------------------
        # C, O, N atoms: 3s.2p.1d (15 [C], 14 [S])
        # H atoms: 2s.1p (5)

        #---NWCHEM----
        MOorder['NWC'].update({'6-31g**': {}})
        # NWC MO order (15): 1s 2s 2px 2py 2pz 3s 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3)
        MOorder['NWC']['6-31g**']['15'] = "00 01 02 03 04 05 06 07 08 09 12 14 10*sqrt(0.33333) 11*sqrt(0.33333) 13*sqrt(0.33333)"
        # NWC MO order (5): 1s 2s 2px 2py 2pz
        MOorder['NWC']['6-31g**']['5'] = '00 01 02 03 04'
        #--------

        #----MOLCAS----
        AOorder['MOL'].update({'6-31g**': {}})
        # MOLCAS AO order (14): 1s 2s 3s 2px 3px 2py 3py 2pz 3pz d-2 d-1 d0 d+1 d+2
        AOorder['MOL']['6-31g**']['14'] = "00 01 05 02 06 03 07 04 08 09 10 11 12 13"
        # # MOLCAS AO order (15): 1s 2s 3s 2px 3px 2py 3py 2pz 3pz xx*sqrt(3) xy*sqrt(3) xz*sqrt(3) yy*sqrt(3) yz*sqrt(3) zz*sqrt(3) 
        AOorder['MOL']['6-31g**']['15'] = "00 01 05 02 06 03 07 04 08 09*sqrt(3) 10*sqrt(3) 11*sqrt(3) 12*sqrt(3) 13*sqrt(3) 14*sqrt(3)"
        # # MOLCAS AO order (5): 1s 2s 2px 2py 2pz
        AOorder['MOL']['6-31g**']['5'] = '00 01 02 03 04'

        MOorder['MOL'].update({'6-31g**': {}})
        # MOLCAS MO order (14): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        MOorder['MOL']['6-31g**']['14'] = "00 01 05 02 03 04 06 07 08 11 12 10 13 09"
        # MOLCAS MO order (15): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3)
        MOorder['MOL']['6-31g**']['15'] = "00 01 05 02 03 04 06 07 08 09 12 14 10*sqrt(0.33333) 11*sqrt(0.33333) 13*sqrt(0.33333)"
        # MOLCAS MO order (5): 1s 2s 2px 2py 2pz
        MOorder['MOL']['6-31g**']['5'] = '00 01 02 03 04'
        #--------

        #----BSE/GAUSSIAN----
        AOorder['BSE'].update({'6-31g**': {}})
        # BasisSetExchange AO order (14): 1s 2s 2px 2py 2pz 3s 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        AOorder['BSE']['6-31g**']['14'] = "00 01 02 03 04 05 06 07 08 11 12 10 13 09"
        # BasisSetExchange AO order (15): 1s 2s 2px 2py 2pz 3s 3px 3py 3pz xx yy zz xy*sqrt(3) xz*sqrt(3) yz*sqrt(3)
        AOorder['BSE']['6-31g**']['15'] = "00 01 02 03 04 05 06 07 08 09 12 14 10*sqrt(3) 11*sqrt(3) 13*sqrt(3)"
        # BasisSetExchange AO order (5): 1s 2s 2px 2py 2pz
        AOorder['BSE']['6-31g**']['5'] = '00 01 02 03 04'

        MOorder['GAU'].update({'6-31g**': {}})
        # GAU MO order (14): 1s 2s 2px 2py 2pz 3s 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        MOorder['GAU']['6-31g**']['14'] = "00 01 02 03 04 05 06 07 08 11 12 10 13 09"
        # GAU MO order (15): 1s 2s 2px 2py 2pz 3s 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3)
        MOorder['GAU']['6-31g**']['15'] = "00 01 02 03 04 05 06 07 08 09 12 14 10*sqrt(0.33333) 11*sqrt(0.33333) 13*sqrt(0.33333)"
        # GAU MO order (5): 1s 2s 3s 2px 2py 2pz
        MOorder['GAU']['6-31g**']['5'] = '00 01 02 03 04'
        #--------

        #----Natural AO/MO order----
        AOorder['NAT'].update({'6-31g**': {}})
        atom_numbers = (5, 14, 15)
        for an in atom_numbers:
            s = ''
            for num in range(an):
                s += '{:02.0f} '.format(num)
            # string from 00 to 04/13/14
            AOorder['NAT']['6-31g**']['{}'.format(an)] = s
        MOorder['NAT'] = copy.deepcopy(AOorder['NAT'])
        #--------

        '''
        ##-----------------------------6-31G--------------------------------------------
        # C, O, N atoms: 3s.2p (9)
        # H atoms: 2s (2)

        # GAU AO order: 1s 2s 2px 2py 2pz 3s 3px 3py 3pz 
        # AOorder['GAU']['6-31g']['9'] = "00 01 02 03 04 05 06 07 08"
        # AOorder['GAU']['6-31g']['2'] = "00 01"
        # GAU MO order: 1s 2s 2px 2py 2pz 3s 3px 3py 3pz 
        MOorder['GAU']['6-31g']['9'] = "00 01 02 03 04 05 06 07 08"
        MOorder['GAU']['6-31g']['2'] = "00 01"

        # NWC AO order: 1s 2s 2px 2py 2pz 3s 3px 3py 3pz 
        # AOorder['NWC']['6-31g']['9'] = "00 01 02 03 04 05 06 07 08"
        # AOorder['NWC']['6-31g']['2'] = "00 01"
        # NWC MO order: 1s 2s 2px 2py 2pz 3s 3px 3py 3pz 
        MOorder['NWC']['6-31g']['9'] = "00 01 02 03 04 05 06 07 08"
        MOorder['NWC']['6-31g']['2'] = "00 01"

        # MOLCAS AO order : 1s 2s 3s 2px 3px 2py 3py 2pz 3pz 
        # AOorder['MOL']['6-31g']['9'] = "00 01 05 02 06 03 07 04 08"
        # AOorder['MOL']['6-31g']['2'] = "00 01"
        # MOLCAS MO order : 1s 2s 3s 2px 2py 2pz 3px 3py 3pz 
        MOorder['MOL']['6-31g']['9'] = "00 01 05 02 03 04 06 07 08"
        MOorder['MOL']['6-31g']['2'] = "00 01"

        # BasisSetExchange AO order: 1s 2s 2px 2py 2pz 3s 3px 3py 3pz 
        AOorder['BSE']['6-31g']['9'] = "00 01 02 03 04 05 06 07 08" 
        AOorder['BSE']['6-31g']['2'] = "00 01"

        # Natural AO/MO order
        AOorder['NAT']['6-31g']['9'] = "00 01 02 03 04 05 06 07 08"
        AOorder['NAT']['6-31g']['2'] = "00 01"
        #--------
        '''

        ##-----------------------------ANO-R--------------------------------------------
        # C, O, N atoms: 3s.2p.1d (14 [S])
        # H atoms: 2s.1p (5)

        #----MOLCAS----
        AOorder['MOL'].update({'ano-r_3s2p1d0f': {}, 'ano-r_2s1p0d0f': {}})
        # MOLCAS AO order (14): 1s 2s 3s 2px 3px 2py 3py 2pz 3pz d-2 d-1 d0 d+1 d+2
        AOorder['MOL']['ano-r_3s2p1d0f']['14'] = '00 01 05 02 06 03 07 04 08 09 10 11 12 13'
        # MOLCAS AO order (5): 1s 2s 2px 2py 2pz
        AOorder['MOL']['ano-r_2s1p0d0f']['5'] = '00 01 02 03 04'

        MOorder['MOL'].update({'ano-r_3s2p1d0f': {}, 'ano-r_2s1p0d0f': {}})
        # MOLCAS MO order (14): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        MOorder['MOL']['ano-r_3s2p1d0f']['14'] = "00 01 05 02 03 04 06 07 08 11 12 10 13 09"
        # MOLCAS MO order (5): 1s 2s 2px 2py 2pz
        MOorder['MOL']['ano-r_2s1p0d0f']['5'] = "00 01 02 03 04"
        #--------

        #----BSE/GAUSSIAN----
        AOorder['BSE'].update({'ano-r_3s2p1d0f': {}, 'ano-r_2s1p0d0f': {}})
        # BasisSetExchange AO order (14): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        AOorder['BSE']['ano-r_3s2p1d0f']['14'] = "00 01 05 02 03 04 06 07 08 11 12 10 13 09" 
        # BasisSetExchange AO order (5): 1s 2s 2px 2py 2pz 
        AOorder['BSE']['ano-r_2s1p0d0f']['5'] = "00 01 02 03 04"

        MOorder['GAU'].update({'ano-r_3s2p1d0f': {}, 'ano-r_2s1p0d0f': {}})
        # GAU MO order (14): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        MOorder['GAU']['ano-r_3s2p1d0f']['14'] = "00 01 05 02 03 04 06 07 08 11 12 10 13 09"
        # GAU MO order (5): 1s 2s 2px 2py 2pz
        MOorder['GAU']['ano-r_2s1p0d0f']['5'] = "00 01 02 03 04"
        #--------

        #----Natural AO/MO order----
        AOorder['NAT'].update({'ano-r_3s2p1d0f': {}, 'ano-r_2s1p0d0f': {}})
        atom_numbers = (5, 14)
        for an in atom_numbers:
            s = ''
            for num in range(an):
                s += '{:02.0f} '.format(num)
            # string from 00 to 04
            if an == 5:
                AOorder['NAT']['ano-r_2s1p0d0f']['{}'.format(an)] = s
            # string from 00 to 13
            elif an == 14:
                AOorder['NAT']['ano-r_3s2p1d0f']['{}'.format(an)] = s
        MOorder['NAT'] = copy.deepcopy(AOorder['NAT'])
        #--------


        ##-----------------------------ANO-L--------------------------------------------
        # C, O, N atoms: 3s.2p.1d (15 [C], 14 [S])
        # H atoms: 2s.1p (5)

        #----MOLCAS----
        AOorder['MOL'].update({'ano-l_3s2p1d0f': {}, 'ano-l_2s1p0d0f': {}})
        # Molcas AO order (14): 1s 2s 3s 2px 3px 2py 3py 2pz 3pz d-2 d-1 d0 d+1 d+2 
        AOorder['MOL']['ano-l_3s2p1d0f']['14'] = "00 01 05 02 06 03 07 04 08 09 10 11 12 13" 
        # Molcas AO order (15): 1s 2s 3s 2px 3px 2py 3py 2pz 3pz xx*sqrt(3) xy*sqrt(3) xz*sqrt(3) yy*sqrt(3) yz*sqrt(3) zz*sqrt(3) 
        AOorder['MOL']['ano-l_3s2p1d0f']['15'] = "00 01 05 02 06 03 07 04 08 09*sqrt(3) 10*sqrt(3) 11*sqrt(3) 12*sqrt(3) 13*sqrt(3) 14*sqrt(3)" 
        # MOLCAS AO order (5): 1s 2s 2px 2py 2pz
        AOorder['MOL']['ano-l_2s1p0d0f']['5'] = "00 01 02 03 04"

        MOorder['MOL'].update({'ano-l_3s2p1d0f': {}, 'ano-l_2s1p0d0f': {}})
        # MOLCAS MO order (14): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        MOorder['MOL']['ano-l_3s2p1d0f']['14'] = "00 01 05 02 03 04 06 07 08 11 12 10 13 09"
        # MOLCAS MO order (15): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3)
        MOorder['MOL']['ano-l_3s2p1d0f']['15'] = "00 01 05 02 03 04 06 07 08 09 12 14 10*sqrt(0.33333) 11*sqrt(0.33333) 13*sqrt(0.33333)"
        # MOLCAS MO order (5): 1s 2s 2px 2py 2pz
        MOorder['MOL']['ano-l_2s1p0d0f']['5'] = "00 01 02 03 04"
        #--------

        #----BSE/GAUSSIAN----
        AOorder['BSE'].update({'ano-l_3s2p1d0f': {}, 'ano-l_2s1p0d0f': {}})
        # BasisSetExchange AO order (14): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        AOorder['BSE']['ano-l_3s2p1d0f']['14'] = "00 01 05 02 03 04 06 07 08 11 12 10 13 09"
        # BasisSetExchange AO order (15): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx yy zz xy*sqrt(3) xz*sqrt(3) yz*sqrt(3)
        AOorder['BSE']['ano-l_3s2p1d0f']['15'] = "00 01 05 02 03 04 06 07 08 09 12 14 10*sqrt(3) 11*sqrt(3) 13*sqrt(3)"
        # BasisSetExchange AO order (5): 1s 2s 2px 2py 2pz 
        AOorder['BSE']['ano-l_2s1p0d0f']['5'] = "00 01 02 03 04"

        MOorder['GAU'].update({'ano-l_3s2p1d0f': {}, 'ano-l_2s1p0d0f': {}})
        # GAU MO order (14): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz d0 d+1 d-1 d+2 d-2
        MOorder['GAU']['ano-l_3s2p1d0f']['14'] = "00 01 05 02 03 04 06 07 08 11 12 10 13 09"
        # GAU MO order (15): 1s 2s 3s 2px 2py 2pz 3px 3py 3pz xx yy zz xy*sqrt(1/3) xz*sqrt(1/3) yz*sqrt(1/3)
        MOorder['GAU']['ano-l_3s2p1d0f']['15'] = "00 01 05 02 03 04 06 07 08 09 12 14 10*sqrt(0.33333) 11*sqrt(0.33333) 13*sqrt(0.33333)"
        # GAU MO order (5): 1s 2s 2px 2py 2pz
        MOorder['GAU']['ano-l_2s1p0d0f']['5'] = "00 01 02 03 04" 
        #-------- 

        #----Natural AO/MO order----
        AOorder['NAT'].update({'ano-l_3s2p1d0f': {}, 'ano-l_2s1p0d0f': {}})
        atom_numbers = (5, 14, 15)
        for an in atom_numbers:
            s = ''
            for num in range(an):
                s += '{:02.0f} '.format(num)
            # string from 00 to 04
            if an == 5:
                AOorder['NAT']['ano-l_2s1p0d0f']['{}'.format(an)] = s
            # string from 00 to 13
            elif an == 14:
                AOorder['NAT']['ano-l_3s2p1d0f']['{}'.format(an)] = s
            # string from 00 to 14
            elif an == 15:
                AOorder['NAT']['ano-l_3s2p1d0f']['{}'.format(an)] = s   
        MOorder['NAT'] = copy.deepcopy(AOorder['NAT'])   
        #--------  
        
###########################################################################################

    def changeBasisReferenceOrder(bs_list, new_basis_order):
        '''In case one wants a different standard than 'NAT' for AO/MO orders, this is required
        to transform all the orders (written in the 'NAT' basis) into the new one.
        It's mainly used as a developer tool, since there is no option to change the standard 
        from the input.'''

        # transformation in case a different reference order is requested (now updated only for AOorder)
        if new_basis_order != 'NAT':
            # list of orders to transform (new-reference order must the LAST)
            orders = ['BSE','{}'.format(new_basis_order)]
            # list of basis sets to apply the transformation on
            bs_to_rotate =  bs_list
            for bs in bs_to_rotate:    
                for order in orders:
                    # repeat for each n. of atomic orbitals
                    for nAO in AOorder['NAT'][bs]:
                        # loop over orders in 'NAT' basis, find the same elements and store the index at which
                        # the element in the order you are using as reference is found
                        newOrder = ''
                        # loop over order to rotate
                        for i in range(len(AOorder[order][bs][nAO].split())):
                            # loop over new reference order to find the correspondent element
                            for j in range(len(AOorder[new_basis_order][bs][nAO].split())):
                                elemRef = AOorder[new_basis_order][bs][str(nAO)].split()[j]
                                elem = AOorder[order][bs][str(nAO)].split()[i]
                                # check correspondence
                                if elem[:2] == elemRef[:2]:
                                    # add to the string representing the new order the index of the element found in the new-reference order (in NAT base)
                                    newOrder += '{:02.0f}'.format(int(j))
                                    # eventual scaling factor within sqrt (ASSUMES WRITTEN AS SQRT(..) ALWAYS)
                                    coeffTop, coeffBottom, scale = 1, 1, 1
                                    if elemRef[2:] != "":
                                        coeffBottom = float(elemRef[8:-1])
                                    if elem[2:] != "":
                                        coeffTop = float(elem[8:-1])
                                    scale = coeffTop / coeffBottom
                                    if scale != 1:
                                        newOrder += '*sqrt({:.5f})'.format(scale)
                                    newOrder += ' '
                        # update the order in dictionary
                        AOorder[order][bs][nAO] = newOrder

###########################################################################################

    def readOccfromNWChemOutput(nwcFileName):                     
        
        with open(nwcFileName) as f:
            output = f.read()

        # reads the total n. of Ao functions (basis set size)
        re_string = "Number of AO functions : (.*) *[0-9]*"
        find_orb = re.findall(re_string, output)
        ntotorb = int((find_orb[0].split())[-1])

        # initialize MOs occupation list
        MOoccupation = []

        # identifies occupied orbitals
        occtxt = "Occ=2.0"
        # identifies virtual orbitals
        virtxt = "Occ=0.0"

        # count occupied orbitals
        occlist = re.findall(occtxt, output)
        # count virtual orbitals
        virlist = re.findall(virtxt, output)
        
        # error check if the number of occ+virt orbitals does not match
        if len(occlist) + len(virlist) > ntotorb and (len(occlist) + len(virlist)) % ntotorb == 0 and (len(occlist) + len(virlist)) / ntotorb == 2:
            occlist = occlist[0:int(len(occlist)/2)]
            virlist = virlist[0:int(len(virlist)/2)]
        elif len(occlist) + len(virlist) == ntotorb:
            pass
        else:
            print("Length of occ+virt != basis set length!")
            quit()

        # read the occupation lists and adds the appropriate amount of 'O' and 'V'
        for orb in range(len(occlist)):
            MOoccupation.append("O")
        for orb in range(len(virlist)):
            MOoccupation.append("V")

        #the output is literally a list with a sequence of 'O's, then 'V's
        return MOoccupation

###########################################################################################

    def readOccfromMolcasOutput(omFileName):

        with open(omFileName) as f:
            output = f.read()

        occuplist = re.findall("Occup= *[0-9].[0-9]*", output)

        MOoccupation = []
        for item in occuplist:

            # 1.3 is arbitrarily chosen as threshold to separate occupied and virtual orbitals
            if float(item.split()[1]) > 1.300:
                MOoccupation.append("O")
            else:
                MOoccupation.append("V")

        return MOoccupation

###########################################################################################

    def readCISfromGaussianOutput(nwcFileName, MOoccup, totstate, extra_roots=False, nextra = 0):
        with open(nwcFileName) as f:
            out = f.read()

        occMO = MOoccup.count("O")
        virtMO = MOoccup.count("V")

        output = {"cis": {}}
        ref_coeff = {}
        # fill the whole matrix with zeros
        for nocc in range(1, occMO + 1):
            for nvirt in range(1, virtMO + 1):
                ref_coeff[(nocc, occMO + nvirt)] = 0.0000
        
        stateRegExp = "Excited State *([0-9]*): *Singlet\-\?Sym *(-?[0-9]*\.[0-9]*) eV *(-?[0-9]*\.[0-9]*) nm *f=(-?[0-9]*\.[0-9]*) *<S\*\*2>=([0-9]*\.[0-9]*)\n([\s\S]*?)\n ?\n"
        excitationRegExp = "([0-9]*) *-> *([0-9]*) *(-?[0-9]*\.[0-9]*)"

        for nstate, eV, nm, f, S, text in re.findall(stateRegExp, out):

            # copy the empty CI matrix at every state
            coeffs_exp = copy.deepcopy(ref_coeff)

            for nocc, nvirt, coeff in re.findall(excitationRegExp, text):
                try:
                    coeffs_exp[(int(nocc), int(nvirt))] = np.sqrt(2)*float(coeff)
                except:
                    pass
            output["cis"][int(nstate)] = coeffs_exp

        # for extra RAS roots, initialize a matrix filled with zeros
        if extra_roots:
            for nstate in range(totstate + 1, totstate + nextra + 1):
                coeffs_exp = {}
                for nocc in range(1, occMO + 1):
                    for nvirt in range(1, virtMO + 1):
                        coeffs_exp[(nocc, nvirt + occMO)] = 0.0000
                        output["cis"][nstate] = coeffs_exp

        return output

###########################################################################################

    def readCISfromNWChemOutput(nwcFileName, MOoccup, totstate, extra_roots=False, nextra = 0):
        with open(nwcFileName) as f:
            out = f.read()

        occMO = MOoccup.count("O")
        virtMO = MOoccup.count("V")

        output = {'cis':{}}

        coeffRegExp = "Ground state a" + "(.*)" + "Task  times"
        coeffExprText = re.findall(coeffRegExp, out, re.DOTALL | re.IGNORECASE)[0]
        ciLines = coeffExprText.rstrip().lstrip().splitlines()
        
        # start from the first excited state
        nstate = 1
        
        for iLine in range(len(ciLines)):
            
            coeffs_exp={}

            # act only if it finds 'Root'
            if ciLines[iLine].find('Root ') != -1:

                # the line at which the CI expansion starts (10 lines after the 'Root' line)
                for jLine in range(iLine+10, len(ciLines)):
                    # keep going until it finds '----' or an empty line
                    if ciLines[jLine].find('--------') != -1 or ciLines[jLine] == '' or ciLines[jLine]==' ' :
                        break
                    # actual execution block
                    else:
                        # between columns 45 and 55 is where the coeff. are stored
                        coeff = ciLines[jLine][45:53]
                        # 11:14 is the occ.Orb coefficient; 32:35 is the virt.Orb one
                        coeffs_exp[(int(ciLines[jLine][11:14]), int(ciLines[jLine][32:35]))] = float(coeff)
                
                output["cis"][nstate] = coeffs_exp

                nstate += 1
                if nstate > totstate:
                    break
                        
        # for extra RAS roots, initialize a matrix filled with zeros
        if extra_roots:
            for nstate in range(totstate + 1, totstate+nextra + 1):
                coeffs_exp = {}
                for nocc in range(1, occMO + 1):
                    for nvirt in range(1, virtMO + 1):
                        coeffs_exp[(nocc, nvirt + occMO)] = 0.0000
                        output["cis"][nstate] = coeffs_exp

        return output

###########################################################################################

    def readCISfromMolcasOutput(MolFileName, MOoccup, totstate, active_el, activeorb, method):

        with open(MolFileName) as f:
            out = f.read()

        occMO = MOoccup.count("O")
        virtMO = MOoccup.count("V")
        
        # the occ/virt values may be forced since the occ/virt threshold is not very good to separate them
        #occMO = 27; virtMO = 118

        output = {"cis": {}}

        # total number of single excitations
        csfexp = "Number of CSFs *[0-9]*"
        tot_csf = int(((re.findall(csfexp, out)[0]).split())[-1])

        if method == "ras":
            # read from first excited state
            regexp = "printout of CI-coefficients larger than  0.00 for root  2" + \
                 "(.*)" + "Natural orbitals and occupation numbers for root  1"
            # excitations starts after 3 lines from the searched string
            linea = 3
        # FAILS IF THERE IS NO RASSI MODULE (probably not anymore)
        elif method == "xms":
            regexp = "The CI coefficients for the MIXED state nr.   2"+\
                "(.*)" + 'Happy landing!'
            linea = 7

        regExprText = re.findall(regexp, out, re.DOTALL | re.IGNORECASE)[0]
        cisLines = regExprText.rstrip().lstrip().splitlines()

        # occupied orbitals of the active space
        occactive = int(active_el / 2)
        # virtual orbitals of the active space
        virtactive = activeorb - occactive

        # string to search for singly-excited configuration 
        CSFstring=occactive*'2'+virtactive*'0'

        for nstate in range(1, totstate + 1):
            
            coeffs_exp = {}

            # extract coeff for excitation within the AS
            for nocc in range(0, occactive):
                
                # put a 'u' at the 'nocc' position 
                istring = CSFstring[0:nocc]+"u"+CSFstring[nocc+1:]

                for nvirt in range(0, virtactive):
                    
                    # put a 'd' at the 'nvirt'+occactive position 
                    jstring = istring[0:occactive+nvirt]+"d"+istring[occactive+nvirt+1:]

                    regExprText = [s for s in cisLines[linea:linea+tot_csf] if jstring in s][0]

                    if method == "ras":
                        coeff = regExprText.rstrip().lstrip().split()[2]
                    elif method == "xms":
                        coeff = regExprText.rstrip().lstrip().split()[6]
                    
                    # basically when 'nocc' is even
                    # I don't know why though
                    if ((occactive-1) - nocc) % 2 == 1:
                        coeffs_exp[(occMO - occactive + nocc + 1, occMO + nvirt + 1)] = -float(coeff)
                    else:
                        coeffs_exp[(occMO - occactive + nocc + 1, occMO + nvirt + 1)] = float(coeff)
            
            output["cis"][nstate] = coeffs_exp    
            if method == "ras":
                #goes to the CI coeffs of the next state
                linea += tot_csf + 4
            elif method == "xms":
                linea += tot_csf + 8
                
        return output

###########################################################################################

    def readAOoverlapfromBSE(file, basis_set_1, basis_set_2, cross_geom = False, file_2 = None):
        '''Reads basis set info from the BasisSetExchange utility and computes the AO
        matrix, which is then reorderd accordingly to the 'NAT' reference order.'''
        
        # store molcas molden file(s)
        orbfile = []

        # store geometry section(s)
        geomLines = []

        # count the n.of files to read (often 1)
        files = 1
        if cross_geom:
            files = 2

        # open file to read geom from
        with open(file) as f:
            orbfile.append(f.read())

        # open second molden file to read second geometry from
        if cross_geom:    
            with open(file_2) as f_2:
                orbfile.append(f_2.read())  

        # store basis set full expressions
        bsInput = []
        bsInput.append(basis_set_1.lower())
        bsInput.append(basis_set_2.lower())

        if time_stamps:
            end_time = time.time() - start_time
            print('\nAO matrix routine start: {} seconds\n'.format(end_time))

        # get number of atoms from molden file
        atomExpr = '\[N_Atoms\]'+'(.*)'+'\[Atoms\]'
        nAtoms = int(re.findall(atomExpr, orbfile[0], re.DOTALL | re.IGNORECASE)[0].split()[0])
        
        # extremes of the geometry section
        geomExpr = '\[Atoms\] \(AU\)' + '(.*)' + '\[Charge\]'

        geomExprText = re.findall(geomExpr, orbfile[0], re.DOTALL | re.IGNORECASE)[0]
        geomLines.append(geomExprText.rstrip().lstrip().splitlines())

        # second geometry if requested
        if cross_geom:
            geomExprText = re.findall(geomExpr, orbfile[1], re.DOTALL | re.IGNORECASE)[0]
            geomLines.append(geomExprText.rstrip().lstrip().splitlines())

        # build atom list and real_layers file as a string
        atoms = []; RL = ['']*files

        # loop over file count
        for fil in range(files):
            RL[fil] = ''
            for atom in range(nAtoms):
                # atom
                Atom = geomLines[fil][atom].split()[0][0]
                if fil == 0:
                    atoms.append(Atom)

                # extend real_layers.xyz
                # atom
                RL[fil] += '{:2} 0'.format(Atom)
                # X coord
                RL[fil] += '{:>14}'.format('{:.8f}'.format(0.52917721092*float(geomLines[fil][atom].split()[3])))
                # Y coord
                RL[fil] += '{:>14}'.format('{:.8f}'.format(0.52917721092*float(geomLines[fil][atom].split()[4])))
                # Z coorfil
                RL[fil] += '{:>14}'.format('{:.8f}'.format(0.52917721092*float(geomLines[fil][atom].split()[5])))
                # newline
                RL[fil] += '   H\n'

        # store coordinates and atoms in a real_layers.xyz file (to read for AO matrix)
        with open('real_layers.xyz','w') as fRL:
            fRL.write(RL[0])

        # separate two real_layers files and remove the single one
        if cross_geom:
            os.remove('real_layers.xyz')

            with open('real_layers_1.xyz','w') as fRL:
                fRL.write(RL[0])

            with open('real_layers_2.xyz','w') as fRL_2:
                fRL_2.write(RL[1])

        if time_stamps:
            end_time = time.time() - start_time
            print('\nreal_layers.xyz written: {} seconds\n'.format(end_time))

        # remove duplicates from the atom list
        atoms_short = list(dict.fromkeys(atoms))

        # process the full basis set expression (in case of ANO basis sets) to craft the proper key for 
        # the AO/MO order dictonaries for each atom, which specifies its contraption
        # loop over both basis sets (relevant only for ANO)
        for bs in bsInput:
            # check if bs is of the ANO family
            if bs[0:4] == 'ano-':
                
                # basis set name: ano-l / ano-r ...
                bs_general = bs.split('_')[0]

                # loop over unique atoms
                for atom in atoms_short:

                    OrderKey[atom] = {}
                    pattern = r'{}\.\d+s\d+p\d+d\d+f'.format(atom.lower())
                    # capture the reduced expression of the atom considered
                    basisText = re.findall(pattern, bs, re.DOTALL)[0] 
                    # cut the atom from the string
                    bs_contraction = basisText.split('.')[-1]

                    # build the index
                    key = bs_general + '_' + bs_contraction
                    OrderKey[atom][bs] = key

        # atomic specifications (like n. of basis functs) for each atom and each bs
        atomOrb = []; index = 0
        for bs in bsInput:

            # trick to make the routine read the appropriate geometry each time in case of two different ones
            if cross_geom:
                shutil.copyfile('real_layers_{}.xyz'.format(index+1),'real_layers.xyz')
                index += 1

            # construct orbitals
            fakegauOrb = Orbitals.readAOfromString(bs)

            # store the number of atomic orbitals associated with each element for each basis set
            AOnumber.update({bs: {}})
            fakeatomOrb = fakegauOrb.atomicOrbitals
            atomOrb.append(fakeatomOrb)
            # loop over atoms
            for idAtom in range(1,len(atoms)+1):
                atom = '{}'.format(atoms[idAtom-1])
                totAO=0
                for b in fakegauOrb.__dict__['AOshells']:
                    if b.__dict__['idAtom'] == idAtom:
                        totAO += b.__dict__['nFunctions']
                if atom not in AOnumber[bs]:
                    AOnumber[bs][atom] = totAO


        # ---** Compute atomic overlap matrix **---
        overlapMatrix = np.zeros([len(atomOrb[0]),len(atomOrb[1])])
        for ileft in range(len(atomOrb[0])):
            for iright in range(len(atomOrb[1])):
                overlap = AtomicOrbital.AOoverlap(atomOrb[0][ileft], atomOrb[1][iright])
                if abs(overlap) > 10**(-6):
                    overlapMatrix[ileft, iright] = float('{:.6f}'.format(overlap))

        if time_stamps:
            end_time = time.time() - start_time
            print('\nAO matrix computed: {} seconds\n'.format(end_time)) 

        calctypeRef=data['basis_order']; calctype = 'BSE'
        AOovlp = np.array(overlapMatrix); bf = 0

        # rotates the AO matrix in the chosen reference order
        for rot in range(2):
            
            bs = bsInput[rot]
            bskey = bs

            for norb in range(0, len(atomOrb[rot])):

                temp = np.zeros(len(AOovlp[norb]))
                # save values temporarily for the reorder        
                temp_arr = np.array(AOovlp[norb])

                for atom in atoms:
                    # number of AO of the atom
                    nAO = AOnumber[bs][atom]

                    # update bskey for ANO basis sets
                    if bs[0:4] == 'ano-':
                        bskey = OrderKey[atom][bs]

                    # run over the atomic orbitals for the atom (5, 15, ..)
                    for ao in range(nAO):
                        # considers eventual scaling factors from one order to another
                        coeffTop, coeffBottom = 1,1   

                        indexRef = AOorder[calctypeRef][bskey][str(nAO)].split()[ao]
                        index = AOorder[calctype][bskey][str(nAO)].split()[ao]

                        if indexRef[2:] != "":
                            coeffTop = float(indexRef[8:-1])
                        if index[2:] != "":
                            coeffBottom = float(index[8:-1])
                        scale = coeffTop / coeffBottom

                        temp[bf + int(index[:2])] = \
                        temp_arr[bf + int(indexRef[:2])]*np.sqrt(scale)
                    
                    bf += AOnumber[bs][atom]

                temp_t = tuple(temp)
                AOovlp[norb] = np.array(temp_t)
                bf = 0

            AOovlp = np.transpose(AOovlp)
        
        if time_stamps:
            end_time = time.time() - start_time
            print('\nAO matrix rotated: {} seconds\n'.format(end_time)) 

        return AOovlp

###########################################################################################

    def readfromfchk(file, occupation):

        totorb = len(occupation)

        with open(file) as f:
            orbfile = f.read()

        MOexp = "MO coefficients" + "(.*)" + "Orthonormal basis"
        regExprText = re.findall(MOexp, orbfile, re.DOTALL | re.IGNORECASE)[0]
        moLines = regExprText.rstrip().lstrip().splitlines()

        molOrb, read_orb = [], []
        linea = 1
        count = 0
        countMO = 1
        countAO = 1
            
        while linea < len(moLines):
            tmp = moLines[linea].split()
            for i in range(len(tmp)):

                read_orb.append(float(tmp[i]))
                countAO += 1

                if (countAO > totorb):
                    countAO=1
                    countMO+=1

                if len(read_orb) == totorb:
                    molOrb.append(read_orb)
                    read_orb = []
            linea += 1

        MO = np.array(molOrb)

        return molOrb

###########################################################################################

    def readfrommolden(file, occupation):

        totorb = len(occupation)

        with open(file) as f:
            orbfile = f.read()

        # matching string
        MOexp = "Sym=" + "(.*)"
        regExprText = re.findall(MOexp, orbfile, re.DOTALL | re.IGNORECASE)[0]
        moLines = regExprText.rstrip().lstrip().splitlines()

        molOrb, read_orb = [], []
        # the coefficients are 4 lines after the matching string
        linea = 4
        while linea < len(moLines):
            for norb in range(totorb):
                read_orb.append(float(moLines[linea][9:]))
                linea += 1
            # skip to the next set of MO coefficients
            linea += 4
            # store the read molecular orbital
            molOrb.append(read_orb)
            read_orb = []

        return molOrb

###########################################################################################

    def rotateOrbs(file, molOrb, occupation, basis_set, calctype, compType):

        totorb = len(occupation)
    
        bs = basis_set.lower()

        with open(file) as f:
            orbfile = f.read()

        ## SUBSTITUTE THIS WITH READING REAL_LAYERS.XYZ

        # list of atoms
        atomlist = []

        # read real_layers.xyz file printed by the AO routine
        with open('real_layers.xyz') as fl:
            out_rl = fl.read().splitlines()

        # extract atoms from real_layers.xyz
        for line in out_rl:
            atomlist.append(line.split()[0])

        # bskey is relevant only when dealing with ANO basis sets
        bskey = bs

        bf = 0

        # the order read from the calculation and the reference one
        calctype = calctype[:3].upper(); calcRef = data['basis_order']

        # rotation of MO coefficients
        for norb in range(0, totorb):
            
            temp = molOrb[norb]
            # save values temporary for the reorder
            temp_arr = np.array(molOrb[norb])

            # loop over atoms
            for atom in atomlist:

                # number of atomic orbitals (basis functions) for each atom 
                nAO = str(AOnumber[bs][atom])

                # for ANO basis sets, the MO order index is gotten from the dictionary 
                # initialized in readAOfromBSE()
                if bs[0:4] == 'ano-':
                    bskey = OrderKey[atom][bs]

                # run over the atomic orbitals component for the atom
                for aoc in range(0,int(nAO)):
                    # track any coefficient difference and therefore scaling
                    coeffTop, coeffBottom = 1,1

                    indexRef = MOorder[calcRef][bskey][nAO].split()[aoc]
                    index = MOorder[calctype][bskey][nAO].split()[aoc]

                    if indexRef[2:] != "":
                        coeffTop = float(indexRef[8:-1])
                    if index[2:] != "":
                        coeffBottom = float(index[8:-1])
                    scale = coeffTop / coeffBottom

                    temp[bf + int(index[:2])] = \
                    temp_arr[bf + int(indexRef[:2])]*np.sqrt(scale)

                bf += AOnumber[bs][atom]

            temp_t = tuple(temp)
            molOrb[norb] = temp_t
            bf = 0

        MO=np.array(molOrb)

        return molOrb

###########################################################################################

    def GS_Orthogonalization(MOvirt2, newMO2):

        '''given two sets of MO (one transformed, short, and one original, bigger). It uses
        the Gram-Schmidt orthogonalization to find the remaining MOs from the original set
        after subtracting the projection with the new rotated set
        The output is a MO matrix of the same size of the original, bigger set'''
        
        '''In our case the rotated set is composed of a rotated part (new virtuals) and a fixed one (occupied)'''


        # loop until the matrix is back to the proper size
        while len(newMO2[:,0]) < len(MOvirt2[0,:]):
            
            # select the MO of the original set (MOvirt2) with the lowest overlap to the set of new MOs (newMO2)
            ovMin = 10000; moMin = 0

            # run over the original MOs 
            for mo in range(len(MOvirt2[:,0])):

                ovlp = 0; orbs = len(newMO2[:,0])
                # compute overlap from one of the old MOs with the total new MO set
                for orb in range(orbs):
                    ovlp += np.dot(MOvirt2[mo]/np.linalg.norm(MOvirt2[mo]),newMO2[orb]/np.linalg.norm(newMO2[orb]))
                # if we reach a new overlap min, register that orbital
                if ovlp < ovMin:
                    ovMin = ovlp 
                    moMin = mo


            # the original MO with the lowest overlap
            iMO = MOvirt2[moMin]

            # subtract iteratively the projection of the chosen MO with each one of the new MOs from it 
            for j in range(orbs):
                proj = np.dot(newMO2[j]/np.linalg.norm(newMO2[j]),iMO/np.linalg.norm(iMO))
                iMO -= proj*newMO2[j]

            # the residual is the new orthogonal vector; gets normalized
            norm = np.linalg.norm(iMO)
            randvec = iMO/norm

            # i guess it stops if the overlap is too low
            if norm < 0.005:
                continue

            # add new vector to new MO matrix
            newMO2 = np.append(newMO2, [randvec], axis = 0)

        return newMO2

############################################################################

    def computeOverlap(MOoccup1, MOoccup2, AOovlp, MOcoeffs1, MOcoeffs2, CI1, CI2, nroots, compare):

        # define function that finds max in array and its index(es)
        def find_max_and_indexes(arr):

            max_value = max(arr)
            indexes = [i for i, v in enumerate(arr) if v == max_value]
            
            return max_value, indexes

        # CI matrices
        states1 = copy.deepcopy(CI1.get("cis"))
        states2 = copy.deepcopy(CI2.get("cis"))
        
        # sets of MO
        MO1 = np.array(MOcoeffs1)
        MO2 = np.array(MOcoeffs2)

        # AO matrix
        AOovlp = np.array(AOovlp)

        ## the calculation of MO overlap matrix
        MOoverlap = np.matmul(MO1, np.matmul(AOovlp, MO2.T))

        #initiates the final matrix
        psioverlap = np.zeros((nroots+1, nroots+1))
        psinorm = np.zeros((nroots+1, nroots+1))

        # Occup.2 is the one that rotates 
        # Occup.1 (molcas) has some inconsistencies when orbital mix:
        # occupation < 1.5 makes a orbital virtual when it's not
        occMO = MOoccup2.count("O")
        virtMO1 = len(MOoccup1)-occMO 
        virtMO2 = MOoccup2.count("V")


        print("Largest off-diagonal element per orbital")

        # MOoverlap[0:occMO, occMO:end] -- top right
        for irow in range(occMO):
            maxel = max(np.absolute(MOoverlap[irow, occMO:]))
            if maxel > 0.02:
                print("MO ", irow + 1, "| MO ", occMO + 1 + np.absolute(MOoverlap[irow, occMO:]).tolist().index(maxel) ,": %5.3f" %maxel)
        print()

        # MOoverlap[occMO:end, 0:occMO] -- bottom left
        for irow in range(occMO, len(MOoverlap[:,0])):
            maxel = max(np.absolute(MOoverlap[irow, 0:occMO]))
            if maxel > 0.02:
                print("MO ", irow + 1, "| MO ", 1 + np.absolute(MOoverlap[irow, 0:occMO]).tolist().index(maxel), ": %5.3f" %maxel)

        # split MOoverlap in two submatrices (actually neglecting out-of-diagonal blocks)
        t_OccS = copy.deepcopy(MOoverlap[0:occMO, 0:occMO])
        t_VirtS = copy.deepcopy(MOoverlap[occMO:, occMO:])

        # Loewdin orthogonalization of Occ,Occ block
        #(S * S^T)
        SST=np.matmul(t_OccS,t_OccS.T)
        #(S * S^T)^(-1/2)
        halfinv_SST=np.linalg.inv(sp.sqrtm(SST))
        #S_o,o^-1  = B = S^T * (S * S^T)^(-1/2)
        transOccS=np.matmul(t_OccS.T,halfinv_SST)

        # SVD for the virtual orbitals
        U, Ddiag, V = np.linalg.svd(t_VirtS)
        # normalization achieved by rounding to 1
        Ddiag = np.round(Ddiag)

        # create a matrix with diagonal values = 1
        D = np.zeros((len(t_VirtS[:,0]),len(t_VirtS[0,:])))
        np.fill_diagonal(D,Ddiag)
        # pseudoinverse
        transVirtS = np.matmul(V.T,np.matmul(D.T,U.T)) 
     

        # obtain MO overlap matrix of MO1 and transformed MO2 (for double-checking)
        # in case of different basis sets, MOoverlap changes dimensions, thus create a new one
        newMOoverlap = np.zeros((occMO+len(transVirtS[0,:]),occMO+len(transVirtS[0,:])))
        
        newMOoverlap[0:occMO, 0:occMO] = np.matmul(MOoverlap[0:occMO, 0:occMO],transOccS)
        newMOoverlap[occMO:, occMO:] = np.matmul(MOoverlap[occMO:, occMO:],transVirtS)

        if compare in ["OMtoNW", "NWtoOM", "OMtoGA", "GAtoOM"]:
            print("\n--------Molcas RAS(PT2) CI vectors--------")
        elif compare in ["NWtoGA", "GAtoNW"]:
            print("\n--------NWChem CIS/TDA CI vectors--------")
        elif compare in ['OMtoOM2', 'OM2toOM']:
            print("\n--------First calculation - Molcas RAS(PT2) CI vectors--------")


        ###----------------------------------- CI kept fixed ---------------------------------------###

        CISarray1 = []

        #i is the state number, state is the actual matrix of coeffs (occOrb,VirtOrb):coeff
        for i, state in states1.items():

            # initialize as a rectangular matrix, Occupied(rows)xVirtual(cols)
            CISarray = np.zeros((occMO, virtMO1))

            # ex is the excitation (occ,virt), coeff is the CI coefficient
            # ex[0] is the occ.orb; ex[1] is the virt.orb
            for ex, coeff in state.items():
                # both -1 are needed to scale the orb numbers with the matrix indexes
                # it accounts for the fact that the matrix it's reading from is indexed with the occ. and virt. orbital number
                CISarray[ex[0] - 1][ex[1] - occMO - 1] = coeff
            
            CISarray1.append(CISarray)

            # print, for each state, its composition of excitations
            print("S_{} ".format(i))

            weight = 0
            for irow in range(occMO):
                for jcol in range(virtMO1):
                    weight += CISarray1[i-1][irow][jcol]**2
                    # if the coefficient is high enough, it's printed with the arrival and starting orbitals

                    if abs(CISarray1[i-1][irow][jcol]**2) > 0.03:
                        print(irow + 1, "->",occMO + jcol + 1, ": %10.8f" %CISarray1[i-1][irow][jcol])
            if weight < 0.5:
                print("WARNING! Small residual weight %5.2f\n" %weight)
            else:
                print("Residual weight %5.2f\n" %weight)

        if compare in ["OMtoNW", "NWtoOM"]:
            print("\n--------NWChem CIS/TDA CI vectors--------")
        elif compare in ["NWtoGA", "GAtoNW", "OMtoGA", "GAtoOM"]:
            print("\nGaussian CIS/TDA CI vectors")
        elif compare in ['OMtoOM2', 'OM2toOM']:
            print("\n--------Second calculation - Molcas RAS(PT2) CI vectors--------")


        ###------------------------------------------------ CI rotated ----------------------------------------------###
        CISarray2 = []
        for i, state in states2.items():
            CISarray = np.zeros((occMO, virtMO2))
            for ex, coeff in state.items():
                CISarray[ex[0] - 1][ex[1] - occMO - 1] = coeff
            
            # apply transformation matrices from left and right
            # notably, only these coefficients are transformed
            CISarray2.append(np.matmul(transOccS.T, np.matmul(CISarray, transVirtS)))

            print("S_{} ".format(i))
            weight = 0
            for irow in range(occMO):
                for jcol in range(len(transVirtS[0,:])):
                    weight += CISarray2[i-1][irow][jcol]**2
                    # if the coefficient is high enough it's printed with the arrival and starting orbitals
                    if abs(CISarray2[i-1][irow][jcol]**2) > 0.03:
                        print(irow + 1, "->",occMO + jcol + 1, ": %10.8f" %CISarray2[i-1][irow][jcol])
            print("weight %5.2f\n" %weight)

        for i in range(1, nroots+1):
            for j in range(1, nroots+1):
                # compute the final wavefunction overlap by multiplying CI factors
                psioverlap[i, j] = np.sum(np.multiply(CISarray2[i - 1], CISarray1[j - 1]))
       

        #print the new CI eigenvectors overlap
        print("\n------------CI roots overlap after the MO transformation------------\n")
        if compare in ['OMtoNW', 'NWtoOM']:
            print("Rows: NWChem CIS/TDA / Columns: RAS(PT2)\n\n")
        elif compare in ['OMtoGA', 'GAtoOM']:
            print("Rows: Gaussian CIS/TDA / Columns: RAS(PT2)\n\n")
        elif compare in ['GAtoNW', 'NWtoGA']:
            print("Rows: NWchem CIS/TDA / Columns: Gaussian CIS/TDA\n\n")
        elif compare in ['OMtoOM2', 'OM2toOM']:
            print("Rows: Second RAS(PT2) / Columns: First RAS(PT2)\n\n")
        

        # IMPORTANT THOUGHS ON WFO MATRIX NORMALIZATION
        # we chose row normalization since we analyze for each DFT state
        # normalizing by column would yield several too high coefficients for the same DFT state
        # to various RAS states
        # Some thoughts: do I even need to use the normalized version?
        # or can I prefer to normalizing the matrix by scaling the abs(max_value)
        # to 1/-1
        # obviously this normalization relies on the user's insight to evaluate if 
        # the normalization is too strong to alter the result too significantly
        # should probably change that by testing a different case study since in our case we were in 
        # favorable spot where the two excited states overlapped with themselves
        # this thought would probably be VERY important if we study a casa with experimental proof

        # # normalize the columns of the final WFO matrix
        # for j in range(1, nroots+1):
        #     sumq=0
        #     for i in range(1, nroots+1):
        #           sumq+=psioverlap[i ,j]**2
        #     #for lines with all zeros which represent not computed DFT states
        #     if (sumq != 0.0):
        #         psinorm[:,j]=psioverlap[:,j]/np.sqrt(sumq)
        #     else:
        #         psinorm[:,j]=psioverlap[:,j]

        # normalize the rows of the final WFO matrix
        for i in range(1, nroots+1):
            sumq=0
            for j in range(1, nroots+1):
                  sumq+=psioverlap[i ,j]**2
            # for lines with all zeros which represent not computed DFT states 
            if (sumq != 0.0):
                psinorm[i]=psioverlap[i]/np.sqrt(sumq)
            else:
                psinorm[i]=psioverlap[i]

        return psioverlap, MOoverlap, psinorm

############################################################################

    def calcWeight(MolFileName, MOoccup, totstate, active_el, activeorb, method):
        """
        Reads the CI coefficients of the ground state in order to calculate the normalization factor between the two calculations
        Only the CI of the closed-shell and single-excited configurations are selected
        Works identically to readCISfromMolcasOutput
        """

        with open(MolFileName) as f:
            out = f.read()
            
        occMO = MOoccup.count("O")
        virtMO = MOoccup.count("V")

        output = {"cis": {}}
        csfexp = "Number of CSFs *[0-9]*"
        tot_csf = int(((re.findall(csfexp, out)[0]).split())[-1])

        if method == "ras":
            regexp = "printout of CI-coefficients larger than  0.00 for root  1" + \
                 "(.*)" + "Natural orbitals and occupation numbers for root  1"
            linea = 2

        elif method == "xms":
            regexp = "The CI coefficients for the MIXED state nr.   1"+\
                "(.*)" + 'Happy landing!'
            linea = 6

        regExprText = re.findall(regexp, out, re.DOTALL | re.IGNORECASE)[0]
        cisLines = regExprText.rstrip().lstrip().splitlines()

        occactive = int(active_el / 2)
        virtactive = activeorb - occactive

        CSFstring=occactive*'2'+virtactive*'0'
        weight=0

        coeffs_exp={}
        
        # closed shell configuration
        regExprText = [s for s in cisLines[linea:linea+tot_csf] if CSFstring in s][0]
        if method == "ras":
            coeff = regExprText.rstrip().lstrip().split()[2]
        elif method == "xms":
            coeff = regExprText.rstrip().lstrip().split()[6]
                    
        weight += float(coeff)**2

        # singly-excited configurations
        for nocc in range(0, occactive):
            istring = CSFstring[0:nocc]+"u"+CSFstring[nocc+1:]
            for nvirt in range(0, virtactive):
                jstring = istring[0:occactive+nvirt]+"d"+istring[occactive+nvirt+1:]

                regExprText = [s for s in cisLines[linea:linea+tot_csf] if jstring in s][0]
                if method == "ras":
                    coeff = regExprText.rstrip().lstrip().split()[2]
                elif method == "xms":
                    coeff = regExprText.rstrip().lstrip().split()[6]
                    
                weight += float(coeff)**2

        return weight
            
############################################################################

    def correctMoldenSigns(NWCfile, occupation, molOrb):
        """
        Required since the NWC data gets the moldens from a simple single point DFT
        calculation outside of the dynamics. Depending on the MO coefficients found in 
        the dynamics output, this routine changes the signs of the ones found in the
        molden file. 
        """

        with open(NWCfile) as f:
            output=f.read()

        totorb=len(occupation)

        for i in range (1,totorb+1):
            
            # read the first coefficient and the AO it belong to
            if (i != totorb):
                MOexpr = "Vector  {:3d}  Occ".format(i) + "(.*)" + "Vector  {:3d}  Occ".format(i+1)
            else:
                MOexpr = "Vector  {:3d}  Occ".format(i) + "(.*)" + "Number of AO functions :"

            MOtxt = re.findall(MOexpr, output, re.DOTALL | re.IGNORECASE)[0]
            MOlines = MOtxt.rstrip().lstrip().splitlines()
            coeff=float((MOlines[4].split())[1])
            AO=(int((MOlines[4].split())[0])-1)

            moldenC = molOrb[i-1][AO]
            if (coeff * moldenC < 0): 
                for j in range(totorb):
                    molOrb[i-1][j]*=-1

        return molOrb

############################################################################

    def matrix_prettystring(matrix: np.ndarray, fmt, atomLabels=None) -> str:
        """
        Return a string with a nice formatted representation of a matrix, stored in a ndarray numpy object.
        The optional argument fmt should contain a string that defines the numeric format of the
        data to represent (without the field size that will be determined dinamically.

        :param matrix: numpy ndarray with the matrix to print to string
        :param fmt: numerical format to use in the matrix representation
        :return: string that contains the representation of the matrix
        """
        # compute the size that is needed to represent each column of the matrix
        colmax = [max([len(("{:." + fmt + "f}").format(x)) + 1 for x in col]) for col in matrix.T]
        # construct the format of the line
        rowformat = "".join(["{:" + str(colsize) + "." + fmt + "f}" for colsize in colmax])
        # print each row with the appropriate format and store the result in a string
        if atomLabels is None:
            string = "\n".join([rowformat.format(*row) for row in matrix]) + "\n"
        else:
            string = "\n".join(
                [("{:4s}" + rowformat).format(atomLabels[irow], *matrix[irow]) for irow in range(len(matrix))]) + "\n"
        # return the string with the matrix representation
        return string

############################################################################

    # Define command line arguments with argparser
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        description="Wavefunction overlap calculation",
        epilog="The script calculates the wavefunction overalap for n roots between a NWChem and a MOLCAS calculation",
        formatter_class=CustomFormatter)

    # selects the case
    parser.add_argument("-comp", "--compare", required=True, metavar='COMP', dest="compare",
                        type=str, help="Programs to compare (either NWtoGA, OMtoNW, OMtoGA or OMtoOM2)")
    

    ##-------------------------------------------- Molcas calculation ------------------------------------------------- 
    # OM basis set
    parser.add_argument("-om_bs", "--molcas_basis_set", required=False, metavar='OM_BSE', dest="molcas_basis_set",
                        type=str, help="basis set name (only 6-31g, 6-31g* and cc-pvdz)")

    # OM file to read the CI coefficients from
    parser.add_argument("-om_ci", "--molcas_ci", required=False, metavar='OM_CI', dest="molcas_ci",
                        type=str, help="ouput of the OpenMolcas calculation (must contain all the ci coeff")
    
    # OM file to read the MO from
    parser.add_argument("-om_mo", "--molcas_mo", required=False, metavar='OM_MO', dest="molcas_mo",
                        type=str, help="OpenMolcas Molden file containing RASSCF MO coeffs")
    
    # OM file to read the AO from
    parser.add_argument("-om_ao", "--molcas_ao", required=False, metavar='OM_AO', dest="molcas_ao",
                        type=str, help="OpenMolcas Molden file containing AO coeffs")
    ##-----------------------------------------------------------------------------------------------------------------


    ##-------------------------------------------- 2nd Molcas calculation --------------------------------------------- 
    # OM basis set
    parser.add_argument("-om2_bs", "--molcas2_basis_set", required=False, metavar='OM2_BSE', dest="molcas2_basis_set",
                        type=str, help="basis set name (only 6-31g, 6-31g* and cc-pvdz)")

    # OM file to read the CI coefficients from
    parser.add_argument("-om2_ci", "--molcas2_ci", required=False, metavar='OM2_CI', dest="molcas2_ci",
                        type=str, help="output of the second OpenMolcas calculation (must contain all the ci coeff")

    # OM file to read the MO from
    parser.add_argument("-om2_mo", "--molcas2_mo", required=False, metavar='OM2_MO', dest="molcas2_mo",
                        type=str, help="second OpenMolcas Molden file containing RASSCF MO coeffs")

    # OM file to read the AO from
    parser.add_argument("-om2_ao", "--molcas2_ao", required=False, metavar='OM2_AO', dest="molcas2_ao",
                        type=str, help="second OpenMolcas Molden file containing AO coeffs")
    ##-----------------------------------------------------------------------------------------------------------------


    ##---------------------------------------------- Gaussian calculation --------------------------------------------- 
    # G16 basis set
    parser.add_argument("-g16_bs", "--gaussian_basis_set", required=False, metavar='G16_BSE', dest="gaussian_basis_set",
                        type=str, help="basis set name (only 6-31g, 6-31g* and cc-pvdz)") 

    # G16 file to read the CI coefficients from
    parser.add_argument("-g16_ci", "--gaussian_ci", required=False, metavar='G16_CI', dest="gaussian_ci",
                        type=str, help="ouput of the Gaussian calculation (must contain all the ci coeff")
    
    # G16 file to read the MO from
    parser.add_argument("-g16_mo", "--gaussian_mo", required=False, metavar='G16_MO', dest="gaussian_mo",
                        type=str, help="Gaussian Fchk file containing HF or DFT MO coeffs")
    
    # G16 file to read the AO from
    parser.add_argument("-g16_ao", "--gaussian_ao", required=False, metavar='G16_AO', dest="gaussian_ao",
                        type=str, help="ouput of the Gaussian calculation (Must contain AO overlap)")
    ##-----------------------------------------------------------------------------------------------------------------


    ##---------------------------------------------- NWChem calculation -----------------------------------------------
    # NWC basis set
    parser.add_argument("-nw_bs", "--nwchem_basis_set", required=False, metavar='NWC_BSE', dest="nwchem_basis_set",
                        type=str, help="basis set name (only 6-31g, 6-31g* and cc-pvdz)")

    # NWC file to read the CI coefficients from
    parser.add_argument("-nw_ci", "--nwchem_ci", required=False, metavar='NWC_CI', dest="nwchem_ci",
                        type=str, help="ouput of the NWChem calculation (must contain all the ci coeff)")
    
    # NWC file to read the MO from
    parser.add_argument("-nw_mo", "--nwchem_mo", required=False, metavar='NWC_MO', dest="nwchem_mo",
                        type=str, help="NWChem Molden file containing HF or DFT MO coeffs")
    
    # NWC file to read the AO from
    parser.add_argument("-nw_ao", "--nwchem_ao", required=False, metavar='NWC_AO', dest="nwchem_ao",
                        type=str, help="ouput of the NWChem calculation (Must contain AO overlap)")
    ##-----------------------------------------------------------------------------------------------------------------   

    # number of TDDFT excited states
    parser.add_argument("-tdnr", "--tdnroot", required=False, metavar='TDR', dest="tdnroots",
                        type=int, help="number of excited states in NWChem")
    
    # number of RASSCF/RASPT2 excited states
    parser.add_argument("-casnr", "--casnroot", required=False, metavar='CAR', dest="casnroots",
                        type=int, help="number of excited states in Molcas")

    # number of RASSCF/RASPT2 excited states (2nd calculation)
    parser.add_argument("-casnr2", "--casnroot2", required=False, metavar='CAR2', dest="casnroots2",
                        type=int, help="number of excited states in second Molcas calculation")

    # number of active electrons in the CAS calculation
    parser.add_argument("-ae", "--activeel", required=False, metavar="AEL", dest="active_el",
                        type=int, help="number of active orbitals in CAS calculation")
    
    # number of active orbitals in the CAS calculation
    parser.add_argument("-ao", "--activeorb", required=False, metavar="AOR", dest="active_orb",
                        type=int, help="number of active orbitals in CAS calculation")
    
    # method used to compute OM excited states ('ras' or 'xms')
    parser.add_argument("-c", "--casmethod", required=False, metavar="MET", dest="cas_method",
                        type=str, help="method used to compute excited states in Molcas (Must be ras or xms)")

    args = parser.parse_args()

    # dictionary that will be used to process ANO basis sets and the keys used in order dictionaries
    OrderKey = {}

    data['comp'] = args.compare
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    
    # store files/basis set info depending on the 'comp' keyword
    calcs = []; basis_sets = []

    # first calculation
    calcs.append(data['comp'][0:2])
    # second calculation
    calcs.append(data['comp'][4:])


    # loop over methods to check the input files
    for calc in calcs:

        # OpenMolcas
        if calc == 'OM':
            data['molcas_mo'] = args.molcas_mo
            data['molcas_ci'] = args.molcas_ci
            data['molcas_bs'] = args.molcas_basis_set.lower()
            # iterate to check if any arguments have not been provided
            for o_key in data:
                # only GAU-related keys
                if o_key.startswith('molcas'):
                    # check if argument is provided
                    if data[o_key] == None:
                        logging.info('\n You missed the "{}" argument for OpenMolcas!\n'.format(o_key))
                        exit()
            basis_sets.append(data['molcas_bs'])

        # NWChem
        elif calc == 'NW':
            data['nwchem_mo'] = args.nwchem_mo
            data['nwchem_ci'] = args.nwchem_ci
            data['nwchem_bs'] = args.nwchem_basis_set.lower()
            # iterate to check if any arguments have not been provided
            for n_key in data:
                # only GAU-related keys
                if n_key.startswith('nwchem'):
                    # check if argument is provided
                    if data[n_key] == None:
                        logging.info('\n You missed the "{}" argument for NWChem!\n'.format(n_key))
                        exit()    
            basis_sets.append(data['nwchem_bs'])
                        
        # Gaussian
        elif calc == 'GA':
            data['gaussian_mo'] = args.gaussian_mo
            data['gaussian_ci'] = args.gaussian_ci
            data['gaussian_bs'] = args.gaussian_basis_set.lower()
            # iterate to check if any arguments have not been provided
            for g_key in data:
                # only GAU-related keys
                if g_key.startswith('gaussian'):
                    # check if argument is provided
                    if data[g_key] == None:
                        logging.info('\n You missed the "{}" argument for Gaussian!\n'.format(g_key))
                        exit()
            basis_sets.append(data['gaussian_bs'])


        # OpenMolcas eventual second calculation
        elif calc == 'OM2':
            data['molcas2_mo'] = args.molcas2_mo
            data['molcas2_ci'] = args.molcas2_ci
            data['molcas2_bs'] = args.molcas2_basis_set.lower()
            # iterate to check if any arguments have not been provided
            for o2_key in data:
                # only MOL2-related keys
                if o2_key.startswith('molcas2'):
                    # check if argument is provided
                    if data[o2_key] == None:
                        logging.info('\n You missed the "{}" argument for the second OpenMolcas!\n'.format(o2_key))
                        exit()
            basis_sets.append(data['molcas2_bs'])             

    # cas method
    data['cas_method'] = args.cas_method

    ## supposed fixed for now
    # active electrons
    data['active_el'] = args.active_el
    # active orbitals
    data['active_orb'] = args.active_orb

    # DFT excited states
    if args.tdnroots in args:
        data['tdnroots'] = args.tdnroots
    # CASPT2 excited states (1)
    data['casnroots'] = args.casnroots
    
    if 'OM2' in calc: 
        # CASPT2 excited states (2)
        data['casnroots2'] = args.casnroots2


    #####################################################################################################################
                                                    ###--MAIN BLOCK--###
    #####################################################################################################################

    ##-------------------------------------------------- Basis set orders ---------------------------------------------
    # dictionary with the orders of molecular orbitals.
    MOorder = {'NWC': {}, 'MOL': {}, 'GAU': {}, 'NAT': {}}

    # dictionary with the orders of atomic orbitals.
    AOorder = {'NWC': {}, 'MOL': {}, 'BSE': {}, 'NAT': {}}

    # initalize AO/MO order for only the required basis sets
    initializeAtomicOrders(basis_sets[0], basis_sets[1])

    # does nothing, is relevant if the basis of the AO/MO orders is not 'NAT'
    changeBasisReferenceOrder(basis_sets, 'NAT')
    ##-----------------------------------------------------------------------------------------------------------------


    ##-------------------------------------------------- AO matrix ----------------------------------------------------
    if args.compare in ["OMtoNW", "NWtoOM"]:
        AO_matrix = readAOoverlapfromBSE(args.molcas_mo, args.molcas_basis_set, args.nwchem_basis_set)

    elif args.compare in ["OMtoGA", "GAtoOM"]:
        AO_matrix = readAOoverlapfromBSE(args.molcas_mo, args.molcas_basis_set, args.gaussian_basis_set)
    
    elif args.compare in ["OMtoOM2", "OM2toOM"]:
        AO_matrix = readAOoverlapfromBSE(args.molcas_mo, args.molcas_basis_set, args.molcas2_basis_set, cross_geom=True, file_2=args.molcas2_mo)
    ##-----------------------------------------------------------------------------------------------------------------


    ##------------------------------------------------- CI/MO File reading --------------------------------------------
    # NWChem
    if args.compare in  ["NWtoGA", "GAtoNW", "OMtoNW", "NWtoOM"]:

        # read orbitals occupation
        occupation_nwchem = readOccfromNWChemOutput(args.nwchem_ci)
        
        # read MO coefficients 
        mo_molden_temp = readfrommolden(args.nwchem_mo, occupation_nwchem)
        
        # find differences with MO coeff. signs between the two files and corrects them (reference is the .out/.log)
        mo_molden = correctMoldenSigns(args.nwchem_ci, occupation_nwchem, mo_molden_temp)

        # MO coefficients after rotation
        MO_NWC = rotateOrbs(args.nwchem_mo, mo_molden, occupation_nwchem, data['nwchem_bs'], 'NWChem', args.compare)
        
        # read CI expansion
        if args.casnroots == None or args.tdnroots == args.casnroots:
            CIS_NWC = readCISfromNWChemOutput(args.nwchem_ci, occupation_nwchem, args.tdnroots)
        # in case the number of roots differ from the two calculations (e.g., when Molcas haas more)
        else:
            CIS_NWC = readCISfromNWChemOutput(args.nwchem_ci, occupation_nwchem, args.tdnroots, extra_roots=True, nextra=(args.casnroots-args.tdnroots))


    # Gaussian
    if args.compare in ["NWtoGA", "GAtoNW", "OMtoGA", "GAtoOM"]:

        # read orbital occupation
        occupation_gaussian = readOccfromGaussianOutput(args.gaussian_ci)

        # read MO coefficients 
        mo_gaussian = readfromfchk(args.gaussian_mo, occupation_gaussian)

        # MO coefficients after rotation
        MO_GAU = rotateOrbs(args.gaussian_mo, mo_gaussian, occupation_gaussian, data['gaussian_bs'], 'Gaussian', args.compare)
        
        # read CI expansion
        if args.casnroots == None or args.tdnroots == args.casnroots:
            CIS_GAU = readCISfromGaussianOutput(args.gaussian_ci, occupation_gaussian, args.tdnroots)
        # in case the number of roots differ from the two calculations (e.g., when Molcas haas more)
        else:
            CIS_GAU = readCISfromGaussianOutput(args.gaussian_ci, occupation_gaussian, args.tdnroots, extra_roots=True, nextra=(args.casnroots-args.tdnroots))


    # Molcas
    if args.compare in ["OMtoNW", "NWtoOM", "OMtoGA", "GAtoOM"]:

        # read orbital occupation
        occupation_molcas = readOccfromMolcasOutput(args.molcas_mo) 

        # read MO coefficients 
        mo_molden = readfrommolden(args.molcas_mo, occupation_molcas)

        # MO coefficients after rotation
        MO_MOL = rotateOrbs(args.molcas_mo, mo_molden, occupation_molcas, data['molcas_bs'], 'Molcas', args.compare)
        
        # read CI expansion
        CIS_MOL = readCISfromMolcasOutput(args.molcas_ci, occupation_molcas, args.casnroots, args.active_el, args.active_orb,args.cas_method)
    

    # Molcas, both
    if args.compare in ['OMtoOM2']:

        # read orbital occupation
        occupation_molcas = readOccfromMolcasOutput(args.molcas_mo)
        occupation_molcas_2 = readOccfromMolcasOutput(args.molcas2_mo)

        # read MO coefficients 
        mo_molden = readfrommolden(args.molcas_mo, occupation_molcas)
        mo_molden_2 = readfrommolden(args.molcas2_mo, occupation_molcas_2)

        # MO coefficients after rotation
        MO_MOL = rotateOrbs(args.molcas_mo, mo_molden, occupation_molcas, data['molcas_bs'], 'Molcas', args.compare)
        MO_MOL_2 = rotateOrbs(args.molcas2_mo, mo_molden_2, occupation_molcas_2, data['molcas2_bs'], 'Molcas', args.compare)
        
        # read Ci expansion
        CIS_MOL = readCISfromMolcasOutput(args.molcas_ci, occupation_molcas, args.casnroots, args.active_el, args.active_orb, args.cas_method)
        CIS_MOL_2 = readCISfromMolcasOutput(args.molcas2_ci, occupation_molcas_2, args.casnroots2, args.active_el, args.active_orb, args.cas_method)
    ##-----------------------------------------------------------------------------------------------------------------


    ##--------------------------------------------------- WF and MO overlap -------------------------------------------
    if args.compare in ["OMtoNW", "NWtoOM"]:
        WFO, MOO, WFOn = computeOverlap(occupation_molcas, occupation_nwchem, AO_matrix, MO_MOL, MO_NWC, CIS_MOL, CIS_NWC, args.casnroots, args.compare)

    elif args.compare in ["OMtoGA", "GAtoOM"]:
        WFO, MOO, WFOn = computeOverlap(occupation_molcas, occupation_gaussian, AO_matrix, MO_MOL, MO_GAU, CIS_MOL, CIS_GAU, args.casnroots, args.compare)
        
    elif args.compare in ['OMtoOM2']:
        WFO, MOO, WFOn = computeOverlap(occupation_molcas, occupation_molcas_2, AO_matrix, MO_MOL, MO_MOL_2, CIS_MOL, CIS_MOL_2, args.casnroots2, args.compare)
    ##-----------------------------------------------------------------------------------------------------------------    
    
    weight = calcWeight(args.molcas_ci, occupation_molcas, args.casnroots, args.active_el, args.active_orb,args.cas_method)

    # print the two WFO matrices
    #'6' and '2' represent the number of decimal digits
    print("States overlap matrix\n")
    print(matrix_prettystring(WFO, "6"))
    print("\nStates overlap matrix - simple\n")
    print(matrix_prettystring(WFO, "2"))
    print("\nNormalized states overlap matrix\n")
    print(matrix_prettystring(WFOn, "6"))
    print("\nNormalized states overlap matrix - simple\n")
    print(matrix_prettystring(WFOn, "2"))

    # print a file called 'NEW', in which is also stored the command input line
    with open("NEW", "w") as fR:
        for arg in sys.argv:
            fR.write('{} '.format(arg))
        fR.write('\n')
    # delete eventual files named 'OLD'
    if os.path.isfile("OLD"):
        os.remove("OLD")

#####################################################################################################

if __name__ == '__main__':
    main()
