#!/usr/bin/env python3
# coding=utf-8

#    COBRAMM
#    Copyright (c) 2019 ALMA MATER STUDIORUM - Università di Bologna

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#####################################################################################################

# import statments of module from python standard library

import argparse    # command line arguments parser
import sys         # System-specific parameters and functions
import os          # filesystem utilities
import shutil      # filesystem utilities
import shlex
import time
import subprocess
import numpy as np
import copy
import re
import concurrent.futures #parallelization

# hijack $PYTHONPATH (sys.path) to find cobramm local modules
try:
    sys.path.append(os.path.join(os.environ['COBRAM_PATH'], 'cobramm'))
except KeyError:
    raise RuntimeError('cannot import cobramm module.\n'
                       'Please check if $COBRAM_PATH is defined in your environment, if not run:\n'
                       'export COBRAM_PATH=/path/to/COBRAMM')

# imports of local modules

import CBF
import logwrt  # write messages and output to log (std out or cobramm.log)
import cobrammenv  # management of the enviroment for running COBRAMM (and utils)
import constants
from output import Output # read data from xml
from cobrammCalculator import CobrammCalculator, CobrammInput, CobrammOutput
from transientCalculator import PumpProbeCalculator, MultiwfnCalculator
import WFOmanager as WFO


#####################################################################################################

def process_folder(folder, pp_instance, charge, func, nc, bset, cartesian, molcas_inputs, basis_block):
    ''' This function controls the parallel execution of the operations on the various trajectories
    in case the --parallel option is activated '''

    print("\nEntering in the trajectory folder : {}\n".format(folder))
    upper_dir = os.getcwd()
    pp_instance.check_folder(folder)
    os.chdir(folder)
    pp_instance.setup_vertical_excitations(chrg=charge, functional=func,
                                          ncores=nc, basis_set=bset,
                                          cartesian=cartesian,
                                          molcas_inputs=molcas_inputs, basis_block=basis_block)
    os.chdir(upper_dir)
    print("\nback to folder : {}\n".format(upper_dir))

#####################################################################################################

def determine_mdv_level_of_theory(command):
    ''' This function determines the level of theory (TDDFT, CASSCF, SS-CASPT2, MS-CASPT2, XMS-CASPT2, RMS-CASPT2)
        from lines of cobram.command of the mdv directory.
        command : list of lines from cobram.command '''
    qmSw = None
    method = None
    molcas_section = False
    for line in command:
        if "qm-type" in line:
            if 'gauss' in line:
                qmSw = 'gaussian'
                break
            elif 'molcas' in line:
                qmsw = 'molcas'
            else:
                print('ERROR: mdv software not recognized (check cobram.command in {0})\nAborting...\n'.format(args.directories_list[0]))
                sys.exit(1)
        elif '!molcas' in line:
            molcas_section = True
            continue
        elif '?molcas' in line:
            molcas_section = False
            break
        elif molcas_section and '&rasscf' in line.lower():
            method = 'CASSCF'
        elif molcas_section and '&caspt2' in line.lower():
            method = 'MS-CASPT2'
        elif molcas_section and method == 'MS-CASPT2' and 'nomu' in line.lower():
            method = 'SS-CASPT2'
            break
        elif molcas_section and method == 'MS-CASPT2' and 'xmul' in line.lower():
            method = 'XMS-CASPT2'
            break
        elif molcas_section and method == 'MS-CASPT2' and 'rmul' in line.lower():
            method = 'RMS-CASPT2'
            break
    if qmSw:
        method = 'TD-DFT'

    return method

#####################################################################################################

def determine_nref(command, method):
    ''' This function determines the number of states in dynamics (TDDFT, CASSCF, SS-CASPT2, MS-CASPT2, XMS-CASPT2, RMS-CASPT2)
        from lines of cobram.command of the mdv directory.
        command : list of lines from cobram.command '''
    molcas_section = False
    g16_route =False
    for i in range(len(command)):
        if method == 'TD-DFT' and "#p" in command[i].lower():
            pattern = r'nstates=(\d+)'
            match = re.search(pattern, command[i], flags=re.IGNORECASE)
            if match:
                x = int(match.group(1))
                return x
            else:
                return None
        else:
            if re.search('ciro', command[i], re.IGNORECASE):
                try:
                    Nroots = int(command[i + 1].split()[0])
                except:
                    Nroots = int(command[i].split('=')[1].split()[0])
                return Nroots

#####################################################################################################

def main():

    # this new class defines a style for the parser message that join the two predefined styles
    class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter, argparse.RawDescriptionHelpFormatter):
        pass

    def dir_path(string):
        if os.path.isdir(string):
            return string
        else:
            raise NotADirectoryError(f"directory {string} doesn't exist")

    # Define command line arguments with argparser
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        description="COBRAMM utility script: setup and run time-resolved transient absorption",
        epilog="The script runs vertical excitations on top of geometries along a TSH trajectories\n\n",
        formatter_class=CustomFormatter)

    parser.add_argument("-dir", "--directories", nargs="+", required=True, metavar='DIR', dest="directories_list",
                        type=dir_path, help="path of the directories containing the TSH trajectories")
    parser.add_argument("-i", "--input", required=True, metavar='INP', dest="input_file",
                        type=str, help="input file for spectroscopy simulation")
    parser.add_argument("--cluster", action="store_true",
                        help="submission of vertical excitation on remote cluster (in case of --cluster option)")
    parser.add_argument("-sub", "--submiss", metavar="SUBSTR", dest="submission_string",
                        type=str, default="", help="submission string for remote cluster")
    parser.add_argument("--parallel", action="store_true",
                        help="activate parallel run for the different trajectories")
    parser.add_argument("--wfo", required=False, help="perform WFO computation after CASPT2 ended", action="store_true")
    parser.add_argument("--tda", required=False, help="perform SP-TDA computation to get the data required to calculate WFO", action="store_true")
    parser.add_argument("-pc", "--pcores", default=1, metavar='PCORES', dest="pcores", type=int,
                        help="number of cpus used for parallelization on local resources (if 'parallel' option is used)")
    parser.add_argument('--version', action='version',
                        version='%(prog)s is part of COBRAMM {}'.format(cobrammenv.getVersion()))
    # TODO ADD PARSER OUTPUT

    args = parser.parse_args()


    print("\n COBRAMM utility to obtain data for Pump-Probe spectroscopy simulation.\n\n")
    # READ DATA FROM cobramm.xml OF FIRST DIR IN THE LIST. 
    # print warning to tell the user
    print("Reading dynamics metadata from first directory {0} (cobramm.xml and cobram.command files)...\n".format(args.directories_list[0]))
    print("WARINING! Metadata (presence of triplets, number of states, timestep etc.) will be assumed uniform for all trajectories in the list.")
    print("          In case you have trajectories with different metadata, please run a copy of this script per each group of trajectories with identical setup.\n\n")
    
    
    # initialize the list with the COBRAMM options
    inphard = CBF.makehard()
    inphard.insert(0, '0')
    # get the content of the spectroscopy command file
    cobcom = CBF.getCobrammCommand(args.input_file)
    # merge command soft and hard
    key_soft = CBF.ReadCobramCommand(cobcom, 'keyword', 'keywords')
    inphard = CBF.key2hard(key_soft, inphard)
    inp_soft = CBF.ReadCobramCommand(cobcom, 'command', 'commands')
    inphard = CBF.soft2hard(inp_soft, inphard)
    # list of input commands
    inp = inphard


    # compatibility control:
    if inp[250] == 'TRPES' and inp[251] == 'TD-DFT':
        print('ERROR: TRPES spectroscopy is currently implemented only for CASSCF or SS/MS/XMS/RMS-CASPT2\nAborting...\n')
        sys.exit(1)

    # argument control:
    if inp[250] not in ('UVVis', 'TRPES'):
        print("ERROR: Currently only 'TRPES' or 'UVVis' spectroscopies are allowed....\nAborting...\n")
        sys.exit(1)
    if inp[251] not in ('TD-DFT', 'CASSCF', 'CASPT2'):
        print("ERROR: allowed methods are CASSCF. CASPT2, TD-DFT.\nAborting...\n")
        sys.exit(1)

    # set parameters for spectroscopy calculations
    
    spectroscopy = inp[250]
    method = inp[251]
    nhighstates = int(inp[252])
    simulation_time = int(inp[253])
    delta_t = int(inp[254])
    ncores = int(inp[7])
    # Set SingleState to false (it will be turned to true onlu for CASPT2 of SS type later)
    SingleState = False

    # open xml file from first trajectory in the list
    try:
        OUT = Output(filename=args.directories_list[0]+'/cobramm.xml', parse=True)
    except IOError:
        print('ERROR: output file not found in {0} directory\nAborting...\n'.format(args.directories_list[0]))
        sys.exit(1)

    # check for cluster/local parallelization compatibility:
    if args.cluster and args.parallel:
        print('ERROR: parallel execution is only available on local resources (cannot run on remote cluster)\n Aborting...\n')
        sys.exit(1)

    # read QM and basisset sections of cobram.command:
    with open (args.directories_list[0]+'/cobram.command', 'r') as f:
        commandlines = f.readlines()
    raw_commandlines = [i.strip().strip('\n') for i in commandlines]
    commandlines = [i for i in raw_commandlines if i != '']

    mdv_level = determine_mdv_level_of_theory(commandlines)
    
    if OUT.command_list[51] == '1':
        start_index = commandlines.index('!gaussian') +1
        end_index = commandlines.index('?gaussian')
    elif OUT.command_list[51] == '6':
        start_index = commandlines.index('!molcas') +1
        end_index = commandlines.index('?molcas')
    else:
        print('ERROR: Only Gaussian and Molcas are currently supported QM softwared for sprctroscopy simulation\nAborting...\n')
        sys.exit(1)

    raw_QM_command = [s.lower() for s in raw_commandlines[start_index : end_index]]
    QM_command = [s.lower() for s in commandlines[start_index : end_index]]

    # set some metadata from xml and command files:
    #(1)  NUMBER OF STATES
    #     GAUSSIAN (TDDFT): we will run only one calculation with nstates = args.nhigh
    if inp[251] == 'TD-DFT':
        if OUT.command_list[42] == '1':
            nsinglets = ntriplets = determine_nref(commandlines, 'TD-DFT')
            nstates = nhighstates
            print("Molecular dynamics with ISC found! Number of singlets is {0}, number of triplets is {1}".format(nsinglets, ntriplets))
        else:
            nstates = nhighstates
            nsinglets = nhighstates
            ntriplets = 0
    # MOLCAS (CASSCF/CASPT2): in case of ISC dynamics, initialize number of singlets/triplets
    else:
        if OUT.command_list[42] == '1':
            nsinglets = int(OUT.command_list[44])
            ntriplets = int(OUT.command_list[43])
            nstates = nsinglets + ntriplets
            print("Molecular dynamics with ISC found! Number of singlets is {0}, number of triplets is {1}".format(nsinglets, ntriplets))
        else:
            nsinglets = determine_nref(commandlines, mdv_level)
            ntriplets = 0
            nstates = nsinglets
            print("Number of reference (valence) states in molecular dynamics is {0}".format(nsinglets))

    #(2)  retrieve charge of QM part
    if OUT.command_list[51] == '1':
        charge = int(QM_command[-1][0])
    else:
        # charge is assumed 0 for QM part in MOLCAS
        charge = 0
        for i in range(len(QM_command)):
            # unless 'charge' is present in molcas section
            if 'charge' in QM_command[i]:
                if '=' in QM_command[i]:
                    #in this case, net charge is in the same line, after '='
                    charge = int(QM_command[i].split('=')[-1])
                else:
                    #in this case, charge is in the next line (alone)
                    charge = int(QM_command[i+1])

    #(3)   retrieve dyn time step (use only short one)
    # DEBUG: it would be complicated to use non-uniform timestep...for now I assume all dyn has the same timestep (command[84])
    time_step = float(OUT.command_list[84])

    #(4) retrieve basis set
    # DEBUG: I assume basis set is defined through command[197]...tocheck if this is always true (I think gaussian dynamics does not need basis set definition in command section)
    basis = OUT.command_list[197]

    #(5) retrieve info on cartesian/spherical basis
    cartesian = False
    if OUT.command_list[194] == '1':
        cartesian = True

    # initialize empty dict for molcas inputs and basis set additional block
    molcas_inputs = {'ref_S' : None, 'ref_T' : None, 'high_S' : None}
    basis_block = ''
    
    if inp[251] in ("CASSCF", "CASPT2"):
        functional = None
        # reference for singlet states (as dyn input)
        ref_S = []
        for i in range(len(QM_command)):
            if '&alaska' in QM_command[i] or '&rassi' in QM_command[i]:
                break
            elif ('grdt' not in QM_command[i]) and ('sadr' not in QM_command[i]) and ('copy' not in QM_command[i]):
                ref_S.append(QM_command[i])
        molcas_inputs['ref_S'] = '\n'.join(ref_S)
    
        # reference for triplet states (edit spin and number of states)
        ref_T = copy.deepcopy(ref_S)
        spin_key_present = False
        for i in range(len(ref_T)):
            if 'spin' in ref_T[i]:
                spin_key_present = True
                if '=' in ref_T[i]:
                    ref_T[i] = 'spin=3'
                else:
                    ref_T[i+1] = 3
            if 'ciro' in ref_T[i]:
                if '=' in ref_T[i]:
                    ref_T[i] = 'ciro={0} {0} 1'.format(ntriplets)
                else:
                    ref_T[i+1] = '{0} {0} 1'.format(ntriplets)
            if "mult" in ref_T[i] or "xmul" in ref_T[i] or "rmul" in ref_T[i]:
                if "nomu" in ref_T[i]:
                    pass
                elif "=" in ref_T[i]:
                    ref_T[i] = ref_T[i].split('=')[0] + "={0} ".format(ntriplets) + ' '.join([str(n) for n in range(1, ntriplets+1)])
                else:
                    ref_T[i+1] = "{0} ".format(ntriplets) + ' '.join([str(n) for n in range(1, ntriplets+1)])
        if not spin_key_present:
            pos = QM_command.index('&rasscf') + 1
            ref_T.insert(pos, 'spin=3')
    
        molcas_inputs['ref_T'] = '\n'.join(ref_T)
    
        # high states (edit number of roots and active electrons in case of TRPES)
        high_S = copy.deepcopy(ref_S)
        # edit molcas section for high ststes
        for i in range(len(high_S)):
            # number of states
            if high_S[i][:4] == 'ciro':
                if '=' in high_S[i]:
                    high_S[i] = 'CIRO={0} {0} 1'.format(nhighstates)
                else:
                    high_S[i+1] = '{0} {0} 1'.format(nhighstates)
            if "mult" in high_S[i] or "xmul" in high_S[i] or "rmul" in high_S[i]:
                if "nomu" in high_S[i]:
                    SingleState = True
                    pass
                elif "=" in high_S[i]:
                    high_S[i] = high_S[i].split('=')[0] + "={0} ".format(nhighstates) + ' '.join([str(n) for n in range(1, nhighstates+1)])
                else:
                    high_S[i+1] = "{0} ".format(nhighstates) + ' '.join([str(n) for n in range(1, nhighstates+1)])
            # in case of TRPES, remove one electron from active space
            if spectroscopy == 'TRPES' and high_S[i][:4] == 'nact':
                if '=' in high_S[i]:
                    n1 = int(high_S[i].split('=')[-1].split()[0])
                    try:
                        n2 = high_S[i].split('=')[-1].split()[1]
                    except KeyError:
                        n2 =0
                    try:
                        n3 = high_S[i].split('=')[-1].split()[2]
                    except KeyError:
                        n3 =0
                    high_S[i] = 'NACTEL={0} {1} {2}'.format(n1-1, n2, n3)
                else:
                    n1 = int(high_S[i+1].split()[0])
                    try:
                        n2 = high_S[i+1].split('=')[-1].split()[1]
                    except KeyError:
                        n2 =0
                    try:
                        n3 = high_S[i+1].split('=')[-1].split()[2]
                    except KeyError:
                        n3 =0
                    high_S[i+1] = '{0} {1} {2}'.format(n1-1, n2, n3)
            if spectroscopy == 'TRPES' and high_S[i][:6] == 'charge':
                if '=' in high_S[i]:
                    high_S[i] = 'CHARGE={0}'.format(charge+1)
                else:
                    high_S[i+1] = '{0}'.format(charge+1)
        molcas_inputs['high_S'] = '\n'.join(high_S)

        #RASSI section
        if method == 'CASSCF' or SingleState:
            molcas_inputs['ref_S'] += '\n>> COPY molcas.JobIph ../ref_WF\n\n'
            molcas_inputs['ref_T'] += '\n>> COPY molcas.JobIph ../ref_WF\n\n'
            RASSI_SEC = "\n>> COPY ../ref_WF JOB001\n>> COPY molcas.JobIph JOB002\n"
        else:
            molcas_inputs['ref_S'] += '\n>> COPY molcas.JobMix ../ref_WF\n\n'
            molcas_inputs['ref_T'] += '\n>> COPY molcas.JobMix ../ref_WF\n\n'
            RASSI_SEC = "\n>> COPY ../ref_WF JOB001\n>> COPY molcas.JobMix JOB002\n"
        # add &RASSI for TDM or Dyson
        if spectroscopy == 'TRPES':
            RASSI_SEC += "\n&RASSI\nNrOfJobs\n2 {0} {1}\n".format(nsinglets, nhighstates)
            RASSI_SEC += ' '.join([str(n) for n in range(1, nsinglets+1)])
            RASSI_SEC += '\n'
            RASSI_SEC += ' '.join([str(n) for n in range(1, nhighstates+1)])
            RASSI_SEC += '\nEJOB\n'
            RASSI_SEC += "Dyson\n"
        elif spectroscopy == 'UVVis':
            RASSI_SEC += "\n&RASSI\nNrOfJobs\n2 {0} {1}\n".format(nsinglets, nhighstates)
            RASSI_SEC += ' '.join([str(n) for n in range(1, nsinglets+1)])
            RASSI_SEC += '\n'
            RASSI_SEC += ' '.join([str(n) for n in range(1, nhighstates+1)])
            RASSI_SEC += '\nEJOB\n'
            RASSI_SEC += "ONEL\nMEIN\nProperties=3\n'Mltpl 1' 1 'Mltpl 1' 2 'Mltpl 1' 3\n"
        molcas_inputs['high_S'] += RASSI_SEC

        for i in molcas_inputs:
            molcas_inputs[i] = "!molcas\n" + molcas_inputs[i] + "\n?molcas"
    else:
        #TDDFT: retrieve functional
        pattern = r'#p\s+(\S+?)/'
        for line in commandlines:
            if "#p" in line.lower():
                match = re.search(pattern, line)
                functional = match.group(1)

    try:
        #DEBUG: reading basisset section from cobram.command...STILL TO TEST IF THIS WORKS
        bs_start_index = commandlines.index('!basisset') + 1
        bs_end_index = commandlines.index('?basisset')
        basis_block = '\n'.join(commandlines[bs_start_index:bs_end_index])
    except ValueError:
        basis_block = ''

    # Start of pumpprobe section:
    # initialize class
    pp = PumpProbeCalculator(trjdir_list=args.directories_list, method=method, simulation_time=simulation_time,
                             time_step=time_step, delta_t=delta_t, nstates=nstates, nsinglets=nsinglets, spectroscopy=spectroscopy)
    if SingleState:
        pp.is_SS_PT2 = True

    upper_dir = os.getcwd()
    print("\nSetting up vertical excitations with a time step of {} fs\n".format(pp.delta_t))

    # setup and run single point calculations
    if args.parallel:
        print("\nPARALLEL RUN ON {} CORES!\n".format(args.pcores))
        with concurrent.futures.ProcessPoolExecutor(max_workers=args.pcores) as executor:
            executor.map(process_folder, args.directories_list, [pp] * len(args.directories_list),\
                                                                [charge] * len(args.directories_list),\
                                                                [functional] * len(args.directories_list),\
                                                                [ncores] * len(args.directories_list),\
                                                                [basis] * len(args.directories_list),\
                                                                [cartesian] * len(args.directories_list),\
                                                                [molcas_inputs] * len(args.directories_list),\
                                                                [basis_block] * len(args.directories_list))
    else:
        for folder in args.directories_list:

            print("\nEntering in the trajectory folder : {}\n".format(folder))
    
            pp.check_folder(folder)
            os.chdir(folder)
    
            pp.setup_vertical_excitations(chrg=charge, functional=functional,
                                          ncores=ncores, basis_set=basis, cluster=args.cluster,
                                          submission_string=args.submission_string,cartesian=cartesian,
                                          molcas_inputs=molcas_inputs, basis_block=basis_block)
    
            os.chdir(upper_dir)

    if args.cluster:
        print("\nVertical excitations submitted!\n")
    else:
        print("\nVertical excitations completed!\n")


    #######################################################################
    ##--------------------WFO(+TDA) computation block----------------------
    #######################################################################

    # the WFO directories are already created, either by the TDA calculation block  
    # or during the dynamic if the higher printout is set
    print("\n--Setting up WFO calculation block--\n")

    for folder in args.directories_list:

        # transform into absolute path
        folder_path = os.path.join(upper_dir,folder)
        
        if args.tda:

            # creates WFO directory; there SP calculations will be performed
            # and then only the two relevant output files will remain
            TDA_upper = WFO.paths['WFO']

            os.chdir(folder_path)
            if os.path.isdir(TDA_upper):
                shutil.rmtree(TDA_upper)
            os.mkdir(TDA_upper)

            # calculation step
            delta = int(float(args.delta_t)/float(args.time_step))        

            # maxstep
            max_step = int(float(args.simulation_time)/float(args.time_step))

            # loop over the same steps of the pump-probe block
            for step in range(args.start_step, max_step+1, delta):

                TDA_dir = os.path.join(TDA_upper,'inputs_from_step_{}'.format(step))

                # launch cobramm-get-step.py
                with open("setupTDA.log","w") as out:
                    command = shlex.split("cobramm-get-step.py -n {}".format(step))
                    subprocess.run(command, stdout=out, stderr=subprocess.STDOUT) 

                # change directory name in case last step is included
                if os.path.isdir("inputs_from_last_step"):
                    shutil.move("inputs_from_last_step", "inputs_from_step_{}".format(step))

                # take the input files and move in the appropriate ('WFO') directory
                shutil.move("inputs_from_step_{}".format(step), TDA_upper)
                
                # substitute the cobram.command with the one for SP calculations
                shutil.copy('tda.command', os.path.join(TDA_dir,'cobram.command'))
                
                # enter directory
                os.chdir(TDA_dir)
                # launch TDA calculations
                with open('qsub.log','w') as fout:
                    command = shlex.split("qcobramm-git -q x24-core --force &".format(step))
                    subprocess.run(command, stdout=fout, stderr=subprocess.STDOUT) 
                # go back to the upper directory
                os.chdir(folder_path)

            # message after calculations have been launched
            print("\n--DFT calculations submitted!--\n")

            # second loop to process the output 
            for step in range(args.start_step, max_step+1, delta):           

                # set directory names
                TDA_input = os.path.join(TDA_upper,'inputs_from_step_{}'.format(step))
                TDA_output = os.path.join(TDA_upper, 'step_{}'.format(step))

                # check calculation completion
                while os.path.isfile(os.path.join(TDA_input,'QM_data/qmALL.log')) == False:
                    time.sleep(20)

                os.mkdir(TDA_output)
                # move the two required output files (.out and .chk)
                shutil.move(os.path.join(TDA_input,'QM_data/qmALL.log'),os.path.join(TDA_output,'GA.out'))
                shutil.move(os.path.join(TDA_input,'QM_data/gaussian-QM_0.chk.gz'),os.path.join(TDA_output,'GA.chk.gz'))
                # delete the rest
                shutil.rmtree(TDA_input)
                # final touches (de-crypting checkpoint file)
                os.chdir(TDA_output)
                os.system('gunzip GA.chk.gz')
                os.system('formchk GA.chk > chk.log')
                os.remove('GA.chk'); os.remove('chk.log')
                os.chdir(folder_path)

            print("\n--DFT calculations completed!--\n") 


        if args.wfo:

            os.chdir(folder_path)

            # temporary setting as 0.5 fs (which is of my dynamic)
            # OVERRIDE with flavia's reading from cobramm.xml
            time_step = 0.5

            # maxstep
            max_step = int(float(args.simulation_time)/float(time_step))

            # calculation step
            delta = int(float(args.delta_t)/float(time_step))

            # launch all WFO calculations with the cobramm-wfo.py routine
            WFO.WFOLauncher(max_step, args.start_step, delta, 'Gaussian', 'Molcas')

#####################################################################################################

if __name__ == '__main__':
    main()
