#!/usr/bin/env python3
# coding=utf-8

#    COBRAMM
#    Copyright (c) 2019 ALMA MATER STUDIORUM - Università di Bologna

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#####################################################################################################

# import statments of module from python standard library

import argparse    # command line arguments parser
import sys         # System-specific parameters and functions
import os          # filesystem utilities
import shutil      # filesystem utilities
import numpy as np
import re

# hijack $PYTHONPATH (sys.path) to find cobramm local modules
try:
    sys.path.append(os.path.join(os.environ['COBRAM_PATH'], 'cobramm'))
except KeyError:
    raise RuntimeError('cannot import cobramm module.\n'
                       'Please check if $COBRAM_PATH is defined in your environment, if not run:\n'
                       'export COBRAM_PATH=/path/to/COBRAMM')

# imports of local modules
import CBF
import logwrt  # write messages and output to log (std out or cobramm.log)
import cobrammenv  # management of the enviroment for running COBRAMM (and utils)
import constants
from cobrammCalculator import CobrammCalculator, CobrammInput, CobrammOutput
from transientCalculator import PumpProbeCalculator, MultiwfnCalculator
from output import Output # read data from xml

#####################################################################################################

def determine_nref(command, method):
    ''' This function determines the number of states in dynamics (TDDFT, CASSCF, SS-CASPT2, MS-CASPT2, XMS-CASPT2, RMS-CASPT2)
        from lines of cobram.command of the mdv directory.
        command : list of lines from cobram.command '''
    molcas_section = False
    g16_route =False
    for i in range(len(command)):
        if method == 'TD-DFT' and "#p" in command[i].lower():
            pattern = r'nstates=(\d+)'
            match = re.search(pattern, command[i], flags=re.IGNORECASE)
            if match:
                x = int(match.group(1))
                return x +1
            else:
                return None
        else:
            if re.search('ciro', command[i], re.IGNORECASE):
                try:
                    Nroots = int(command[i + 1].split()[0])
                except:
                    Nroots = int(command[i].split('=')[1].split()[0])
                return Nroots

#####################################################################################################
def main():

    # this new class defines a style for the parser message that join the two predefined styles
    class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter, argparse.RawDescriptionHelpFormatter):
        pass

    def dir_path(string):
        if os.path.isdir(string):
            return string
        else:
            raise NotADirectoryError(f"directory {string} doesn't exist")


    # Define command line arguments with argparser
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        description="COBRAMM utility script: simulate pump-probe spectrum during a dynamics",
        epilog="The script collects all the necessary data (pre-computed through cobramm-runPP.py)"
               " and simulates the transient spectrum\n\n",
        formatter_class=CustomFormatter)

    parser.add_argument("-dir", "--directories", nargs="+", required=True, metavar='DIR', dest="directories_list",
                        type=dir_path, help="path of the directories containing the trajectories")
    parser.add_argument("-i", "--input", required=True, metavar='INP', dest="input_file",
                        type=str, help="input file for spectroscopy simulation")
    parser.add_argument('--version', action='version',
                        version='%(prog)s is part of COBRAMM {}'.format(cobrammenv.getVersion()))
    # TODO ADD PARSER OUTPUT

    args = parser.parse_args()

    print("\n COBRAMM utility for Pump-Probe spectroscopy simulation.\n\n")
    # READ DATA FROM cobramm.xml OF FIRST DIR IN THE LIST. 
    # print warning to tell the user
    print("Reading dynamics metadata from first directory {0} (cobramm.xml and cobram.command files)...\n".format(args.directories_list[0]))
    print("WARINING! Metadata (presence of triplets, number of states, timestep etc.) will be assumed uniform for all trajectories in the list.")
    print("          In case you have trajectories with different metadata, please run a copy of this script per each group of trajectories with identical setup.\n\n")
    
    # initialize the list with the COBRAMM options
    inphard = CBF.makehard()
    inphard.insert(0, '0')
    # get the content of the spectroscopy command file
    cobcom = CBF.getCobrammCommand(args.input_file)
    # merge command soft and hard
    key_soft = CBF.ReadCobramCommand(cobcom, 'keyword', 'keywords')
    inphard = CBF.key2hard(key_soft, inphard)
    inp_soft = CBF.ReadCobramCommand(cobcom, 'command', 'commands')
    inphard = CBF.soft2hard(inp_soft, inphard)
    # list of input commands
    inp = inphard

    # transform polarization and decomposition commands if needed
    if inp[260] == '0':
        inp[260] = None
    if inp[261] == '0':
        inp[261] = None

    # set parameters for spectroscopy calculations
    
    spectroscopy = inp[250]
    method = inp[251]
    simulation_time = int(inp[253])
    delta_t = int(inp[254])
    emin = float(inp[255])
    emax = float(inp[256])
    grid = int(inp[257])
    ew = float(inp[258])
    tw = int(inp[259])
    polarization = inp[260]
    decomposition = inp[261]
    if decomposition:
        decomposition = int(decomposition)

    # control section:
    if spectroscopy not in ('UVVis', 'TRPES'):
        print("ERROR! spectroscopy type {} not recognized! Only 'UVVis' and 'TRPES' options are allowed...".format(spectroscopy))
        sys.exit(1)
    if spectroscopy == 'TRPES' and method == 'TD-DFT':
        print("ERROR! TRPES spectroscopy not available for TD-DFT...")
        sys.exit(1)
    if method not in ('CASPT2', 'CASSCF', 'TD-DFT'):
        print("ERROR! method {} not recognized! Only 'CASPT2', 'CASSCF' and 'TD-DFT' options are allowed...".format(method))
        sys.exit(1)
    if polarization not in (None, 'parallel', 'orthogonal'):
        print("ERROR! polarization type {} not recognized! Only 'off', 'parallel' and 'orthogonal' polarization options are allowed...")
        sys.exit(1)

    # open xml file
    try:
        OUT = Output(filename=args.directories_list[0]+'/cobramm.xml', parse=True)
    except IOError:
        print('ERROR: output file not found in {0} directory\nAborting...\n'.format(args.directories_list[0]))
        sys.exit(1)    

    # read cobram.command:
    with open (args.directories_list[0]+'/cobram.command', 'r') as f:
        commandlines = f.readlines()
    commandlines = [i.strip().strip('\n') for i in commandlines if i.strip().strip('\n') != '']
    #commandlines = [i for i in commandlines if i != '']

    # set number of reference (valence) states from dyn input
    nref = determine_nref(commandlines, method)

    # set some metadata from xml and command files:
    #(1)  NUMBER OF STATES
    if OUT.command_list[42] == '1':
        nsinglets = int(OUT.command_list[44])
        ntriplets = int(OUT.command_list[43])
        nstates = nsinglets + ntriplets
        print("Molecular dynamics with ISC found! Number of singlets is {0}, number of triplets is {1}".format(nsinglets, ntriplets))
    else:
        nsinglets = nref
        ntriplets = 0
        nstates = nsinglets
        print("Number of reference (valence) states in molecular dynamics is {0}".format(nsinglets))

    #(2) retrieve dyn time step (use only short one)
    # DEBUG: it would be complicated to use non-uniform timestep...for now I assume all dyn has the same timestep (command[84])
    time_step = float(OUT.command_list[84])

    # initialize PumpProbeCalculator instance pp
    pp = PumpProbeCalculator(method=method,\
                             simulation_time=simulation_time,\
                             time_step=time_step,\
                             delta_t=delta_t,\
                             nstates=nstates,\
                             trjdir_list=args.directories_list,\
                             en_min=emin,\
                             en_max=emax,\
                             grid=grid,\
                             en_width=ew,\
                             t_width=tw,\
                             nsinglets=nsinglets,\
                             spectroscopy=spectroscopy)

    if decomposition:
        selected = decomposition 
        ''' The selected state is intended in the COBRAMM convention (i.e., S0=1, S1=2 etc.).
            However, when we pass it as argument in the functions below, we use selected-1 because the active state indexes saved in cobramm.xml start from 0'''
        print("Convolution of the spectra from state {} for each time\n".format(selected))

        # create (only for TDDFT) a list of dipole moment GS -> active_state for all trajs at time 0
        all_init_tdm = pp.extract_init_tdm()

        if spectroscopy == 'TRPES':
            for step in range(0, pp.nsteps + 1, pp.delta):
                t = step * pp.time_step

                print("Convolution of the spectrum for time {}\n".format(t))
                collected_values = pp.collect_values_single_time_TRPES(nstep=step, all_init_tdm=all_init_tdm, selected_state=selected-1)

                pp.get_spectrum_single_time_all_traj(values=collected_values, currtime=t)

            print("Convoluting the final pump-probe spectrum")

            pp.time_convolution(output="spectrumPP_state_{0}.txt".format(selected))
            print("Spectrum written in 'spectrumPP_state_{0}.txt'\n".format(selected))
            print("Writing gnuplot script as 'plot_matrix_state_{0}.gp'".format(selected))
            pp.write_gnuplot(toplot="spectrumPP_state_{0}.txt".format(selected),output="plot_matrix_state_{0}.gp".format(selected))
        else:
            for transition in ('ESA', 'SE', None):
                for step in range(0, pp.nsteps + 1, pp.delta):
                    t = step * pp.time_step
    
                    #print("Convolution of the spectrum for time {}\n".format(t))
                    collected_values = pp.collect_values_single_time(nstep=step, polarization=polarization, all_init_tdm=all_init_tdm, selected_state=selected-1, transition=transition)
    
                    pp.get_spectrum_single_time_all_traj(values=collected_values, currtime=t, spectrum=transition)

                print("Convoluting the final pump-probe spectrum")
    
                if transition == None:
                    pp.time_convolution(output="spectrumPP_state_{0}_ALL.txt".format(selected))
                    print("Spectrum written in 'spectrumPP_state_{0}_ALL.txt'\n".format(selected))
                    print("Writing gnuplot script as 'plot_matrix_state_{0}_ALL.gp'".format(selected))
                    pp.write_gnuplot(toplot="spectrumPP_state_{0}_ALL.txt".format(selected), output="plot_matrix_state_{0}_ALL.gp".format(selected))
                else:
                    pp.time_convolution(output="spectrumPP_state_{0}_{1}.txt".format(selected, transition), spectrum=transition)
                    print("Spectrum written in 'spectrumPP_state_{0}_{1}.txt'\n".format(selected, transition))
                    print("Writing gnuplot script as 'plot_matrix_state_{0}_{1}.gp'".format(selected, transition))
                    pp.write_gnuplot(toplot="spectrumPP_state_{0}_{1}.txt".format(selected, transition), output="plot_matrix_state_{0}_{1}.gp".format(selected, transition))

    else:

        print("Convolution of the spectra for each time\n")

        # create (only for TDDFT) a list of dipole moment GS -> active_state for all trajs at time 0
        all_init_tdm = pp.extract_init_tdm()

        for step in range(0, pp.nsteps + 1, pp.delta):
            t = step * pp.time_step

            print("Convolution of the spectrum for time {}\n".format(t))

            if spectroscopy == 'TRPES':
                collected_values = pp.collect_values_single_time_TRPES(nstep=step, all_init_tdm=all_init_tdm)
            else:
                collected_values = pp.collect_values_single_time(nstep=step, polarization=polarization, all_init_tdm=all_init_tdm)

            pp.get_spectrum_single_time_all_traj(values=collected_values, currtime=t)

        print("Convoluting the final pump-probe spectrum")

        pp.time_convolution(output="spectrumPP.txt")
        print("Spectrum written in 'spectrumPP.txt'\n")
        print("Writing gnuplot script as 'plot_matrix.gp'")
        pp.write_gnuplot(toplot="spectrumPP.txt")

#####################################################################################################

if __name__ == '__main__':
    main()
