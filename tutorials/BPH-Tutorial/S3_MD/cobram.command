!keyword
type=mdv
nsteps=250
tstep=0.5
nproc=16
qm-type=gauss
qmem=10000MB
surhop=tully
DC=tdc
numrlx=4
hoptogs=2
?keyword

!sander
Comment
 &cntrl
  imin   = 1,
  maxcyc = 0,
  ntb    = 0,
  igb    = 0,
  ntr    = 0,
  ibelly = 1,
  cut    = 14,
  ntxo   = 1
 /
?sander

!gaussian 
#P CAM-B3LYP/6-31G* NOSYM TD(Nstates=3,root=3)

MD of BPH in water (S3)

0  1
?gaussian

!gaussadd
--link1--
%mem=1000MB
%Nproc=8
%chk=gaussian-QM
#P CAM-B3LYP/6-31G* Geom=Checkpoint Charge=Check NOSYM TD(Nstates=3,triplets,root=3)

MD of BPH in water triplet states

0  1
?gaussadd

!RATTLE
25 26
26 27
25 27
28 29
29 30
28 30
?RATTLE
