
# Tutorial #1 - Ground/excited state optimization and MD of benzophenone in H2O

This tutorial will cover MM and QM/MM setup of water solvated
*benzophenone* (BPH) to compute absorption and emission properties
with *TD-CAM-B3LYP* and *Gaussian* and finally excited state 
molecular dynamics. 

Note that this tutorial just exemplifies the usage 
of COBRAMM, it is not meant for obtaining publishable results.

The files for this tutorial are in the subdirectories:
* ``BHP.xyz`` starting sructure for QM/MM setup for BPH
  in water
* ``OPT_S0`` QM/MM input files for S0 geometry optimizations
* ``S3_MD`` QM/MM input files for 0K molecular dynamics
 
Instructions for this tutorial are available from the project 
[wiki pages](https://gitlab.com/cobrammgroup/cobramm/-/wikis/Tutorial-1).

