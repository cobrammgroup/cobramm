
# Tutorial #2: Photoisomerization of the retinal chromophore embedded in the Rhodopsin protein

This tutorial is meant to introduce some of the tasks that can be 
performed with the COBRAMM interface. The various steps considered 
here deal with a specific scientific problem: the photochemistry 
of the retinal chromophore embedded in the Rhodopsin (Rh) protein.

This tutorial will assist you from the input generation to the 
result analysis, bearing in mind that the data obtained here are 
not of adequate accuracy for a research project. The various 
calculations proposed have been indeed optimized (and eventually limited)
to access results within a reasonable computational time, 
i.e. to complete this tutorial in few days. The scope of 
this tutorial is thus to exemplify how the COBRAMM interface
can be of help in your research project and not (at all!) 
to suggest or indicate computational protocols/recipes 
(in terms of basis sets, active space, wavefunction
approach) to be used.

This tutorial contains examples of both “static” and “dynamical”
studies of the retinal photochemistry in protein environment,
using as initial structure the crystallographic data of 
Bovine Rhodopsin (Rh; PDB code: 1U19, chain A).

The files for this tutorial are in the subdirectories:
* ``COBRAMM_input_files`` input files for the 
  system structure and the QM/MM layers
* ``HF`` *COBRAMM* command file for 6-31G*/HF geometry optimization
* ``HF-STO-sp`` *COBRAMM* command file for STO-3G/HF single point
* ``CASSCF-sp`` *COBRAMM* command file for STO-3G/CASSCF single point
 
Instructions for this tutorial are available from the project 
[wiki pages](https://gitlab.com/cobrammgroup/cobramm/-/wikis/Tutorial-2).

