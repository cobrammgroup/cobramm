#!/usr/bin/env python3
# coding=utf-8

#    COBRAMM
#    Copyright (c) 2019 ALMA MATER STUDIORUM - Università di Bologna

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#####################################################################################################
import os  # filesystem utilities
import shutil  # filesystem utilities
import subprocess  # run external program as child process
import fileinput  # fileinput is used for inplace editing of files
import shlex  # simple lexical analysis for unix syntax
import math  # import mathematical functions
import re  # process output with regular expressions
import copy  # shallow and deep copy operations
import time  # provides various time-related functions

# imports of local modules

import logwrt  # write messages and output to log (std out or cobramm.log)
import constants  # physical and mathematical constants
import cobrammenv  # function to check if executables are available
from gaussianCalculator import GaussianOutput
from cobrammCalculator import CobrammCalculator, CobrammInput, CobrammOutput
from CBF import getCobrammCommand, ReadCobramCommand
from output import Output # read data from xml

# math libraries

import numpy as np  # numpy: arrays and math utilities

#try:
#    sys.path.append(os.path.join(os.environ['COBRAM_PATH'], 'cobramm'))
#except KeyError:
#    raise RuntimeError('cannot import cobramm module.\n'
#                       'Please check if $COBRAM_PATH is defined in your environment, if not run:\n'
#                       'export COBRAM_PATH=/path/to/COBRAMM')

#####################################################################################################

class SpectronError(Exception):
    """A custom exception used to report errors in use of Spectron"""

class MultiwfnError(Exception):
    """A custom exception used to report errors in use of Multiwfn"""

#####################################################################################################

class PumpProbeCalculator:

    def __init__(self, trjdir_list, method="TDDFT", simulation_time=500, start_step=0, time_step=0.5, delta_t=2, nstates=20, en_min=0.0,
                 en_max=10.0, en_width=0.15, grid=400, t_width=25, nsinglets=20, spectroscopy="UVVis"):
        """
        :param trjdir_list: list of directories containing the trajectories
        :param method: quantum mechanical method
        :param simulation_time: simulation time considered for PP simulation
        :param start_step: starting step to calculate vertical excitations from
        :param time_step: time step employed in TSH
        :param delta_t: time delay to calculate vertical excitations along the trajectories
        :param nsteps: number of simulation steps
        :param nstates: number of valence states to calculate
        :param nsinglets: number of valence singlet states
        :param en_min: minimum energy to consider in the convolution of the TR spectrum
        :param en_max: maximum energy to consider in the convolution of the TR spectrum
        :param en_width: width of the Gaussian used to convolute the spectrum along the energies
        :param grid: number of grid points to convolute the spectrum on along the energies
        :param t_width: width of the Gaussian used to convolute the spectrum along the time
        :param spectroscopy: type of transient spectroscopy for simulation (TRPES or UVVis)
        """

        #Check and define the QM method
        method = (method.upper().replace("-", ""))
        available_methods = ["TDDFT", "CASSCF", "CASPT2"]

        if method in available_methods:
            self.method = method
        else:
            raise KeyError("The method {} is not available!".format(method))

        #Initialise attributes
        self.traj = trjdir_list
        self.simtime = simulation_time
        self.delta_t = delta_t
        self.nsteps = int(simulation_time/time_step)
        self.delta = int(delta_t/time_step)
        self.time_step = time_step
        self.start_step = start_step
        self.nstates = nstates
        self.nsinglets = nsinglets
        self.min_en = en_min
        self.max_en = en_max
        self.engrid = grid
        self.enwidth = en_width
        self.twidth = t_width
        self.time, self.grid, self.spectrum, self.ESA, self.SE = [], [], [], [], []
        self.matrix = np.zeros(self.engrid)
        self.spectroscopy = spectroscopy
        self.is_SS_PT2 = False

    ####################################################
    @staticmethod
    def check_folder(folder):
        """ Check the directories where the trajectories are stored """

        ## check if the simulation is terminated normally
        termination_files = ["cobramm.xml"]

        for file in termination_files:
            if not os.path.exists("{0}/{1}".format(folder, file)):
                raise FileNotFoundError

        ##TODO: add checking functionality in case of CAS calculations

    ####################################################    
    def modify_rassi(self, original_string, ntriplets):
        ''' RASSI section must be adapted in MOLCAS to adapt to the number of singlet/triplet states 
            when the dynamics was done with ISC '''
        # Define the pattern to search for
        pattern = r'(NrOfJobs\n2 )(\d+) (\d+)\n.*?\n'
        
        # Define the replacement string
        replacement = rf'\g<1>{ntriplets} \g<3>\n' + ' '.join(map(str, range(1, ntriplets + 1))) + r'\n'
        
        # Use regular expressions to perform the replacement
        modified_string = re.sub(pattern, replacement, original_string, count=1)
    
        return modified_string

    ####################################################
    def modify_spin(self, original_string, spin):
        ''' When doind UVVis spectroscopy, the spin of the second calculation 
            has to be adapted to the spin of reference state.
            For TRPES, instead, we need to remove the spin kayword to avoid errors. '''
        if self.method in ('CASSCF', 'CASPT2'):
            # Define the pattern to search for (nact)
            nact_pattern = r'\bnact'
            
            # Define the pattern to search for (spin=3)
            spin3_pattern = r'^spin=3$'
            
            # Split the original string into lines for easier manipulation
            lines = original_string.splitlines()
            
            if spin == 3 and self.spectroscopy == 'UVVis':
                # Check if 'spin=3' is already present
                if any(re.match(spin3_pattern, line) for line in lines):
                    # 'spin=3' is already present, no modification needed
                    return original_string
                else:
                    # Add 'spin=3' above the line containing 'nact'
                    modified_lines = []
                    for line in lines:
                        if re.search(nact_pattern, line, flags=re.IGNORECASE):
                            modified_lines.append(f'spin=3\n{line}')
                        else:
                            modified_lines.append(line)
                    return '\n'.join(modified_lines)
            
            elif spin == 1 or self.spectroscopy == 'TRPES':
                # Remove the line containing 'spin=3' if present
                modified_lines = []
                spin3_found = False
                for line in lines:
                    if re.match(spin3_pattern, line):
                        spin3_found = True
                    else:
                        modified_lines.append(line)
                if spin3_found:
                    return '\n'.join(modified_lines)
                else:
                    # 'spin=3' is not present, no modification needed
                    return original_string
            
            # Return the original string if spin is neither 1 nor 3
            return original_string
        
        else:
            #TDDFT
            return re.sub("tda\(nstates","tda(Triplets,nstates",original_string)


    ####################################################
    def modify_rassi(self, original_string, ntriplets):
        ''' RASSI section must be adapted in MOLCAS to adapt to the number of singlet/triplet states 
            when the dynamics was done with ISC '''
        # Define the pattern to search for
        pattern = r'(NrOfJobs\n2 )(\d+) (\d+)\n.*?\n'
        
        # Define the replacement string
        replacement = rf'\g<1>{ntriplets} \g<3>\n' + ' '.join(map(str, range(1, ntriplets + 1))) + r'\n'
        
        # Use regular expressions to perform the replacement
        modified_string = re.sub(pattern, replacement, original_string, count=1)
    
        return modified_string

    ####################################################
    def modify_spin(self, original_string, spin):
        ''' When doind UVVis spectroscopy, the spin of the second calculation 
            has to be adapted to the spin of reference state.
            For TRPES, instead, we need to remove the spin kayword to avoid errors. '''
        if self.method in ('CASSCF', 'CASPT2'):
            # Define the pattern to search for (nact)
            nact_pattern = r'\bnact'
            
            # Define the pattern to search for (spin=3)
            spin3_pattern = r'^spin=3$'
            
            # Split the original string into lines for easier manipulation
            lines = original_string.splitlines()
            
            if spin == 3 and self.spectroscopy == 'UVVis':
                # Check if 'spin=3' is already present
                if any(re.match(spin3_pattern, line) for line in lines):
                    # 'spin=3' is already present, no modification needed
                    return original_string
                else:
                    # Add 'spin=3' above the line containing 'nact'
                    modified_lines = []
                    for line in lines:
                        if re.search(nact_pattern, line, flags=re.IGNORECASE):
                            modified_lines.append(f'spin=3\n{line}')
                        else:
                            modified_lines.append(line)
                    return '\n'.join(modified_lines)
            
            elif spin == 1 or self.spectroscopy == 'TRPES':
                # Remove the line containing 'spin=3' if present
                modified_lines = []
                spin3_found = False
                for line in lines:
                    if re.match(spin3_pattern, line):
                        spin3_found = True
                    else:
                        modified_lines.append(line)
                if spin3_found:
                    return '\n'.join(modified_lines)
                else:
                    # 'spin=3' is not present, no modification needed
                    return original_string
            
            # Return the original string if spin is neither 1 nor 3
            return original_string
        
        else:
            #TDDFT
            return re.sub("tda\(nstates","tda(Triplets,nstates",original_string)


    ####################################################

    def setup_vertical_excitations(self, ncores=1, chrg=0, basis_set="6-31g*", functional="", cluster=False, submission_string="", molcas_inputs={}, basis_block='',cartesian=False):
        """Setup of the vertical excitations"""

        init_dir = os.getcwd()
        cobcomm, cobcomm_first, cobcomm_second = "", "", ""
        #read cobram.command from current trajectory calculation
        try:
            original_cobcom = getCobrammCommand('cobram.command')
        except:
            print("Error while reading cobram.command file for trajectory {0}. Please check the presence of this file...".format(init_dir))

        #sander block is copied from original cobram.command of the trajectory (if present)
        if ReadCobramCommand(original_cobcom,'sander','sander') != []:
            sander_block = "!sander\n"
            for i in ReadCobramCommand(original_cobcom,'sander','sander'):
                sander_block += i+'\n'
            sander_block += "?sander\n\n"
        else:
            sander_block = ""

        basisfunc = 'spher'
        if cartesian:
            basisfunc = 'cart'

        #Define the cobram.command for the method chosen
        if self.method == 'TDDFT':
            software = "gauss"
            qm_string = "!gaussian\n#p {0} {1} nosym tda(nstates={2}) " \
                        "iop(9/40=4) gfinput\n\n" \
                           "gaussian input generated by COBRAMM\n\n" \
                           "{3} 1\n?gaussian".format(basis_set, functional, self.nstates, chrg)
            cobcomm = "!keyword\ntype=optxg\nnsteps=sp\nqm-type={0}\nnproc={1}\nricd=1\nbasisfunc={2}\n?keyword\n\n" \
                      "{3}\n{4}".format(software, ncores, basisfunc, sander_block, qm_string)

        elif self.method in ('CASSCF', 'CASPT2'):

            if not os.path.isfile("input_files/INPORB"):
                logwrt.fatalerror("File INPORB containing initial orbitals not found in {}!".format(init_dir+'/input_files'))
            software = "molcas"

            if basis_block.strip(" \n") != '':
                bs_block = "!basisset\n" + basis_block + "\n?basisset\n\n"
            else:
                bs_block = ''

            #FLAVIA: I do not think the 'ta' key is needed if copy of JobMix is required from template...however let's keep it for now
            cobcomm_first = "!keyword\ntype=optxg\nnsteps=sp\nqm-type={0}\nnproc={1}\nricd=1\nta=1\nbasisfunc={2}\nbasis={3}\n?keyword\n\n" \
                                    "{4}\n{5}\n{6}".format(software, ncores, basisfunc, basis_set, sander_block, molcas_inputs['ref_S'], bs_block)
            cobcomm_first_ISC = "!keyword\ntype=optxg\nnsteps=sp\nqm-type={0}\nnproc={1}\nricd=1\nta=1\nbasisfunc={2}\nbasis={3}\n?keyword\n\n" \
                                    "{4}\n{5}\n{6}".format(software, ncores, basisfunc, basis_set, sander_block, molcas_inputs['ref_T'],bs_block)
            cobcomm_second = "!keyword\ntype=optxg\nnsteps=sp\nqm-type={0}\nnproc={1}\nricd=1\nbasisfunc={2}\nbasis={3}\n?keyword\n\n" \
                                     "{4}\n{5}\n{6}".format(software, ncores, basisfunc, basis_set, sander_block, molcas_inputs['high_S'], bs_block)

        #Extract the data for each step
        with open("setup_PP.log", "w") as out:

            #create a directory for PP simulation
            try:
                os.mkdir('PumpProbe/')
            except:
                print("{0}/PumpProbe/ directory exists! Cannot overwrite...\n".format(os.getcwd()))

            # get the steps
            for step in range(self.start_step, self.nsteps+1, self.delta):

                if os.path.isdir("PumpProbe/inputs_from_step_{}".format(step)):
                    print("PumpProbe/inputs_from_step_{}/ already existing!".format(step))
                    continue

                if os.path.isdir("PumpProbe/inputs_from_step_{}".format(step)):
                    print("PumpProbe/inputs_from_step_{}/ already existing!".format(step))
                    continue

                if os.path.isdir("inputs_from_step_{}".format(step)):
                    logwrt.writewarning("overwriting previous inputs_from_step_{} directory ".format(step))
                    shutil.rmtree("inputs_from_step_{}".format(step))

                command = shlex.split("cobramm-get-step.py -n {}".format(step))
                subprocess.run(command, stdout=out, stderr=subprocess.STDOUT)

                # change directory name in case last step is included
                if os.path.isdir("inputs_from_last_step"):
                    shutil.move("inputs_from_last_step", "inputs_from_step_{}".format(self.nsteps))
                # move directory to PumpProbe/ directory
                shutil.move("inputs_from_step_{}".format(step), "PumpProbe/inputs_from_step_{}".format(step))

            # run the calculations
            for step in range(self.start_step, self.nsteps+1, self.delta):

                active_state = PumpProbeCalculator.get_active_states(step)
                multiplicity = PumpProbeCalculator.get_active_state_multiplicity(step)

                step_dir = init_dir+"/PumpProbe/inputs_from_step_{}".format(step)

                if os.path.isfile(step_dir+"/cobramm.log"):
                    print("Skipping {0}...".format(step_dir))
                    continue

                if self.method == 'TDDFT':
                    if multiplicity == 3:
                        cobcomm = self.modify_spin(cobcomm, 3)
                    os.chdir(step_dir)
                    PumpProbeCalculator.launch_cobramm(cobcomm, submission_string, cluster)
                if self.method in ('CASSCF', 'CASPT2'):

                    if step == self.start_step:
                        #copy traj starting INPORB in the step directory
                        shutil.copy("input_files/INPORB", step_dir)
                    else:
                        if not os.path.exists(step_dir+'/molcas_0.RasOrb.gz'):
                            print("Starting orbital file not found for step {}. Check if previous step has converged...".format(step))
                            exit()
                        os.system("gunzip {}/molcas_0.RasOrb.gz".format(step_dir))
                        shutil.move("{}/molcas_0.RasOrb".format(step_dir), "{}/INPORB".format(step_dir))

                    #if WF of current step was already saved, we do not need to run reference calculation
                    WF_path = init_dir + "/QM_data/molcas_" + str(step) + ".Job"
                    if self.method == "CASSCF" or self.is_SS_PT2:
                        WF_path += "Iph.gz"
                    else:
                        WF_path += "Mix.gz"

                    #DEBUG: currently, only WF of SINGLETS is saved during dynamics...fix this problem and correct the condition below
                    if os.path.exists(WF_path) and active_state < self.nsinglets:
                        shutil.copyfile(WF_path, "{0}/ref_WF.gz".format(step_dir))
                        os.system("gunzip {0}/ref_WF.gz".format(step_dir))
                        shutil.move("{0}/ref_WF".format(step_dir), "{0}/ref_WF".format(step_dir))
                        # copy also orbitals as starting orbitals for current step
                        shutil.copyfile("{0}/QM_data/molcas_{1}.RasOrb.gz".format(init_dir,step), "{0}/molcas_0.RasOrb.gz".format(step_dir))
                        # if this is not the last step, copy also orbitals as starting orbitals for next step
                        if os.path.exists(init_dir+"/PumpProbe/inputs_from_step_{}".format(step+self.delta)):
                            shutil.copyfile("{0}/QM_data/molcas_{1}.RasOrb.gz".format(init_dir,step), init_dir+"/PumpProbe/inputs_from_step_{}/molcas_0.RasOrb.gz".format(step+self.delta))
                        os.chdir(step_dir)
                        os.system("gunzip molcas_0.RasOrb.gz")
                        shutil.move("molcas_0.RasOrb", "INPORB")
                        if active_state < self.nsinglets:
                            cobcomm_second = self.modify_rassi(cobcomm_second, self.nsinglets)
                            cobcomm_second = self.modify_spin(cobcomm_second, 1)
                        else:
                            #active state is a triplet:
                            #edit rassi section of second calculation
                            #obtain number of triplets:
                            ntriplets = self.nstates - self.nsinglets
                            cobcomm_second = self.modify_rassi(cobcomm_second, ntriplets)
                            cobcomm_second = self.modify_spin(cobcomm_second, 3)

                    else:
                        #create the directory for the first calculation and move there
                        reference_dir = "reference"
                        reference_path = os.path.join(step_dir, reference_dir)
                        shutil.copytree(os.path.abspath(step_dir), os.path.abspath(reference_path))
                        os.chdir(os.path.abspath(reference_path))
                        # if active state is a singlet:
                        if active_state < self.nsinglets:
                            PumpProbeCalculator.launch_cobramm(cobcomm_first, submission_string, cluster)
                            cobcomm_second = self.modify_rassi(cobcomm_second, self.nsinglets)
                            cobcomm_second = self.modify_spin(cobcomm_second, 1)
                        else:
                            #active state is a triplet:
                            #edit rassi section of second calculation
                            #obtain number of triplets:
                            ntriplets = self.nstates - self.nsinglets
                            cobcomm_second = self.modify_rassi(cobcomm_second, ntriplets)
                            cobcomm_second = self.modify_spin(cobcomm_second, 3)
                            #run reference calc. with triplets
                            PumpProbeCalculator.launch_cobramm(cobcomm_first_ISC, submission_string, cluster)
                        os.chdir(step_dir)
                        #wait for reference calculation to finish 
                        if cluster:
                            while True:
                                if os.path.exists("{}/ref_WF".format(os.path.abspath(reference_path))):
                                    time.sleep(10)
                                    break
                        # copy: (1) final orbitals as INPORB for second calculation; (2) JobMix (ref_WF) for second calculation; (3) final orbitals as INPORB for next step
                        #(1)
                        shutil.copyfile("{}/QM_data/molcas_0.RasOrb.gz".format(os.path.abspath(reference_path)), "molcas_0.RasOrb.gz")
                        os.system("gunzip molcas_0.RasOrb.gz")
                        shutil.move("molcas_0.RasOrb", "INPORB")
                        #(2)
                        shutil.copyfile("{}/ref_WF".format(os.path.abspath(reference_path)), "ref_WF")
                        #(3) only if it is not last step
                        if os.path.exists(init_dir+"/PumpProbe/inputs_from_step_{}".format(step+self.delta)):
                            shutil.copyfile("{}/QM_data/molcas_0.RasOrb.gz".format(os.path.abspath(reference_path)), init_dir+"/PumpProbe/inputs_from_step_{}/molcas_0.RasOrb.gz".format(step+self.delta))
                    
                    # launch second calculation
                    PumpProbeCalculator.launch_cobramm(cobcomm_second, submission_string, cluster)

                os.chdir(init_dir)

    ####################################################
    @staticmethod
    def launch_cobramm(input_commands,  sub_string, cluster=False):
        with open("cobram.command", "w") as inp:
            inp.write(input_commands)
        if cluster:
            with open("qsub.log", "w") as fstdout:
                command = shlex.split("{}".format(sub_string))
                subprocess.run(command, stdout=fstdout, stderr=subprocess.STDOUT)
        else:
            with open("cobramm.log", "w") as fstdout:
                subprocess.call("cobram.py", stdout=fstdout, stderr=fstdout)
    
    ####################################################
    @staticmethod
    def get_active_states(nstep: int):
        """Extract and return the active state for a step along the trajectory"""

        # collect active states
        with open("cobramm.xml", "r") as f:
            xml_text = f.readlines()

        all_active_states = []
        for index, line in enumerate(xml_text):
            if "<state>" in line:
                state = (xml_text[index+1].split()[0])
                all_active_states.append(int(state))

        active_state = all_active_states[nstep]

        return active_state

    ###################################################
    @staticmethod
    def get_active_state_multiplicity(nstep: int):
        """Extract and return the active state multiplicityfor a step along the trajectory"""

        # collect active states
        with open("cobramm.xml", "r") as f:
            xml_text = f.readlines()

        all_mult = []
        for index, line in enumerate(xml_text):
            if "<state_multiplicity>" in line:
                mult = int(xml_text[index+1].split()[0])
                all_mult.append(int(mult))

        multiplicity = all_mult[nstep]

        return multiplicity

    ####################################################

    def extract_overlap_molcas(self, refstates, logfile="QM_data/qmALL.log"):
        ''' This function reads the overlap matrix between reference states and new manifold of states for
            UVVis transient spectroscopy.
            It retuns a n_ref * n_high list of arrays (a matrix) that stores for every reference state its composition
            in term sof the new (extended manifold)'''

        #get the number of states in second calculation (extended manifold)
        with open(logfile, "r") as log:
            out = log.read()
        state_string = "Number of root\(s\) required *([0-9]*)"
        highstates = int(re.findall(state_string, out)[0])

        #extract the matrix
        re_string = "OVERLAP MATRIX FOR THE ORIGINAL STATES:" + "(.*)" + "\+\+ Matrix elements for input states"
        find_overlap = (re.findall(re_string, out, re.DOTALL | re.IGNORECASE)[0]).rstrip().lstrip().split()
        find_overlap = [float(i) for i in find_overlap]

        # total dimension of RASSI matrix is n_ref + n_high
        total_dim = refstates + highstates
        # initialize empty matrix
        overlap = np.zeros((highstates,refstates))

        #initialize counter for row start
        start = 0
        # iterate over total RASSI matrix dimension
        for i in range(1,total_dim+1):
            # if we are beyond the diagonal n_ref*n_ref block, start saving in overlap
            if i > refstates:
                overlap[i - refstates -1] = find_overlap[start:start+refstates]
            # update start counter
            start += i

        # final matrix has n_ref columns and n_high rows.
        return overlap

    ####################################################
    @staticmethod
    def extract_tdm_energies_molcas(calcDir, method, logfile="QM_data/qmALL.log"):

        init_dir = os.getcwd()
        os.chdir(os.path.abspath(calcDir))

        #extract number of states
        with open(logfile, "r") as log:
            out = log.read()
        rassi_state_string = "Nr of states: *([0-9]*)"
        rassi_states = int(re.findall(rassi_state_string, out)[0])

        # ENERGIES:
        if method == 'CASSCF':
            re_string = "::    RASSCF root number" + "(.*)" + "\+\+    Molecular orbitals"
        elif method == 'CASPT2':
            method_string = "Type of calculation" + "(.*)" + "Fock operator"
            find_method = (re.findall(method_string, out, re.DOTALL | re.IGNORECASE)[0]).rstrip().lstrip().splitlines()
            if find_method[0] == "XMS-CASPT2":
                re_string = "Total XMS-CASPT2 energies:" + "(.*)" + "Eigenvectors:"
            elif find_method[0] == "MS-CASPT2":
                re_string = "Total MS-CASPT2 energies:" + "(.*)" + "Eigenvectors:"
            elif find_method[0] == "SS-CASPT2":
                re_string = "Total CASPT2 energies:" + "(.*)" + "  A NEW JOBIPH FILE NAMED"
        find_en = (re.findall(re_string, out, re.DOTALL | re.IGNORECASE)[0]).rstrip().lstrip().splitlines()
        QM_energies = [float(i.split()[-1]) for i in find_en]

        #extract block of text with TDM
        sizeblock = (rassi_states+6)
        nblocks = int(rassi_states / 4)
        if rassi_states % 4 != 0:
            nblocks += 1
        totlines = nblocks*3*sizeblock

        re_string = "MATRIX ELEMENTS OF 1-ELECTRON OPERATORS" + "(.*)" + "I/O STATISTICS"
        find_tdm = (re.findall(re_string, out, re.DOTALL | re.IGNORECASE)[0]).rstrip().lstrip().splitlines()
        all_tdm = find_tdm[1:totlines+1]

        ## extract components/states
        # Flavia: don't know exactly how the following block works but it worsk fine
        init_count = 5
        tdm = []
        for comp in range(0,3):
            component = []
            for state in range(1, rassi_states+1):
                count = init_count+state
                state_tdm = []
                for block in range(nblocks):
                    newcount = count + block*sizeblock
                    splitted = all_tdm[newcount].split()
                    for i in range(1,5):
                        try:
                            state_tdm.append(float(splitted[i]))
                        except IndexError:
                            break
                component.append(state_tdm)
            init_count += sizeblock * nblocks
            tdm.append(component)
        dipoles = [[[] for j in range(rassi_states)] for i in range(rassi_states)]
        ###reorganize in the same format as when reading Multiwfn
        for from_state in range(rassi_states):
            for to_state in range(from_state, rassi_states):
                dipoles[from_state][to_state] = dipoles[to_state][from_state] = [tdm[0][from_state][to_state], tdm[1][from_state][to_state], tdm[2][from_state][to_state]]

        os.chdir(init_dir)

        return np.array(dipoles), QM_energies

    ####################################################
    @staticmethod
    def extract_pt2_energies_molcas(calcDir, logfile="QM_data/qmALL.log"):

        init_dir = os.getcwd()
        os.chdir(os.path.abspath(calcDir))
        energies = []

        #extract number of states
        with open(logfile, "r") as log:
            out = log.read()
        state_string = "Number of CI roots used *([0-9]*)"
        nstates = int(re.findall(state_string, out)[0])

        ## extract the caspt2 energies
        method_string = "Type of calculation" + "(.*)" + "Fock operator"
        find_method = (re.findall(method_string, out, re.DOTALL | re.IGNORECASE)[0]).rstrip().lstrip().splitlines()
        if find_method[0] == "XMS-CASPT2":
            re_string = "Total XMS-CASPT2 energies:" + "(.*)" + "Eigenvectors"
        elif find_method[0] == "MS-CASPT2":
            re_string = "Total MS-CASPT2 energies:" + "(.*)" + "Eigenvectors"
        elif find_method[0] == "SS-CASPT2":
            re_string = "Total CASPT2 energies" + "(.*)" + " A NEW JOBIPH FILE NAMED 'JOBMIX' IS PREPARED"
        find_en = (re.findall(re_string, out, re.DOTALL | re.IGNORECASE)[0]).rstrip().lstrip().splitlines()
        final_en = find_en[:nstates]
        ## store and return only the energies
        for state in range(nstates):
            energies.append(float((final_en[state])[-13:]))
        gs = energies.pop(0)
        for state in range(nstates-1):
            energies[state] = round((energies[state] - gs) * 27.2114, 2)

        os.chdir(init_dir)

        return energies

    ####################################################

    def extract_init_tdm(self):
        ''' Returns dipole moment GS -> active_state for all trajs at time 0'''

        init_dir = os.getcwd()
        all_init_tdm = []

        for folder in self.traj:
            os.chdir(folder)
            active_state = PumpProbeCalculator.get_active_states(0)
            os.chdir("PumpProbe/inputs_from_step_0")
            try:
                if self.method == 'TDDFT':
                    dipoles = MultiwfnCalculator.extract_TDM_MWFN(".", "restart.chk")
                else:
                    molcas_dip, molcas_en = PumpProbeCalculator.extract_tdm_energies_molcas(".", self.method)
                    # uniform format of dipoles library to the one of Multiwfn used for TDDFT
                    dipoles = []
                    for i in range(len(molcas_en)):
                        for j in range(i+1,len(molcas_en)):
                            dipoles.append([i, j, molcas_dip[i][j][0], molcas_dip[i][j][1], molcas_dip[i][j][2]])
                    # calculate eV energies and osc. strangth and append to dipoles
                    for transition in dipoles:
                        Hartree = molcas_en[transition[1]] - molcas_en[transition[0]]
                        eV = Hartree * 27.2114
                        TDM_square = transition[2]**2 + transition[3]**2 + transition[4]**2
                        f = (2/3) * TDM_square * Hartree
                        transition.append(eV)
                        transition.append(f)

                init_tdm = [dipoles[active_state-1][2], dipoles[active_state-1][3], dipoles[active_state-1][4]]
                all_init_tdm.append(init_tdm)
            except:
                skip = ""
                all_init_tdm.append(skip)

            os.chdir(init_dir)

        return all_init_tdm


    ####################################################

    def extract_en_fos_polarization(self, calcDir, act_state, ref_states=None, polarization=None, init_tdm=[], apply_filter=None, ES_multiplicity=None):
        """Returns energies and oscillator strengths of transitions from active state between excited states
           calcDir      : path to directory in which the SP for PP spectroscopy was run
           ref_states   : number of states in reference calculation
           act_state    : index of active state
           polarization : activate polarization
           init_tdm     : required for TDDFT simulations, list of GS -> ES dipoles at step 0 
           apply_filter : optional. If None, no filter is apllied, if equal to 'ESA' or 'SE' only excitations (ESA) or de-excitations (SE) will be considered"""

        energies, intensities = [], []

        if self.method == "TDDFT":
            # run MultiWfn
            dipoles = MultiwfnCalculator.extract_TDM_MWFN(calcDir, "restart.chk")
        else:
            # CASSCF/CASPT2
            # extract overlap matrix
            ovlp = PumpProbeCalculator.extract_overlap_molcas(self, ref_states)
            # normalize column vecors
            column_norms = np.linalg.norm(ovlp, axis=0)
            ovlp /=  column_norms
            # extract original energies and TDMs from molcas calculation on extended manifold
            original_dipoles, original_QM_energies = PumpProbeCalculator.extract_tdm_energies_molcas(calcDir, self.method)
            # buold diagonal matrix of energies
            original_QM_energies = np.diag(original_QM_energies)
            # save number of states in extended manifols
            n_ext = len(original_QM_energies)
            # extract block of TDM matrix referring to extended manifold (i.e., exclude TDMS involving reference states)
            original_dipoles = original_dipoles[ref_states:,ref_states:]
            # based on columns of overlap matrix, extract the dimensions (in the n_ext x n_ext dimensional space), that are nor mapped yet
            # initialize empty square matrix
            A = np.zeros((n_ext, n_ext))
            # first n_ref columns of square matrix are simply taken from overlap
            A[:, :ref_states] = ovlp
            # identify directions of the n_ext dimensional space that are not spenned by reference states
            spanned_directions = []
            for i in range(ref_states):
                main_contribute = np.argmax(np.abs(A[:,i]))
                # if the main contribute is from a direction already spanned, make sure to take the second, third...etc.
                vector = np.abs(A[:,i])
                while main_contribute in spanned_directions:
                    vector = np.delete(vector, main_contribute)
                    main_contribute = np.argmax(vector)
                spanned_directions.append(main_contribute)
            # create list of missing directions
            missing_directions = [i for i in range(n_ext) if i not in spanned_directions]
            # add versors for the missing directions as new columns of the overlap matrix:
            # initialize identity matrix of same size as square matrix
            I = np.eye(n_ext)
            # add new columns to A taking them from I
            for i in range(len(missing_directions)):
                new_column = I[:,missing_directions[i]]
                A[:, ref_states + i] = new_column
            # apply Gram-Schmidt ortho-normalization to colums of A
            A = np.linalg.qr(A)[0]
            # obtain energies in the new representation by using A as change of basis
            QM_energies = np.einsum('li,lk,lj->ij', A, original_QM_energies, A, optimize=True)
            QM_energies = np.diag(QM_energies)
            # obtain TDMs in the new representation by using A as change of basis
            dipoles = np.einsum('li, lmk, mj->ijk', A, original_dipoles, A, optimize=True)
            
            # uniform format of dipoles library to the one of Multiwfn used for TDDFT
            tmp_dipoles = []
            for i in range(n_ext):
                for j in range(i+1,n_ext):
                    tmp_dipoles.append([i, j, dipoles[i][j][0], dipoles[i][j][1], dipoles[i][j][2]])
            dipoles = tmp_dipoles
            # calculate eV energies and osc. strangth and append to dipoles
            for transition in dipoles:
                Hartree = QM_energies[transition[1]] - QM_energies[transition[0]]
                eV = Hartree * 27.2114
                TDM_square = transition[2]**2 + transition[3]**2 + transition[4]**2
                f = (2/3) * TDM_square * Hartree
                transition.append(eV)
                transition.append(f)

        ### store energy and intensity for all transitions
        for transition in dipoles:
            # Stimulated emission(s)
            state = transition[1]
            if state == act_state and apply_filter in (None, 'SE'):
                # skip SE to S0 if excited state is a triplet
                if transition[0] == 0 and ES_multiplicity == 3:
                    pass
                else:
                    energies.append(transition[5])
                    intensity = -transition[6]
                    if polarization == "parallel":
                        current_tdm = transition[2:5]
                        cos = np.dot(init_tdm, current_tdm)/(np.linalg.norm(init_tdm)*np.linalg.norm(current_tdm))
                        pol_coeff = (1+(2*(cos*cos)))
                        intensity *= pol_coeff
                    elif polarization == "orthogonal":
                        current_tdm = transition[2:5]
                        cos = np.dot(init_tdm, current_tdm)/(np.linalg.norm(init_tdm)*np.linalg.norm(current_tdm))
                        pol_coeff = (2-(cos*cos))
                        intensity *= pol_coeff
                    intensities.append(intensity)

            # Excited State Absorption(s)
            state = transition[0]
            if state == act_state and apply_filter in (None, 'ESA'):
                energies.append(transition[5])
                intensity = transition[6] 
                if polarization == "parallel":
                    current_tdm = transition[2:5]
                    cos = np.dot(init_tdm, current_tdm)/(np.linalg.norm(init_tdm)*np.linalg.norm(current_tdm))
                    pol_coeff = (1+(2*(cos*cos)))
                    intensity *= pol_coeff
                elif polarization == "orthogonal":
                    current_tdm = transition[2:5]
                    cos = np.dot(init_tdm, current_tdm)/(np.linalg.norm(init_tdm)*np.linalg.norm(current_tdm))
                    pol_coeff = (2-(cos*cos))
                    intensity *= pol_coeff
                intensities.append(intensity)

        return energies, intensities

    ####################################################
    def extract_en_fos_polarization_TRPES(self, calcDir, nstates: int, active_state: int):
        """Extract Dyson Intensities between excited states"""

        with open(calcDir+"/QM_data/qmALL.log", 'r') as logfile:
            molcaslines = logfile.readlines()
        molcaslines = [i.strip('\n ') for i in molcaslines]

        n_ionic_states = 0
        for line in molcaslines:
            if "Number of root(s) required" in line:
                n_ionic_states = int(line.split()[-1])
                break

        energies = [0 for i in range(n_ionic_states)]
        intensities = [0 for i in range(n_ionic_states)]

        startindex = molcaslines.index('++ Dyson amplitudes Biorth. corrected(spin-free states):')+6
        endindex = molcaslines.index('*                                    Special properties section                                    *')-6
        DYSON_SECTION = molcaslines[startindex:endindex]
        for i in DYSON_SECTION:
            i=i.split()
            start_state = int(i[0])
            end_state = int(i[1])
            if start_state == active_state+1:
                energies[end_state-nstates-1] = float(i[2])
                intensities[end_state-nstates-1] = float(i[3])

        return energies, intensities

   ####################################################
    def collect_values_single_time(self, nstep:int, polarization=None, all_init_tdm=[], selected_state=None, transition=None):
        """Collect the UVVis spectrum values for a certain time"""

        init_dir = os.getcwd()
        # initialize empty lists for collected energies and corresponding intensities
        # they will be filled by function execution and returned as lists of floats
        collected_energies = []
        collected_intensities = []

        #get the values
        for folder in self.traj:
            try:
                os.chdir(folder)
                active_state = PumpProbeCalculator.get_active_states(nstep)

                if (selected_state == None) or (active_state == selected_state):
                
                    # correct the active state index according to singlet/triplet reference calc.
                    # and set accordingly the number of states in reference QM calculation
                    if self.method in ('CASSCF', 'CASPT2'):
                        if active_state  >= self.nsinglets:
                            QMstates = self.nstates - self.nsinglets
                            active_state -= self.nsinglets
                        else:
                            QMstates = self.nsinglets
                    else:
                        QMstates = None
                        multiplicity = PumpProbeCalculator.get_active_state_multiplicity(nstep)
                        # in case of TDDFT, we have to identify the active spece index among singlets/triplets computed by the single-point
                        # (1) get current step object from xml of dynamics
                        STEP = Output(filename='cobramm.xml', parse=True).get_step(nstep)
                        # (2) get energy of active state at the current step
                        EN =STEP.__getattr__('E_QM')[active_state]
                        # (3) load xml from PumpProbe single point
                        OUT = Output(filename="PumpProbe/inputs_from_step_{}/cobramm.xml".format(nstep), parse=True)
                        # (4) get QM energies list from single point
                        EN_LIST = OUT.grep_all('E_QM')[0]
                        # (5) read EN_LIST and compare each value with active state energy to identify which state is the active state
                        for i in range(len(EN_LIST)):
                            if np.isclose(EN_LIST[i], EN):
                                active_state = i
                                break
        
                    os.chdir("PumpProbe/inputs_from_step_{}".format(nstep))
                    print("Entering in folder {0}/PumpProbe/inputs_from_step_{1}".format(folder, nstep))
                    if polarization == "parallel" or polarization == "orthogonal":
                        # in case we have to apply polarization, save the initial TDM (pump pulse)
                        init_tdm = all_init_tdm[self.traj.index(folder)]
                    elif polarization == None:
                        init_tdm = []
        
                    en, fos = PumpProbeCalculator.extract_en_fos_polarization(self, ".", active_state, QMstates, polarization=polarization, init_tdm=init_tdm, apply_filter=transition, ES_multiplicity=multiplicity)

                    for state in range(len(en)):
                        collected_energies.append(en[state])
                        collected_intensities.append(fos[state])

            except FileNotFoundError:
                print("Folder {0}/PumpProbe/inputs_from_step_{1} not found!".format(folder, nstep))

            os.chdir(init_dir)

        return collected_energies, collected_intensities


    ####################################################

    def collect_values_single_time_TRPES(self, nstep:int, all_init_tdm=[], selected_state=None):
        """Collect the TRPES spectrum values for a certain time"""

        init_dir = os.getcwd()
        collected_energies = []
        collected_intensities = []

        #get the values
        for folder in self.traj:
            os.chdir(folder)

            try:
                active_state = PumpProbeCalculator.get_active_states(nstep)
        
                if (selected_state == None) or (active_state == selected_state):
    
                    if active_state  >= self.nsinglets:
                        QMstates = self.nstates - self.nsinglets
                        active_state -= self.nsinglets
                    else:
                        QMstates = self.nsinglets
        
                    os.chdir("PumpProbe/inputs_from_step_{}".format(nstep))
                    print("Entering in folder {0}/PumpProbe/inputs_from_step_{1}".format(folder, nstep))
            
                    en, fos = PumpProbeCalculator.extract_en_fos_polarization_TRPES(self, ".", QMstates, active_state)
            
                    for state in range(len(en)):
                        collected_energies.append(en[state])
                        collected_intensities.append(fos[state])
    
            except:
                print("Folder {0}/PumpProbe/inputs_from_step_{1} not found!".format(folder, nstep))

            os.chdir(init_dir)

        return collected_energies, collected_intensities

    ####################################################
    def get_spectrum_single_time_all_traj(self, values, currtime=0.5, spectrum='all'):
        """Convolute the spectrum values for a certain time"""

        #define grid
        spectral_grid = np.linspace(self.min_en, self.max_en, self.engrid)
        spec_value = np.zeros(len(spectral_grid))

        #convolute spectrum
        for center, intensity in zip(*values):
            linef = np.array([1. / (self.enwidth * math.sqrt(2.0 * math.pi)) * np.exp(-((e - center) ** 2) / (2.0 * (self.enwidth ** 2)))
                              for e in spectral_grid])
            to_add = intensity*linef
            spec_value += to_add

        #store in matrix format 
        self.matrix = np.vstack([self.matrix,spec_value])

        #store in list format
        for en, ints_e in zip(spectral_grid, spec_value):
            self.time.append(currtime)
            self.grid.append(en)
            if spectrum == 'ESA':
                self.ESA.append(ints_e) #/ wavelength**2)
            elif spectrum == 'SE':
                self.SE.append(ints_e) #/ wavelength**2)
            else:
                self.spectrum.append(ints_e) #/ wavelength**2)

    #####################################################
    def time_convolution(self, output="time_convolution_matrix.txt", spectrum='all'):
        """Convolute along time the spectrum obtained"""

        if spectrum == 'ESA':
            contribution = self.ESA
        elif spectrum == 'SE':
            contribution = self.SE
        else:
            contribution = self.spectrum

        #set up the grid
        index = 1
        temp_en_mat = np.zeros(self.simtime+1)
        temp_matrix = np.zeros([self.engrid, self.simtime+1])

        #convolute for each spectrum value
        for c_time, wav, ints in zip(self.time, self.grid, contribution):

            tconv_time, tconv_wav, tconv_spec = [], [], []

            #add zero values to consistency of matrices' formats
            for t in range(0, int(c_time-self.twidth)):
                tconv_time.append(t)
                tconv_wav.append(wav)
                tconv_spec.append(0.0000)

            #convolute
            tgrid = np.linspace(c_time-self.twidth, c_time+self.twidth, self.twidth*2+1)
            spec_value = np.zeros(len(tgrid))

            gau = np.array([np.exp( (- (t-c_time)**2) / ( (2*(self.twidth/2.355))**2) ) for t in tgrid])

            to_add = gau*ints
            spec_value += to_add

            #add zero values to consistency of matrices' formats
            for t, ints_e in zip(tgrid, spec_value):
                if t < 0 or t > self.simtime:
                    pass
                else:
                    tconv_time.append(t)
                    tconv_wav.append(wav)
                    tconv_spec.append(ints_e)
            for t in range(int(c_time+self.twidth), self.simtime):
                tconv_time.append(t)
                tconv_wav.append(wav)
                tconv_spec.append(0.0000)

            #store in matrix format
            sp = np.array(tconv_spec)
            if index == 1:
                temp_en_mat = np.add(temp_en_mat, sp)
            else:
                temp_en_mat = np.vstack([temp_en_mat, sp])

            if index == self.engrid:
                temp_matrix = np.add(temp_matrix, temp_en_mat)
                index = 1
                temp_en_mat = np.zeros(self.simtime+1)
            else:
                index += 1

        time_zero = np.zeros([self.engrid,1])
        final_mat = np.hstack([time_zero,temp_matrix])

        #Write matrix of the spectrum contribution given by the Gaussian obtained
        with open(output, "w") as m:
            np.savetxt(m, np.divide(final_mat,self.engrid*self.simtime), fmt='%16.12f')

    #####################################################
    def write_gnuplot(self, toplot="time_convolution_spectrum.txt", output="plot_matrix.gp"):

        gp_commands = "set title 'Time Resolved PP Spectrum eV, fs'\n" \
                      "set yrange [{0}:{1}]\n" \
                      "set xrange [0:{4}]\n" \
                      "set ylabel 'energy (eV)'\n" \
                      "set xlabel 'time (fs)'\n" \
                      "set palette defined (-1 '#00008B', -0.5 'blue', 0.5 'cyan', 1. 'yellow', 1.5 'orange', 2 'red', 2.5 'brown')\n" \
                      "set pm3d map\n" \
                      "set size square\n" \
                      "sp '{2}' u 1:({0}+$2*{3:.6f}):3 matrix title ''\n"\
                      "\n"\
                      "pause -1\n" \
                      "".format(self.min_en, self.max_en, toplot, (self.max_en-self.min_en)/self.engrid, self.simtime)

        with open(output, "w") as gp:
            gp.write(gp_commands)

#####################################################################################################
class SpectronCalculator:
    spectron_dir = ""
    multiwfn_dir = ""
    _SpectronCheck = False

    def __init__(self):
        """ the constructor of the SpectronCalculator class checks if the environment for SPECTRON execution
            is properly defined and if all the necessary executables are available """

        if not SpectronCalculator._SpectronCheck:

            # environmental variable with the COBRAMM path
            try:
                SpectronCalculator.spectron_dir = os.environ['SPECTRON_DIR']
            except KeyError:
                raise SpectronError("environment variable $SPECTRON_DIR is not defined")

            # check if COBRAMM executables are available
            for exe in ["spectron2", "iSPECTRONa.py", "iSPECTRONb.py"]:
                if not cobrammenv.which(exe):
                    raise SpectronError(exe + " executable is not available")
            SpectronCalculator._spectrumCheck = True

    ####################################################

    @staticmethod
    def runiSpectronA(tdm_file="dipoles.txt", energies_file="energies.txt", grad_files="gradient.txt",
                      modes="frequencies.txt", free=2, w1i=10000, w1f=60000, nw1=1000, signal="PP", tag="test",
                      diagrams="ESA GSB SE", t2=0):
        """A function to run iSPECTRONa.py to create the input file for Spectron

        :param tdm_file: input file containing dipoles
        :param energies_file: input file containing energies
        :param grad_files: input file containing gradients
        :param modes: input file containing normal modes and frequencies
        :param free: number of states
        :param w1i: Initial frequency (cm^-1)
        :param w1f: Final frequency (cm^-1)
        :param nw1: Number of samples along W1
        :param signal: Signal type
        :param tag: tag for the folder's name
        :return: output directory where to run Spectron
        """

        # defining output directory

        outputDir = signal + tag
        if os.path.isdir(outputDir):
            logwrt.writewarning("overwriting previous " + outputDir + " directory ")
            shutil.rmtree(outputDir)

        # running iSPECTRONa.py

        with open("iSpectronA.log", "w") as out:
            command = shlex.split(
                "iSPECTRONa.py {0} {1} {2} {3} -free {4} -w1i {5} -w1f {6} -nw1 {7} -sig {8} -tag {9} -diagrams {10} -t2 {11}".
                format(tdm_file, energies_file, grad_files, modes, free, w1i, w1f, nw1, signal, tag, diagrams, t2))
            subprocess.run(command, stdout=out, stderr=subprocess.STDOUT)

        return outputDir

    ####################################################

    @staticmethod
    def runSpectron(calcDir,
                    inputfile="input.com"):  # , simulation_time=0, time_step=0): #overwrite=False, nCores=1, store=False):
        """A function to run Spectron

        :param calcDir: directory where to run Spectron
        :param inputfile: input file for Spectron
        """

        start_dir = os.getcwd()

        # run spectron
        os.chdir(calcDir)
        with open("spectron.log", "w") as out:
            command = shlex.split("spectron2 -i {} ".format(inputfile))
            subprocess.run(command, stdout=out, stderr=subprocess.STDOUT)

        os.chdir(start_dir)

    ####################################################
    @staticmethod
    def run_pp(inpDir, outDir="2D_data", inputfile="input.com", simulation_time=int, time_step=int):

        if os.path.isdir(outDir):
            logwrt.writewarning("overwriting previous " + outDir + " directory ")
            shutil.rmtree(outDir)

        startDir = os.getcwd()
        workDir = os.path.abspath(outDir)
        pathfile = os.path.abspath(inpDir)

        # create a folder for each time step
        for step in range(0, simulation_time + 1, time_step):
            local_dir = shutil.copytree(pathfile, "{0}/t2_{1}".format(workDir, step))
            os.chdir(local_dir)

            # change the current time step
            with open(inputfile, "r") as f:
                input = f.read()

            input = input.replace("DEL_TIME2 0", "DEL_TIME2 {}".format(step))

            with open(inputfile, "w") as f:
                f.write(input)

            SpectronCalculator.runSpectron(calcDir=".")

            os.chdir(workDir)

        os.chdir(startDir)

        return outDir

    ####################################################
    @staticmethod
    def runiSpectronB(logdir, units="eV", sig2plot="PPheatmap"):

        outputDir = "result/{0}_{1}".format(sig2plot, logdir)
        if os.path.isdir(outputDir):
            logwrt.writewarning("overwriting previous " + outputDir + " directory ")
            shutil.rmtree(outputDir)
        # start_dir = os.getcwd()
        # os.chdir(logdir)

        with open("iSPECTRONb.log", "w") as out:
            command = shlex.split("iSPECTRONb.py {0} -units {1} -sig2plot {2}".
                                  format(logdir, units, sig2plot))
            subprocess.run(command, stdout=out, stderr=subprocess.STDOUT)

        return outputDir

    ###################################################

    @staticmethod
    def insert_monoexp_k(workDir, decay_time="slow", tau=0.0001, nstates=int, active_state=int, final_state=0):

        # if decay_time != "slow" or "medium" or "fast" or "custom":
        #    raise TypeError("Decay time must be 'slow', 'medium', 'fast' or 'custom' ")

        rate_constants = {"slow": 0.001, "medium": 0.02, "fast": 0.01, "custom": 1 / tau}

        KS = np.zeros((nstates, nstates))
        KS[final_state, active_state] = float(rate_constants[decay_time])
        KS[active_state, active_state] = -float(rate_constants[decay_time])

        np.savetxt("{}/transport_rates.txt".format(os.path.abspath(workDir)), KS, fmt='%1.3f')


#####################################################################################################

class readCobrammOut:
    """ The readCobrammOut class defines the object that store and print output informations collected from a Cobramm output file"""

    def __init__(self, energies=False, en_dir="", gs_dipoles=False, gs_dip_dir="", gradient=False, grad_dir="",
                 es_dipoles=False, tda_dir="", storeFiles=False):

        self.storeFiles = True

        if energies:
            self.energies = self.extract_energies_gau(os.path.join(en_dir, "QM_data", "qmALL.log"))
        else:
            self.energies = None

        if gs_dipoles:
            self.gs_dipoles, self.nstates = self.extract_TDM_gau(os.path.join(gs_dip_dir, "QM_data", "qmALL.log"))
        else:
            self.gs_dipoles = None

        if gradient:
            self.grad, self.natom, self.nroot, self.atom_list = self.extract_grads_gau(
                os.path.join(grad_dir, "QM_data", "qmALL.log"))
        else:
            self.grad = None

        if es_dipoles:
            self.gs_dipoles = None
            self.es_dipoles = self.extract_ES_TDM_gau(tda_dir)
        else:
            self.es_dipoles = None

    ####################################################
    def write_files(self, en_out="energies.txt", dip_out="dipoles.txt", grad_out="gradient"):

        if self.energies:
            with open(en_out, "w") as en:
                en.write("free format - energies\n0\n")
                for state in self.energies:
                    en.write("{}\n".format(int(state)))

        if self.gs_dipoles:
            with open(dip_out, "w") as tdm:
                tdm.write("free format - dipoles\n")
                for index in range(self.nstates):
                    state = self.gs_dipoles[index]
                    tdm.write("0 {0} {1} {2} {3}\n".format(index + 1, state[0], state[1], state[2]))

        if self.es_dipoles:
            with open(dip_out, "w") as tdm:
                tdm.write("free format - dipoles\n")
                for index in range(len(self.es_dipoles)):
                    dipole = self.es_dipoles[index]
                    tdm.write(
                        "{0} {1} {2} {3} {4}\n".format(int(dipole[0]), int(dipole[1]), dipole[2], dipole[3], dipole[4]))

        if self.grad:
            with open("{0}_S{1}.txt".format(grad_out, str(self.nroot)), "w") as gradfile:
                gradfile.write("free format - gradient\ngradient root {}\n".format(self.nroot + 1))
                for index in range(self.natom):
                    gradfile.write("{0} {1:.6f} {2:.6f} {3:.6f}\n"
                                   .format(constants.atomic_numbers[self.atom_list[index]], self.grad[0][index],
                                           self.grad[1][index], self.grad[2][index]))

                # return en_out, dip_out, gra_dout

    ####################################################

    @staticmethod
    def extract_energies_gau(file_name):
        """ use regexpr to extract the list of electronic state energies"""

        # open output file and read content
        with open(file_name, "r") as input_file:
            out_text = input_file.read()

        # excited state string
        re_string = "Excited State *[0-9]*: *.*? *([0-9]*\.[0-9]*) *eV *[0-9]*\.[0-9]* *nm *" \
                    "f=([0-9]*\.[0-9]*) *\<S\*\*2\>=[0-9]*\.[0-9]*"
        # extract results with regular expression
        results = re.findall(re_string, out_text)

        # define and return lists of electronic state energies (convert from eV to cm^-1)
        el_energies = [float(i[0]) * 8065.5443 for i in results]

        return el_energies

    ####################################################
    @staticmethod
    def extract_TDM_gau(file_name):
        """ extract the list of transition dipole moments"""

        # extract electric transition dipole moment strings
        tdm_string = "Ground to excited state transition electric dipole moments (Au)"

        # open output file and read content
        with open(file_name, "r") as f:
            out = f.read()
            f.seek(0)
            out_text = f.readlines()

        re_string = "nstates=*([0-9]*)"
        find_root = re.findall(re_string, out)
        nstates = int(find_root[0])

        # store dipoles
        dipoles = []
        for index, line in enumerate(out_text):
            if tdm_string in line:
                for i in range(nstates):
                    results = (out_text[index + 2 + i].split()[1:4])
                    dipoles.append([float(j) for j in results])

        return dipoles, nstates

    ####################################################
    @staticmethod
    def extract_ES_TDM_gau(calcDir):
        """ extract the list of transition dipole moments between excited states"""

        dipoles = MultiwfnCalculator.extract_TDM_MWFN(calcDir, "restart.chk")

        return dipoles

    ####################################################

    @staticmethod
    def get_bright_state(file_name):
        """ use regexpr to extract the brightest state"""

        # open output file and read content
        with open(file_name, "r") as input_file:
            out_text = input_file.read()

        # excited state string
        re_string = "Excited State *[0-9]*: *.*? *([0-9]*\.[0-9]*) *eV *[0-9]*\.[0-9]* *nm *" \
                    "f=([0-9]*\.[0-9]*) *\<S\*\*2\>=[0-9]*\.[0-9]*"
        # extract results with regular expression
        results = re.findall(re_string, out_text)

        # get the oscillator strengths and extract the index for the brightest state
        fos = [float(i[1]) for i in results]
        bright = fos.index(max(fos)) + 1

        return bright

    ####################################################
    @staticmethod
    def extract_grads_gau(file_name):

        # open output file and read content
        with open(file_name, "r") as grad:
            out = grad.read()
        output = out.splitlines()

        # extract which root
        re_string = "nstates=*[0-9]*, root=*([0-9]*)"
        find_root = re.findall(re_string, out)
        nroot = int(find_root[0])

        # extract gradient, number of atom and atom list in atomic numbers
        gradient = [[], [], []]
        atom_list = []
        natom = 0
        for i in range(len(output)):

            if output[i].strip() == 'Center     Atomic                   Forces (Hartrees/Bohr)':
                while True:
                    try:
                        element = output[i + natom + 3].split()
                        atom_list.append(int(element[1]))
                        gradient[0].append(-float(element[2]))
                        gradient[1].append(-float(element[3]))
                        gradient[2].append(-float(element[4]))
                    except (IndexError, ValueError):
                        break
                    natom += 1

        return gradient, natom, nroot, atom_list

############################################################################################################################################################
class MultiwfnCalculator:

    multiwfn_dir = ""
    _multiwfnCheck = False

    def __init__(self):
        """ the constructor of the SpectronCalculator class checks if the environment for SPECTRON execution
            is properly defined and if all the necessary executables are available """

        if not MultiwfnCalculator._multiwfnCheck:

        # check multiwfn and setup
            try:
                MultiwfnCalculator.multiwfn_dir = os.environ['Multiwfnpath']
            except KeyError:
                raise MultiwfnError("environment variable $Multiwfnpath is not defined")

            if not cobrammenv.which("Multiwfn"):
                raise MultiwfnError("Multiwfn executable is not available")
            MultiwfnCalculator._multiwfnCheck = True

    ##########################################################
    @staticmethod
    def extract_TDM_MWFN(calcDir, checkfile="restart.chk"):

        init_dir = os.getcwd()
        os.chdir(os.path.abspath(calcDir))
        logfile = "QM_data/qmALL.log"
        # find number of excited states
        with open(logfile, "r") as f:
            log = f.read()

        re_string = "nstates=*([0-9]*)"
        find_root = re.findall(re_string, log)
        nexstates = int(find_root[0])

        if os.path.isfile("transdipmom.txt") == False:
            # write input for mwfn
            input_string = "{0}\n18\n5\n{1}\n2\n0\nq".format(checkfile, logfile)
            with open("input_multiwfn", "w") as f:
                f.write(input_string)
                f.close()
            # run multiwfn and check
            os.system("Multiwfn < input_multiwfn > multiwfn.log")

        try:
            with open("transdipmom.txt", "r") as out:
                mwfnout = out.readlines()
        except IOError:
            raise MultiwfnError("\nSomething went wrong with Multiwfn...")

        groundstate = "Transition dipole moment between ground state (0) " \
                      "and excited states (a.u.)"
        excitedstates = "Transition dipole moment between excited states (a.u.):"
        dipoles = []
        for index, line in enumerate(mwfnout):
            if groundstate in line:
                for state in range(nexstates):
                    results = (mwfnout[index + 2 + state].split()[0:7])
                    results = [float(j) for j in results]
                    results[0], results[1] = int(results[0]), int(results[1])
                    dipoles.append(results)
            if excitedstates in line:
                newindex = int(index) + 3
                for state in range(1, nexstates):
                    remaining_states = nexstates - state
                    for i in range(remaining_states):
                        results = (mwfnout[newindex].split()[0:7])
                        results = [float(j) for j in results]
                        results[0], results[1] = int(results[0]), int(results[1])
                        dipoles.append(results)
                        newindex += 1
                    newindex += 1

        os.chdir(init_dir)

        return dipoles

    ##########################################################

    @staticmethod
    def read_TDM_MWFN(calcDir, checkfile="restart.chk"):

        init_dir = os.getcwd()
        os.chdir(os.path.abspath(calcDir))

            # define files to read
        logfile = "QM_data/qmALL.log"

            # find number of excited states

        with open(logfile, "r") as f:
            log = f.read()

        re_string = "nstates=*([0-9]*)"
        find_root = re.findall(re_string, log)
        nexstates = int(find_root[0])

        if os.path.isfile("transdipmom.txt") == False:
            # write input for mwfn
            input_string = "{0}\n18\n5\n{1}\n2\n0\nq".format(checkfile, logfile)
            with open("input_multiwfn", "w") as f:
                f.write(input_string)
                f.close()

            # run multiwfn and check
            os.system("Multiwfn < input_multiwfn > multiwfn.log")

        try:
            with open("transdipmom.txt", "r") as out:
                mwfnout = out.readlines()
        except IOError:
            raise MultiwfnError("\nSomething went wrong with Multiwfn...")

        groundstate = "Transition dipole moment between ground state (0) " \
                      "and excited states (a.u.)"
        excitedstates = "Transition dipole moment between excited states (a.u.):"
        dipoles = []
        for index, line in enumerate(mwfnout):
            if groundstate in line:
                for state in range(nexstates):
                    results = (mwfnout[index + 2 + state].split()[0:7])
                    dipoles.append([float(j) for j in results])
            if excitedstates in line:
                newindex = int(index) + 3
                for state in range(1, nexstates):
                    remaining_states = nexstates - state
                    for i in range(remaining_states):
                        results = (mwfnout[newindex].split()[0:7])
                        dipoles.append([float(j) for j in results])
                        newindex += 1
                    newindex += 1

        os.chdir(init_dir)

        return dipoles

    ##########################################################
    @staticmethod
    def read_resp_charges(calcDir, checkfile="restart.chk",state="gs"):

        init_dir = os.getcwd()
        os.chdir(os.path.abspath(calcDir))

        logfile = "QM_data/qmALL.log"

            # find number of excited states

        if state == "gs":

            if os.path.isfile("transdipmom.txt") == False:
                # write input for mwfn
                input_string = "{0}\n7\n18\n8\n1\n{1}\ny\n0\n0\nq".format(checkfile, logfile)
                with open("input_multiwfn", "w") as f:
                    f.write(input_string)
                    f.close()

                # run multiwfn and check
                os.system("Multiwfn < input_multiwfn > multiwfn.log")

            try:
                with open("restart.chg", "r") as out:
                    mwfnout = out.read().splitlines()
            except IOError:
                raise MultiwfnError("\nSomething went wrong with Multiwfn...")

            charges = []
            for q in range(len(mwfnout)):
                charges.append(mwfnout[q][40:51])

            os.chdir(init_dir)

        return charges 