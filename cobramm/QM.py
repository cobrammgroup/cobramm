#!/usr/bin/env python3
# coding=utf-8

#    COBRAMM
#    Copyright (c) 2019 ALMA MATER STUDIORUM - Università di Bologna

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#####################################################################################################

# import statements of module from python standard library

import os  # filesystem utilities
import shutil  # filesystem utilities
import subprocess  # run external program as child process
# import shelve
import math  # mathematical functions
import multiprocessing as mp  # run child processes in parallel

# math libraries
import numpy as np  # numpy library for scientific computation

# regular expressions
import re

# imports of local objects

from timer import Timer  # keep timings of the different sections of the code
from QMOutput import QMOutput  # template class for the output of a QM calculation
from gaussianCalculator import GaussianInput, GaussianOutput  # objects to handle Gaussian Input and Output
from molcasCalculator import MolcasInput, MolcasOutput
from sharcQMCalculator import SharcInterfaceInput, SharcInterfaceOutput  # objects to handle QM with SHARC QM Interfaces
from orbitals import Orbitals  # object to parse and store orbital information from QM output files

# imports of local modules

import logwrt  # manages log file output + start/end procedures
import CBF
import cobrammenv  # functions to handle third-party software environment
import constants  # values of physical constants and conversion factors

###################################################################################################################


class QM:
    """Object that generate, store and process data for the QM part of the QM/MM calculation"""

    # CLASS variables that controls the preparation of the QM_DATA_STORAGE directory
    # variable with the name of the directory where to store the QM output files (orbitals and logs)
    _QM_DATA_STORAGE = 'QM_data'
    # initialization status of the QM data storage
    _QM_DATA_SETUP = False

    @Timer("QM input")
    def __init__(self, cobcom, command, geometry, charges, step, restart=None, displacement=None,
                 setState=None, forceState=None, MolcasOptions={}, gradOnly=False, triplets=False): 
        """Constructor of the QM class. The input of the QM calculation is defined when
        the class instance is created. """

        # define a string attribute to store the log related to QM input and output, to be printed when convenient
        self.log = ""

        # when the dictionary displacement is given, use its arguments to set up a displaced modelH geometry
        # otherwise use the regular modelH defined in geometry
        if displacement is not None:
            modelHgeom = geometry.modelHdisplace(displacement["iAt"], displacement["iCoord"], displacement["iDir"],
                                                 displacement["length"], displacement["linkfollow"])
        else:
            modelHgeom = geometry.modelH

        self.tstep = None
        # following attributes are needed to activate special actions in case of PT2 calculations
        # for QM/MM X/R/MS-PT2 calculations different from SP, a second run is needed on the roatted CASSCF states to retrieve electric field
        self.PT2_QMMM = False
        #in case of SS-CASPT2 calculations requiring gradients, the &CASPT2(GRDT) and &ALASKA routines must be executed in a secondary calculation
        #(after the root order is retrieved from the first run)
        self.is_SSPT2_first_run = False
        # for nonadiabatic dynamics with THS scheme, the WF must be saved for overlap calculation at the next step
        self.save_WF = False
        #initialize dict with additional options for Molcas calculation
        self.otheropt = {}
        # initialize DoRotate attribute: it is None by default, and it will contain the text for 
        # the Do_Rotate.txt file for the second run of a CASPT2 calculation (when needed)
        self.DoRotate = None

        # process the dictionary with the other options, to check and complete it
        for key in ["MS_second_run", "SS_second_run", "NAC"]:
            if key in MolcasOptions:
                self.otheropt[key] = MolcasOptions[key]
            else:
                self.otheropt[key] = None

        # depending on the type of QM chosen with the command[51] keyword, create the
        # instance of the appropriate class that stores the information on the input file
        # create dictionary with additional options for the QM calculation
        optdict = {}
        # add common keys to optdict (software-specific kaeys will be added later for every specific interface)
        # background charges, only for a calculation with atoms in the H or M layers
        if geometry.NatomMM > 0: optdict["charges"] = geometry.pod, charges.CRG_emb
        # when this is not a single point calculation, put option to compute forces
        if command[60] != '1': #-->F: is '1' and 'sp' really equivalent everywhere in the code? CHECK!!!
            optdict["forces"] = True
            optdict["SP"] = False
            # when this is not a single step and there are M atoms, electric field computation is required
            if geometry.NatomM > 0: optdict["field"] = geometry.MEDIUM, charges.CRG_MEDIUM
        else:
            optdict["SP"] = True
        # when the restart input value contains the path of an available restart file, set restart to true
        if restart and os.path.exists(restart):
            self.log += '{0} file found! \nIt will be used ' \
                        'for initial WF guess.\n\n'.format(restart)
            optdict["restart"] = restart

        # when a specific state is requested, define the "nstate" option in optdic
        if forceState is not None:
            optdict["nstate"] = int(forceState)
            optdict["forcestate"] = True
        # this is an alternative option to set the initial state, that gives error when there is a conflict with
        # what is defined in the QM input text
        elif setState is not None:
            optdict["nstate"] = int(setState)
            optdict["forcestate"] = False

        # NOW CREATE SOFTWARE-SPECIFIC CASES AND CREATE INPUT INSTANCES

        if command[51] == '1':  # QM calculation by Gaussian

            # extract sections of the command file that are related to the QM code
            keys = CBF.ReadCobramCommand(cobcom, 'gaussian', '')  # gaussian keywords
            gen = CBF.ReadCobramCommand(cobcom, 'gen', '')  # gaussian basis set
            gaussadd = CBF.ReadCobramCommand(cobcom, 'gaussadd', '')  # gaussian additional parts
            gaussweights = CBF.ReadCobramCommand(cobcom, 'gaussweights', '')  # gaussian weights of the CASSCF
            
            # prints CI and MO coefficients in a separate file along the dynamics as they are needed for the WFO calculation
            if command[202] != '0':
                optdict["wfo"] = True
            else:
                optdict["wfo"] = False

            # increase verbosity in gaussianALL.log, do not delete AO, MO and CI definitions
            if command[2] >= '2':
                optdict["verbose"] = True
            else:
                optdict["verbose"] = False

            # option to disable nosymm control on input file
            if command[192] != '0': optdict["suppress_nosymm_error"] = True

            # when TDcouplings are requested, require tamm-damkov and force printing of all excitations
            if command[14] == '1':
                optdict["tdcouplings"] = True

            if command[103] == '0':
                optdict["suppress_TDDFT"] = True

            # for dynamics with ISC, add SOC option for the computation of singlet-triplet couplings
            if command[42] == '1' and not gradOnly:
                optdict['SOC'] = True

            # when a CI optimization is requested, set an option for the calculation of both gradients (upper and lower state)
            # CI optimization at (TD)DFT level is only available with branching plane based on gradient mean and gradient difference (no NACs)
            if command[1] == 'ci':
                if command[19] == '0':
                    self.inputData = None
                    self.outputData = None
                    logwrt.fatalerror('CI optimization with gaussian is only available with gmean branching plane! Please set command 19=1 (keyword BranchPlane=gmean)\n')
                else:
                    optdict["CIopt"] = True
                    optdict["upper_state"] = int(command[13])
            else:
                optdict["CIopt"] = False

            #if command[1] == 'optxg' and optdict["SP"] == False:
            if optdict["SP"] == False:
                # check that the requested root in cobram.command matches the requested state in gaussian input
                optstate = int(command[13])
                gauss_state = 1
                TDstate = re.search("root=(\d+)", keys[0])
                if TDstate:
                    gauss_state = int(TDstate[1])+1
                if optstate != gauss_state and 'casscf' not in keys[0].lower():
                    logwrt.fatalerror("Ambiguity in target state for optimization! key 'numrlx' (command 13) differes from requested root in Gaussian input...\n")

            # save instance of GaussianInput as the input file for the QM calculation
            self.inputData = GaussianInput(keys, gen, gaussadd, gaussweights, modelHgeom,
                                           geometry.getAtomLabels("modelH"), optdict)

        elif command[51] == '6':  # QM calulation by Molcas
            # extract sections of the command file that are related to the QM code
            keys = CBF.ReadCobramCommand(cobcom, 'molcas', '')  # molcas keywords
            basisset = CBF.ReadCobramCommand(cobcom, 'basisset', '')  # user-defined molcas basis set (optional)
            sewardkey = CBF.ReadCobramCommand(cobcom, 'seward', '')  # additional seward keywords added through !sewars section of cobram.command

            #basis set from !command (or !keyword) section, to use for molcas (unless non standard basis set is specified through !bassset section)
            optdict["basis"] = command[197]

            # type of basis functions
            if command[194] == '1':
                optdict["cartesian"] = True
            else:
                optdict["cartesian"] = False

            # use Cholesky decomposition
            if command[196] == '1':
                optdict["ricd"] = True
                optdict["cdth"] = command[195]

            # pass command[1] (i.e. type of calculatio ) to MolcasInput to build appropriate input file
            optdict['command1'] = command[1]
            #in case of MDV, pass hopping scheme to MolcasInput for appropriate input construction
            if command[1] == 'mdv':
                if int(command[85]) == 1:
                    # Tully with THS time-derivative coupling
                    if int(command[14]) == 1:
                        optdict['mdv_type'] = 1
                    # Tully with NACs 
                    else:
                        optdict['mdv_type'] = 2
                # energy gap-based hop
                elif int(command[85]) == 2:
                    optdict['mdv_type'] = 3
                else:
                    optdict['mdv_type'] = 0

            # for CI optimizations: define type of branching plane (NAC-based or gradients-only)
            if command[19] == '1':
                optdict["newBP"] = True

            # True if this is the second QM run for MS-XMS-RMS PT2 QM/MM (to retrieve electric field)
            if self.otheropt['MS_second_run'] or self.otheropt['SS_second_run']:
                optdict["ROST"] = True
            if command[81] != '0':
                optdict["highest_NAC_state"] = int(command[81])
            if gradOnly:
                optdict["gradOnly"] = True
            if self.otheropt['NAC']:
                #argument NAC (when present) is a list of two elements, identifying the old and new state of a hopping event
                #these are passed to MolcasInput through optdict so that NAC is calculated after hop and we can rescale velocity
                optdict["NAC_for_vel_rescale"] = self.otheropt['NAC']
            if triplets:
                #in case this is the triplet calculation of mdv with ISC, tell Molcas how many triplet states to include
                optdict["ISC"] = [int(command[44]), int(command[43])]
                self.triplets = True
            else:
                self.triplets = False

            # save instance of MolcasInput as the input file for the QM calculation
            self.inputData = MolcasInput(keys, basisset, sewardkey, modelHgeom,
                                           geometry.getAtomLabels("modelH"), optdict, step)
            # set an attributes to remember if a second PT2 run is needed
            # (in case of QM/MM calculations different from SP a second run is needed to retrieve electric field)
            if (self.inputData.calctype in ['MSPT2', 'XMSPT2', 'RMSPT2']) and not optdict["SP"] and (geometry.NatomMM > 0) and not self.otheropt['MS_second_run']:
                self.PT2_QMMM = True
            # (in case of SS-CASPT2 calculations different from SP a second run is needed to retrieve gradient of the correct state, after checking possible root flipping)
            if (self.inputData.calctype == 'SSPT2') and not optdict["SP"] and not self.otheropt['SS_second_run']:
                self.is_SSPT2_first_run = True
            # in case of nonadiabatic dynamics (command 1 + command 85) with THS scheme (command 14)
            if self.inputData.calctype in ['SSPT2', 'MSPT2', 'XMSPT2', 'RMSPT2'] \
               and command[1] == 'mdv' \
               and int(command[85]) > 0 \
               and command[14] == '1' \
               and not self.otheropt['MS_second_run'] \
               and not self.is_SSPT2_first_run:
                self.save_WF = True
            #for CASSCF, only for triplets in ISC we need to save JobIphs
            if self.inputData.calctype == 'CAS' \
               and command[1] == 'mdv' \
               and int(command[85]) > 0 \
               and command[14] == '1' \
               and self.triplets:
                self.save_WF = True

        elif command[51] == '7':  # QM calulation by MOLPRO
            # Molpro is not yet supported by the QM class
            self.inputData = None

        elif command[51] == '11':  # QM calulation with SHARC INTERFACES

            qmcode = command[110]

            if forceState is not None:
                logwrt.fatalerror("cannot change electronic state in QM calculation with SHARC interface")

            # background charges and electric field computation, only for a calc with atoms in the H or M layers
            if geometry.NatomMM > 0:
                optdict["charges"] = geometry.pod, charges.CRG_emb, geometry.pod_mobileatoms
                if qmcode.upper() in ["MOLCAS"]:
                    optdict["field"] = geometry.MEDIUM, charges.CRG_MEDIUM

            self.inputData = SharcInterfaceInput(qmcode, modelHgeom, geometry.getAtomLabels("modelH"), optdict)

        else:
            # other type of calculations are not supported by this QM class
            self.inputData = None

        # initialize an empty outputData container, that will be used to store the result of the QM calculation
        self.outputData = QMOutput()
        # initialize another attribute to store the orbitals, for the QM for which orbital parsing is available
        self.orbitals = None

    # =============================================================================================================
    
    def __del__(self):
        """Destructor for the QM class: destroys the input data (defined on instance construction)
        and the output data, when defined"""

        # destroy input data
        try:
            del self.inputData
            # when available, destroy output data
            if self.outputData: del self.outputData
        except AttributeError:
            pass

    # =============================================================================================================

    def setOutputData(self, energy, gradient, state, charges, dipole, selfenergy, gradcharges, NAC):
        """Workaround to define the QM instance from QM results that have been already obtained
        elsewhere, in order to use the QM class as a simple data storage object in cases where the QM
        code is not fully implemented"""

        # store list of the electronic energies and the corresponding state ID (GS = 1, ES = 2,3,4... )
        # data is stored as a dictionary {state_number : energy_of_the_state}
        self.outputData.set("energy", {i: en for i, en in enumerate(energy)})
        # store the gradient of the electronic state of interest
        self.outputData.set("gradient", {i: grad for i, grad in zip(state, gradient)})
        # store information on the state that has been optimized in the QM calculation
        self.outputData.set("optstate", state[0])
        # store the electrostatic output information
        self.outputData.set("charges", charges)
        self.outputData.set("dipole", dipole)
        # store the energy/gradient components of the electrostatic embedding
        self.outputData.set("selfenergy", selfenergy)
        self.outputData.set("gradcharges", {i: grad for i, grad in zip(state, gradcharges)})
        self.outputData.set("nac", {i: {j: NAC[i][j] for j in range(len(NAC[i]))} for i in range(len(NAC))})

    # =============================================================================================================

    def setTstep(self, tstep):
        self.tstep = tstep

    # =============================================================================================================

    def setTDC(self, tdc):
        self.outputData.set("tdc", tdc)

    # =============================================================================================================

    # TODO: extend the behaviour of the __getattribute__ to handle in more generic and
    #  rational way the code to get access to the output data stored in self.outputData...
    #  the following methods energy(), gradient(), gradcharges(), energydict(), ... dipole()
    #  should be revisited.
    #  - For most of the attributes, one can simply use the name of the
    #  attribute as argument of a self.outputData.get() call
    #  - In the case of energy/gradient/gradcharges, it should be decided what is
    #  the default behaviour of the attribute (full dictionary vs optimized element)

    def energy(self, nstate=None):
        """Return a list containing the electronic state energy (or energies) computed with the QM calculation """

        # get the dictionary that contains the QM energy of the states
        energydict = self.outputData.get("energy")

        try:  # exception handler, return None if that state requested from input is not available
            # when nstate is None, no state is requested... return the full list in order of increasing energy
            if nstate is None:
                return [energydict[n] for n in sorted(energydict.keys())]
            # if nstate = "opt", then return the energy of the state that has been optimized in QM calculation
            elif nstate == "opt":
                return energydict[self.outputData.get("optstate")]
            # otherwise return the energy of the state requested as input
            else:
                return energydict[nstate]
        except (KeyError, TypeError):
            return None

    # =============================================================================================================

    def gradient(self, nstate=None):
        """Return the gradient on the QM part (or gradients) computed with the QM calculation """

        # get the dictionary that contains the QM gradient of the states computed with QM
        graddict = self.outputData.get("gradient")

        try:  # exception handler, return None if that state requested from input is not available
            # when nstate is None, no state is requested... return the full list in order of increasing energy
            if nstate is None:
                return [graddict[n] for n in sorted(graddict.keys())]
            # if nstate = "opt", then return the gradoemt of the state that has been optimized in QM calculation
            elif nstate == "opt":
                return graddict[self.outputData.get("optstate")]
            # otherwise return the gradient of the state requested as input
            else:
                return graddict[nstate]
        except (KeyError, TypeError):
            return None

    # =============================================================================================================

    def nac(self, nstate1=None, nstate2=None):
        """Return the NAC on the QM part computed with the QM calculation """

        # get the dictionary that contains the QM nac of the states computed with QM
        nacdict = self.outputData.get("nac")

        try:  # exception handler, return None if that state requested from input is not available
            # when nstate is None, no state is requested... return the full list in order of increasing energy
            if nstate1 is None or nstate2 is None:
                logwrt.fatalerror("Looks like this calculation needs a NAC that is not available...please check input\n")
            # otherwise return the nac of the state requested as input
            else:
                return nacdict[nstate1][nstate2]
        except (KeyError, TypeError):
            return None

    # =============================================================================================================

    def gradcharges(self, nstate=None):
        """Return the gradient of the QM-MM electrostatic energy wrt the position of the background charges """

        # get the dictionary that contains the QM gradient of the states computed with QM
        graddict = self.outputData.get("gradcharges")

        try:  # exception handler, return None if that state requested from input is not available
            # when nstate is None, no state is requested... return the full list in order of increasing energy
            if nstate is None:
                return [graddict[n] for n in sorted(graddict.keys())]
            # if nstate = "opt", then return the energy of the state that has been optimized in QM calculation
            elif nstate == "opt":
                return graddict[self.outputData.get("optstate")]
            # otherwise return the energy of the state requested as input
            else:
                return graddict[nstate]
        except (KeyError, TypeError):
            return None

    # =============================================================================================================

    @property
    def energydict(self):
        """Return the dictionary with the energy results of the QM calculation """
        return self.outputData.get("energy")

    # =============================================================================================================

    @property
    def gradientdict(self):
        """Return the dictionary with the gradient results of the QM calculation """
        return self.outputData.get("gradient")

    # =============================================================================================================

    @property
    def charges(self):
        """Return a list containing the atomic charges computed with the QM calculation """
        return self.outputData.get("charges")

    # =============================================================================================================

    @property
    def selfenergy(self):
        """Return a list containing the self-energy of the background charge distribution """
        return self.outputData.get("selfenergy")

    # =============================================================================================================

    @property
    def dipole(self):
        """Return a list containing the x,y,z components + norm of the dipole moment computed with the QM calc """
        return self.outputData.get("dipole")

    # =============================================================================================================

    @staticmethod
    def _prepareQMDataStorage():
        """Function to initialize the QM_DATA_STORAGE directory, to be called at the
        before running any QM calculation. The function cleans up the storage dir and remove
        files that might remain from previous run of COBRAMM. """

        if not QM._QM_DATA_SETUP:  # the data storage is not set up yet...

            # remove existing QM_DATA_STORAGE dir, which has been left by another cobramm execution
            if QM._QM_DATA_STORAGE in os.listdir('.'):
                shutil.rmtree(QM._QM_DATA_STORAGE)

            # create a new directory
            os.mkdir(QM._QM_DATA_STORAGE)

            # set initialization status variable to True
            QM._QM_DATA_SETUP = True

    # =============================================================================================================

    def archiveStep(self, copyLog, copyOrb, step):
        """ save output and orbital files for the calculation defined in the instance of QM:
        the log files is appended to the QMall.log file, and the orbitals files are copied
        depending on the type of QM used. copyLog and copyOrb are logical flags: when True,
        log and orbital files are saved, respectively. Step is an integer number that is used to label
        the information that is saved, and tipically is the step number. """

        # initialize data storage
        QM._prepareQMDataStorage()

        # file where to store QM results
        allName = os.path.join(QM._QM_DATA_STORAGE, 'qmALL.log')

        # check if the QM calculation output exists, otherwise print a warning to screen
        if self.outputData is None:
            logwrt.writewarning("QM output is not available, log and orbital files will not be saved")
            return

        if copyLog:
            # write the content of the QM single point calc in the ALL file, decorated with
            # comments that highligh the step number of the QM single point
            with open(allName, 'a') as qmall:
                qmall.write('=' * 80 + '\n')
                qmall.write("Start QM calculation output of STEP : " + str(step) + "\n")
                qmall.write(' || ' * 20 + '\n' + ' \/ ' * 20 + '\n')
                qmall.write(self.outputData.get("outfile"))
                qmall.write(' /\ ' * 20 + '\n' + ' || ' * 20 + '\n')
                qmall.write("End QM calculation output of STEP : " + str(step) + "\n")
                qmall.write('=' * 80 + '\n' + '\n')

        # copy orbitals  when required
        if copyOrb:
            # loop over the files that one needs to copy to save the orbitals
            for srcf in self.outputData.orbfiles():
                # process name to extract path, name and extension of the file
                path, filename = os.path.split(srcf)
                basename, ext = os.path.splitext(filename)
                # define the name of the destination file
                tarf = os.path.join(QM._QM_DATA_STORAGE, "{0}_{1}".format(basename, step) + ext)
                # save orbital file with a name that contains the step number, and then gzip it
                shutil.copy(srcf, tarf)
                CBF.GZIP(tarf)

    # =============================================================================================================

    def saveRestartFile(self, name='restart'):
        """Save a copy of the file for orbital restart that is appropriate for the type
        of QM calculation that has been done, and return to caller the full path of the
        file."""

        # name of the file for restart
        srcf = self.outputData.restartfile()

        # if the restart file is available
        if srcf is not None:

            # process name to extract path, name and extension of the file
            path, filename = os.path.split(srcf)
            basename, ext = os.path.splitext(filename)

            # define the name of the destination file
            tarf = os.path.join(os.getcwd(),  name + ext)
            # save orbital file with the name defined above and then return the path
            shutil.copy(srcf, tarf)
            return tarf

        # if the file is not available, do nothing and return none
        else:
            return None
        
    # =============================================================================================================

    def getDoRotate(self):
        '''This function extracts the eigenvectors from a CASPT2 calculation, and returns the string for the
        Do_Rotate.txt file to be written in the next calculation'''
        Do_Rotate_string = ''
        for row in self.outputData.get("eigenvectors").T:
            for col in row:
                Do_Rotate_string += "{0:12.8f} ".format(col)
            Do_Rotate_string += "\n"
        Do_Rotate_string += " CMS-PDFT"
        
        return  Do_Rotate_string

    # =============================================================================================================

    def saveMolcasFiles(self):
        """Save a copy of the Molcas files needed for next step of the calculation."""

        # WF needs to be saved sometimes (cases are identified by the following conditions)
        if self.save_WF and not self.inputData.otheropt["gradOnly"]:
            # name of the WF file corresponding to calculation type
            srcf = self.outputData.WFfile()
            if srcf is not None:
                # process name to extract path, name and extension of the file
                path, filename = os.path.split(srcf)
                basename, ext = os.path.splitext(filename)
                # define the name of the destination file
                if self.triplets:
                    dest_name = "Prev_triplet"
                else:
                    dest_name = "Prev"
                tarf = os.path.join(os.getcwd(), dest_name + ext)
                # save orbital file with the name defined above and then return the path
                shutil.copy(srcf, tarf)
            if '.JobMix' in srcf and self.triplets:
                #in case of (X)(R)MS-CASPT2 for triplets we need to save also triplet JobIph for CIREstart
                try:
                    shutil.copy(os.path.join(path, 'molcas.JobIph'), os.path.join(os.getcwd(), 'Prev_triplet.JobIph'))
                except:
                    shutil.copy(os.path.join(path, 'JOBIPH'), os.path.join(os.getcwd(), 'Prev_triplet.JobIph'))

    # =============================================================================================================

    @staticmethod
    def copyMolcasFiles(rundir, qm):
        """ This function copies the 'extra' Do_Roate or WF files (from previous calculations) for running Molcas QM
         WF files are needed by RASSI in case of dynamics with TDC coupling and/or SOC """

        # if the text for a Do_Rotate.txt file was set, write the file int rundir
        if qm.DoRotate:
            with open(os.path.join(rundir, 'Do_Rotate.txt'), 'w') as DoRotate_file:
                DoRotate_file.write(qm.DoRotate)
        
        # CASSCF calculations:
        # copy WF of triplets at previous step (in case of ISC)
        if qm.inputData.calctype == 'CAS' and qm.save_WF:
            if os.path.exists("Prev_triplet.JobIph"):
                if qm.inputData.otheropt["gradOnly"]:
                    # in case this is the gradient calculation after a hop, we need previous triplet JobIph as restart
                    # however, we have to keep it in maindir as well for next step (so copy)
                    shutil.copy("Prev_triplet.JobIph", rundir)
                else:
                    #in any other case, we can use move
                    shutil.move("Prev_triplet.JobIph", rundir)
        # SS-CASPT2 calculations:
        # copy WF of triplets but also manage WF of singlets in case of TDC scheme       
        elif qm.inputData.calctype == 'SSPT2':
            if qm.triplets:
                # in case this is the first run of SS-PT2 or it is the gradient calc after a hop, we need previous WF of triplet as restart
                # however, we have to keep it in maindir as well for next triplet QM run (so copy)
                if qm.is_SSPT2_first_run or qm.inputData.otheropt["gradOnly"]:
                    if os.path.exists("Prev_triplet.JobIph"):
                        shutil.copy("Prev_triplet.JobIph", rundir)
                else:
                    #in any other case, we can use move (for triplet WF)
                    # for singlet WF, instead, use copy because we need to keep file for next singlet QM run
                    if os.path.exists("Prev_triplet.JobIph") and os.path.exists("Prev.JobIph"):
                        shutil.move("Prev_triplet.JobIph", rundir)
                        shutil.copy("Prev.JobIph", rundir+"/singlet.JobIph")
            else:
                # we are computing singlets (second run) and this is not a gradient calculation after a hop: use move
                if not qm.is_SSPT2_first_run and not qm.inputData.otheropt["gradOnly"] and os.path.exists("Prev.JobIph"):
                    shutil.move("Prev.JobIph", rundir+"/QMPrev.JobIph")

        elif qm.inputData.calctype in ['MSPT2', 'XMSPT2', 'RMSPT2']:
            if qm.otheropt["MS_second_run"] is not True:
                if qm.triplets:
                    if os.path.exists("Prev_triplet.JobIph") and qm.inputData.otheropt["gradOnly"]:
                        shutil.copy("Prev_triplet.JobIph", rundir)
                    elif os.path.exists("Prev_triplet.JobMix") and os.path.exists("Prev.JobMix") and os.path.exists("Prev_triplet.JobIph"):
                            shutil.move("Prev_triplet.JobMix", rundir)
                            shutil.move("Prev_triplet.JobIph", rundir)
                            shutil.copy("Prev.JobMix", rundir+"/singlet.JobMix")
                else:
                    if os.path.exists("Prev.JobMix") and not qm.inputData.otheropt["gradOnly"]:
                        shutil.move("Prev.JobMix", rundir+"/QMPrev.JobMix")


    # =============================================================================================================

    def getDoRotate(self):
        '''This function extracts the eigenvectors from a CASPT2 calculation, and returns the string for the
        Do_Rotate.txt file to be written in the next calculation'''
        Do_Rotate_string = ''
        for row in self.outputData.get("eigenvectors").T:
            for col in row:
                Do_Rotate_string += "{0:12.8f} ".format(col)
            Do_Rotate_string += "\n"
        Do_Rotate_string += " CMS-PDFT"
        
        return  Do_Rotate_string

    # =============================================================================================================

    def saveMolcasFiles(self):
        """Save a copy of the Molcas files needed for next step of the calculation."""

        # WF needs to be saved sometimes (cases are identified by the following conditions)
        if self.save_WF and not self.inputData.otheropt["gradOnly"]:
            # name of the WF file corresponding to calculation type
            srcf = self.outputData.WFfile()
            if srcf is not None:
                # process name to extract path, name and extension of the file
                path, filename = os.path.split(srcf)
                basename, ext = os.path.splitext(filename)
                # define the name of the destination file
                if self.triplets:
                    dest_name = "Prev_triplet"
                else:
                    dest_name = "Prev"
                tarf = os.path.join(os.getcwd(), dest_name + ext)
                # save orbital file with the name defined above and then return the path
                shutil.copy(srcf, tarf)
            if '.JobMix' in srcf and self.triplets:
                #in case of (X)(R)MS-CASPT2 for triplets we need to save also triplet JobIph for CIREstart
                try:
                    shutil.copy(os.path.join(path, 'molcas.JobIph'), os.path.join(os.getcwd(), 'Prev_triplet.JobIph'))
                except:
                    shutil.copy(os.path.join(path, 'JOBIPH'), os.path.join(os.getcwd(), 'Prev_triplet.JobIph'))

    # =============================================================================================================

    @staticmethod
    def copyMolcasFiles(rundir, qm):
        """ This function copies the 'extra' Do_Roate or WF files (from previous calculations) for running Molcas QM
         WF files are needed by RASSI in case of dynamics with TDC coupling and/or SOC """

        # if the text for a Do_Rotate.txt file was set, write the file int rundir
        if qm.DoRotate:
            with open(os.path.join(rundir, 'Do_Rotate.txt'), 'w') as DoRotate_file:
                DoRotate_file.write(qm.DoRotate)
        
        # CASSCF calculations:
        # copy WF of triplets at previous step (in case of ISC)
        if qm.inputData.calctype == 'CAS' and qm.save_WF:
            if os.path.exists("Prev_triplet.JobIph"):
                if qm.inputData.otheropt["gradOnly"]:
                    # in case this is the gradient calculation after a hop, we need previous triplet JobIph as restart
                    # however, we have to keep it in maindir as well for next step (so copy)
                    shutil.copy("Prev_triplet.JobIph", rundir)
                else:
                    #in any other case, we can use move
                    shutil.move("Prev_triplet.JobIph", rundir)
        # SS-CASPT2 calculations:
        # copy WF of triplets but also manage WF of singlets in case of TDC scheme       
        elif qm.inputData.calctype == 'SSPT2':
            if qm.triplets:
                # in case this is the first run of SS-PT2 or it is the gradient calc after a hop, we need previous WF of triplet as restart
                # however, we have to keep it in maindir as well for next triplet QM run (so copy)
                if qm.is_SSPT2_first_run or qm.inputData.otheropt["gradOnly"]:
                    if os.path.exists("Prev_triplet.JobIph"):
                        shutil.copy("Prev_triplet.JobIph", rundir)
                else:
                    #in any other case, we can use move (for triplet WF)
                    # for singlet WF, instead, use copy because we need to keep file for next singlet QM run
                    if os.path.exists("Prev_triplet.JobIph") and os.path.exists("Prev.JobIph"):
                        shutil.move("Prev_triplet.JobIph", rundir)
                        shutil.copy("Prev.JobIph", rundir+"/singlet.JobIph")
            else:
                # we are computing singlets (second run) and this is not a gradient calculation after a hop: use move
                if not qm.is_SSPT2_first_run and not qm.inputData.otheropt["gradOnly"] and os.path.exists("Prev.JobIph"):
                    shutil.move("Prev.JobIph", rundir+"/QMPrev.JobIph")

        elif qm.inputData.calctype in ['MSPT2', 'XMSPT2', 'RMSPT2']:
            if qm.otheropt["MS_second_run"] is not True:
                if qm.triplets:
                    if os.path.exists("Prev_triplet.JobIph") and qm.inputData.otheropt["gradOnly"]:
                        shutil.copy("Prev_triplet.JobIph", rundir)
                    elif os.path.exists("Prev_triplet.JobMix") and os.path.exists("Prev.JobMix") and os.path.exists("Prev_triplet.JobIph"):
                            shutil.move("Prev_triplet.JobMix", rundir)
                            shutil.move("Prev_triplet.JobIph", rundir)
                            shutil.copy("Prev.JobMix", rundir+"/singlet.JobMix")
                else:
                    if os.path.exists("Prev.JobMix") and not qm.inputData.otheropt["gradOnly"]:
                        shutil.move("Prev.JobMix", rundir+"/QMPrev.JobMix")


    # =============================================================================================================

    @staticmethod
    def _availablePathForQM():
        """Returns a path corresponding to an empty directory where the QM calculation
        can be run by the runQM function. The directory is created by this function."""

        qmID = 0
        while True:
            # increment an ID variable that labels the qm directory
            qmID += 1
            # define the name of the directory
            qmdir = "qmCalc{0:05d}".format(qmID)
            # check whether the directory exists, if it does not returns it to caller function
            if not os.path.isdir(qmdir): break

        # create the directory
        os.mkdir(qmdir)
        return qmdir

    # =============================================================================================================

    @staticmethod
    def _lauchMolcasQM(rundir, filename, qmexe):
        """ This simple function executes the command for running Gaussian QM
         It is needed to store the functions to execute for a parallel execution """

        # store starting dir
        startdir = os.getcwd()
        # move to the directory of the calculation
        os.chdir(rundir)

        # environmental variables for MOLCAS execution
        os.putenv('Project', 'molcas')
        os.putenv('WorkDir', os.getcwd())
        os.putenv('MOLCAS_OUTPUT', os.getcwd())

        # run qm calculation
        finp = filename + ".com"
        with open(filename + ".log", "w") as fout:
                with open(filename + ".err", "w") as ferr:
                    subprocess.call(["nice", "-1", qmexe, finp], stdout=fout, stderr=ferr)

        # move to the directory of the calculation
        os.chdir(startdir)

    # =============================================================================================================

    @staticmethod
    def _lauchGaussianQM(rundir, filename, qmexe):
        """ This simple function executes the command for running Gaussian QM
         It is needed to store the functions to execute for a parallel execution """

        # store starting dir
        startdir = os.getcwd()
        # move to the directory of the calculation
        os.chdir(rundir)

        # run qm calculation
        with open(filename + ".com", "r") as finp:
            with open(filename + ".log", "w") as fout:
                with open(filename + ".err", "w") as ferr:
                    subprocess.call(["nice", "-1", qmexe], stdin=finp, stdout=fout, stderr=ferr)

        # move to the directory of the calculation
        os.chdir(startdir)

    # =============================================================================================================

    @staticmethod
    def _lauchPysoc(rundir, filename, qmexe, G16out):
        """ This simple function executes the command for running Pysoc to get SOC between DFT states """

        # store starting dir
        startdir = os.getcwd()
        # move to the directory of the calculation
        os.chdir(rundir)

        # run qm calculation
        with open(filename + ".log", "w") as fout:
            with open(filename + ".err", "w") as ferr:
                subprocess.call(["nice", "-1", qmexe, G16out], stdout=fout, stderr=ferr)

        # move to the directory of the calculation
        os.chdir(startdir)

    # =============================================================================================================

    @staticmethod
    def readPysoc(pysoclog, multiplicities):
        """ This function returns the SOC matrix out of Pysoc log file 
            pysoclog       : pysoc log file (path to)
            multiplicities : list of excited state multiplicities from gaussian """

        # store output lines
        with open(pysoclog) as f:
            log = f.readlines()

        # read number of states
        nsinglets = int(re.search("S\((\d+)\)", log[-1])[1])
        ntriplets = int(re.search("T\((\d+)\)", log[-1])[1])

        # create list of labels
        labels = {'S(0)':0}
        Scount, Tcount = 1, 1
        for k in multiplicities.keys():
            if multiplicities[k] == 'Singlet':
                label = 'S(' + str(Scount) + ')'
                labels[label] = k
                Scount += 1
            else:
                label = 'T(' + str(Tcount) + ')'
                labels[label] = k
                Tcount += 1
        
        # initialize SOC matrix (complex to have uniform format with Molcas, but here we will only have real numbers)
        SOC = np.zeros((len(labels), len(labels)))

        # read log line by line (start after header) and store values
        for line in log[2:]:
            line = line.split()
            try:
                state_i = labels[line[0]]
                state_j = labels[line[1]]
                SOC[state_i][state_j] = SOC[state_j][state_i] = (float(line[-3]) + float(line[-2]) + float(line[-1])) * constants.wavnr2au
            except KeyError:
                pass

        return SOC

    # =============================================================================================================

    @staticmethod
    @Timer("QM run")
    def runQM(QMcalculations, step=0, memory="500MB", nprocQM=1, nprocParall=1):
        """Given a list of input files, defined as instances of the QM class, run the QM program
        and extract the relevant results from the output files, that are then saved in the QM instances
        themselves and can be later read as attributes of the instances"""

        # make argument a list
        if type(QMcalculations) is not list: QMcalculations = [QMcalculations]

        # for SHARC, the parallel calculation is not possible
        if np.any([isinstance(qm.inputData, SharcInterfaceInput) for qm in QMcalculations]):
            logwrt.writewarning("parallel execution is not available with SHARC, setting nproc = 1")
            nprocQM = 1

        # if the QM calculations are run in parallel, initialize Pool object from the multiprocessing library
        if nprocParall > 1 and len(QMcalculations) > 1:
            pool = mp.Pool(processes=nprocParall)
        else:
            pool = None

        # store starting dir
        startdir = os.getcwd()

        # lists to store the names of the directory and of the file for the calculations
        rundirList, filenameList = [], []

        # in the standard case, run the calculations serially
        for qm in QMcalculations:

            # input should be run with gaussian!
            if isinstance(qm.inputData, GaussianInput):

                # check if there is a working environment for gaussian
                checkResults = cobrammenv.checkGaussianQMEnv()
                if not checkResults[0]:
                    # if not, stop execution with error message
                    logwrt.fatalerror(checkResults[1])

                # name of the executable and of the I/O files
                qmexe, filename = os.environ["GAUSSIAN_EXE_QM"], "gaussian-QM"
                # set the name where the qm calculation will be run, store it, and move there
                rundir = QM._availablePathForQM()
                # store the names of the dir and files for the current calculation
                rundirList.append(rundir), filenameList.append(filename)

                # write the input text to file
                with open(os.path.join(rundir, filename + ".com"), "w") as finp:
                    finp.write(qm.inputData.fileText(filename, memory=memory, nproc=nprocQM, gversion=qmexe))

                # copy restart file to the working directory, to use the file for restart
                if qm.inputData.otheropt["restart"] is not None:
                    shutil.copy(qm.inputData.otheropt["restart"], os.path.join(rundir, filename + ".chk"))

                # when parallel calculation, use apply_async method to run asincronously
                if pool is not None:
                    pool.apply_async(QM._lauchGaussianQM, args=(rundir, filename, qmexe))
                else:  # when serial calculation, run normally
                    QM._lauchGaussianQM(rundir, filename, qmexe)
                    # when SOC is requested for mdv with ISC, run pysoc after gaussian
                    # (but only if active state is not GS, in which case the gaussian run has two consecutive jobs and pysoc will give error)
                    if qm.inputData.otheropt["SOC"] and qm.inputData.otheropt["nstate"] != 0:
                        checkResults = cobrammenv.checkPysocEnv()
                        if not checkResults[0]:
                            # if not, stop execution with error message
                            logwrt.fatalerror(checkResults[1])
                        # name of the executable and of the I/O files
                        qmexe = os.environ["PYSOC_EXE"]
                        QM._lauchPysoc(rundir, 'pysoc', qmexe, filename+'.log')


            # input is of type SHARC INTERFACE
            elif isinstance(qm.inputData, SharcInterfaceInput):

                # check SHARC environment
                # TODO: add here some checks to control whether SHARC has been properly set

                # name of the executable and of the I/O files
                qmexe, filename = os.path.join(os.environ["SHARC"], qm.inputData.interface), "QM"
                # the SHARC INTERFACE should be run from the parent directory
                rundir = '.'
                os.chdir(rundir)
                # store the names of the dir and files for the current calculation
                rundirList.append(rundir), filenameList.append(filename)

                # write the input text to file
                with open(filename + ".in", "w") as finp:
                    finp.write(qm.inputData.fileText())
                # write the input charge info to file
                with open("charge.dat", "w") as fcharge:
                    fcharge.write(qm.inputData.chargeText())

                # run qm calculation
                with open(filename + ".log", "w") as fout:
                    with open(filename + ".err", "w") as ferr:
                        subprocess.call(["nice", "-1", qmexe, filename + ".in"], stdout=fout, stderr=ferr)

                # move back to starting directory
                os.chdir(startdir)

            elif isinstance(qm.inputData, MolcasInput):

                # check if there is a working environment for Molcas
                checkResults = cobrammenv.checkMolcasEnv()
                if not checkResults[0]:
                    # if not, stop execution with error message
                    logwrt.fatalerror(checkResults[1])

                # name of the executable and of the I/O files
                qmexe, filename = os.environ["MOLCAS_SCRIPT"], "molcas"
                # set the name where the qm calculation will be run, store it, and move there
                rundir = QM._availablePathForQM()
                # store the names of the dir and files for the current calculation
                rundirList.append(rundir), filenameList.append(filename)

                # write the input text to file
                with open(os.path.join(rundir, filename + ".com"), "w") as finp:
                    finp.write(qm.inputData.fileText(filename, memory=memory, nproc=nprocQM))

                # copy restart file to the working directory, to use the file for restart
                if qm.inputData.otheropt["restart"] is not None:
                    if qm.inputData.otheropt["restart"] in ("INPORB", "molcas.RasOrb"):
                        shutil.copy(qm.inputData.otheropt["restart"], os.path.join(rundir, "INPORB"))
                    else: #restart is not of RasOrb type, so it is assumed to be JobIph type
                        shutil.copy(qm.inputData.otheropt["restart"], os.path.join(rundir, "JOBIPH"))
                    logwrt.writelog("restart file copied to {1}\n".format(qm.inputData.otheropt["restart"], rundir), 3)
                # copy to rundir additional files (if required) for next QM calculations
                QM.copyMolcasFiles(rundir, qm)

                # when parallel calculation, use apply_async method to run asincronously
                if pool is not None:
                    pool.apply_async(QM._lauchMolcasQM, args=(rundir, filename, qmexe))
                else:  # when serial calculation, run normally
                    QM._lauchMolcasQM(rundir, filename, qmexe)

        # ---------------------------------------------------------------------------------------------------------

        # parallel execution of the jobs
        if pool is not None:
            pool.close(), pool.join()

        # ---------------------------------------------------------------------------------------------------------

        for qm, filename, rundir in zip(QMcalculations, filenameList, rundirList):

            # extract results from the output files and save them in qm
            if isinstance(qm.inputData, GaussianInput):
                # input is of type GAUSSIAN INTERFACE
                qm.outputData = GaussianOutput(filename, rundir, step, qm.inputData.otheropt["SP"], qm.inputData.otheropt["verbose"], qm.inputData.otheropt["wfo"])
                # read orbitals using the ad-hoc class
                try:
                    qm.orbitals = Orbitals.readMOfromGaussianOutput(os.path.join(rundir, filename+".log"))
                except (ValueError, IndexError) as e:
                    qm.orbitals = None
                # when there are singlets and triplets (mdv with ISC) read SOC from pysoc output and store in qm instance
                if qm.inputData.otheropt["SOC"]:
                    # if active state is GS, set SOC to zero matrix
                    if qm.inputData.otheropt["nstate"] == 0:
                        qm.outputData.dataDict["SOC"] = np.zeros((qm.outputData.dataDict["nroots"],qm.outputData.dataDict["nroots"]))
                    else:
                        try:
                            qm.outputData.dataDict["SOC"] = QM.readPysoc(os.path.join(rundir, "pysoc.log"),  qm.outputData.dataDict["multiplicities"])
                        except KeyError:
                            pass


            elif isinstance(qm.inputData, SharcInterfaceInput):
                # input is of type SHARC INTERFACE
                qm.outputData = SharcInterfaceOutput(filename, rundir)

            elif isinstance(qm.inputData, MolcasInput):
                #input is of type MOLCAS INTRFACE
                qm.outputData = MolcasOutput(filename, rundir, qm.inputData.calctype, qm.inputData.otheropt["SP"])
                # save additional files (if) required for next QM calculations
                qm.saveMolcasFiles()


            # append log from output file processing to the QM log
            qm.log += qm.outputData.log

            # in case, handle QM execution errors
            if qm.outputData.get("termination") is None:
                logwrt.fatalerror('Cannot determine the final status of the QM calculation ...\n'
                                  'terminating COBRAMM execution')
            else:
                if qm.outputData.get("termination") == 1:
                    errmsg = "QM terminated abnormally. Here is an excerpt of the QM log file:\n" \
                             "-----------------------------------------------------------------------------------\n"
                    for line in qm.outputData.get("errormsg"):
                        errmsg += ">   " + line + "\n"
                    errmsg += "-----------------------------------------------------------------------------------\n" \
                              "For further details please check QM log file. "
                    logwrt.fatalerror(errmsg)

            # when the electric field has been computed at the point charges and the gradient is not available
            if qm.outputData.get("elfield") is not None and qm.outputData.get("gradcharges") is None:
                # process electric field to compute the forces on the point charges dependent on QM-MM interaction
                gradcharges = {}
                for nstate, elfield in qm.outputData.get("elfield").items():
                    # when the electric field is set to None for a specific state, just set the gradient to None
                    if elfield is None:
                        gradcharges[nstate] = None
                    else:
                        g = [], [], []
                        for charge, Ex, Ey, Ez in zip(qm.inputData.otheropt["field"][1], *elfield):
                            g[0].append(-Ex * charge), g[1].append(-Ey * charge), g[2].append(-Ez * charge)
                        gradcharges[nstate] = g
                # store the computed forces
                qm.outputData.set("gradcharges", gradcharges)

            # when the gradient of the whole set of point charges is available, process the data
            # to extract the gradient on the mobile charges alone
            if qm.outputData.get("fullgradcharge") is not None and qm.outputData.get("gradcharges") is None:
                gradcharges = {}
                for nstate, fullgrad in qm.outputData.get("fullgradcharge").items():
                    # when for a state the gradient is None for a specific state, just set the gradient to None
                    if fullgrad is None:
                        gradcharges[nstate] = None
                    else:
                        g = [], [], []
                        for mobile, gx, gy, gz in zip(qm.inputData.otheropt["charges"][2], *fullgrad):
                            if mobile:
                                g[0].append(gx), g[1].append(gy), g[2].append(gz)
                        gradcharges[nstate] = g
                # store the gradient on the mobile charges
                qm.outputData.set("gradcharges", gradcharges)

            # if the force on the point charges has been computed, process the data to acompute when needed the
            # self-energy of the point charges (M-ML interaction) and to remove the self-interaction counter term
            if qm.outputData.get("gradcharges") is not None and qm.outputData.get("gradcharges_extraterm"):
                # initialize the energy and the gradient corresponding to the selfinteraction on the M charges
                FX, FY, FZ = [], [], []
                E = 0.0
                # loop over the charges of the M layer
                for ch, xAng, yAng, zAng in zip(qm.inputData.otheropt["field"][1], *qm.inputData.otheropt["field"][0]):
                    # convert to atomic units
                    x, y, z = xAng / constants.Bohr2Ang, yAng / constants.Bohr2Ang, zAng / constants.Bohr2Ang
                    # initialize component of the force
                    Fx, Fy, Fz = 0.0, 0.0, 0.0
                    # loop over the charges of the M and L layers
                    for ch1, x1Ang, y1Ang, z1Ang in zip(qm.inputData.otheropt["charges"][1],
                                                        *qm.inputData.otheropt["charges"][0]):
                        # convert to atomic units
                        x1, y1, z1 = x1Ang / constants.Bohr2Ang, y1Ang / constants.Bohr2Ang, z1Ang / constants.Bohr2Ang
                        # use this couple of charges only when the two charges are not the same
                        if x1 != x or y1 != y or z1 != z:
                            # distance of the two charges
                            D2 = (x - x1) ** 2 + (y - y1) ** 2 + (z - z1) ** 2
                            # increment energy
                            E = (ch1 * ch) / math.sqrt(D2) + E
                            # increment force components
                            F = (ch1 * ch) / D2
                            N = math.sqrt(D2)
                            Fx = ((x - x1) / N) * F + Fx
                            Fy = ((y - y1) / N) * F + Fy
                            Fz = ((z - z1) / N) * F + Fz
                    # now store the component computed for the M layer atom
                    FX.append(Fx), FY.append(Fy), FZ.append(Fz)
                # store the self energy
                qm.outputData.set("selfenergy", E / 2.0)
                # get the actual force read from outputData
                newgradcharges = {}
                for nstate, Fmol in qm.outputData.get("gradcharges").items():
                    FmolX = list(-np.array(Fmol[0]) + np.array(FX))
                    FmolY = list(-np.array(Fmol[1]) + np.array(FY))
                    FmolZ = list(-np.array(Fmol[2]) + np.array(FZ))
                    newgradcharges[nstate] = [FmolX, FmolY, FmolZ]
                # store the computed forces
                qm.outputData.set("gradcharges", newgradcharges)

            # save all the stuff to sef
            # sef = shelve.open("cobram-sef")
            # sef['gradient'] = qm.outputData.get("gradient")[qm.outputData.get("optstate")]
            # sef['gradch'] = qm.outputData.get("gradcharges")[qm.outputData.get("optstate")]
            # sef['dipole'] = qm.outputData.get("dipole")
            # sef['charges'] = qm.outputData.get("charges")
            # if qm.outputData.get("optstate") is not None:
            #     sef['state'] = qm.outputData.get("optstate")
            #     sef['newstate'] = qm.outputData.get("optstate")
            # sef.close()
    # =============================================================================================================

    @staticmethod
    def ISC_join_QM_output(QMsinglet, QMtriplet, nsinglets, ntriplets):
        '''this method joins the output of singlet and triplet run for ISC dynamics,
        results are joined in the QMOutput instance of SINGLET calculation (main calculation)'''

        #define internal routine to shift and complete NAC dictionaries
        def shift_and_add_cross_terms(dict1, dict2,shift_amount):
        
            def shift_keys_recursive(d, shift):
                return {
                    k + shift if isinstance(k, int) else k:
                        shift_keys_recursive(v, shift) if isinstance(v, dict) else v
                    for k, v in d.items()
                }
        
            dict2_shifted = shift_keys_recursive(dict2, shift_amount)
        
            dict_merged = {**dict1, **dict2_shifted}
        
            for key1 in range(shift_amount):
                for key2 in range(shift_amount, shift_amount + len(dict2_shifted)):
                    dict_merged[key1][key2] = [[0.0],[0.0],[0.0]]
                    dict_merged[key2][key1] = [[0.0],[0.0],[0.0]]
        
            return dict_merged
        
        #join output files
        QMsinglet.outputData.dataDict["outfile"] = QMsinglet.outputData.dataDict["outfile"] + QMtriplet.outputData.dataDict["outfile"]
        
        #append triplet energies after singlets
        QMtriplet.outputData.dataDict["energy"] = {key + nsinglets: value for key, value in QMtriplet.outputData.dataDict["energy"].items()}
        QMsinglet.outputData.dataDict["energy"].update(QMtriplet.outputData.dataDict["energy"])

        #if gradient is in triplet calculation replace all related properties
        if QMsinglet.outputData.dataDict["gradient"] == {}:
            #gradient
            QMtriplet.outputData.dataDict["gradient"] = {key + nsinglets: value for key, value in QMtriplet.outputData.dataDict["gradient"].items()}
            QMsinglet.outputData.dataDict["gradient"] = QMtriplet.outputData.dataDict["gradient"]
            #charges
            QMsinglet.outputData.dataDict["charges"] = QMtriplet.outputData.dataDict["charges"]
            #dipole
            QMsinglet.outputData.dataDict["dipole"] = QMtriplet.outputData.dataDict["dipole"]
            #gradcharges
            QMtriplet.outputData.dataDict["gradcharges"] = {key + nsinglets: value for key, value in QMtriplet.outputData.dataDict["gradcharges"].items()}
            QMsinglet.outputData.dataDict["gradcharges"] = QMtriplet.outputData.dataDict["gradcharges"]
        if QMsinglet.outputData.dataDict["psioverlap"] is not None:
            #overlap matrix:
            #create a unique block-diagonal overlap matrix
            n1, m1 = QMsinglet.outputData.dataDict["psioverlap"].shape
            n2, m2 = QMtriplet.outputData.dataDict["psioverlap"].shape
            tot_matrix = np.zeros((n1 + n2, m1 + m2))
            tot_matrix[:n1, :m1] = QMsinglet.outputData.dataDict["psioverlap"]
            tot_matrix[n1:, m1:] = QMtriplet.outputData.dataDict["psioverlap"]
            QMsinglet.outputData.dataDict["psioverlap"] = tot_matrix


        #append triplet elfield after singlets
        QMtriplet.outputData.dataDict["elfield"] = {key + nsinglets: value for key, value in QMtriplet.outputData.dataDict["elfield"].items()}
        QMsinglet.outputData.dataDict["elfield"].update(QMtriplet.outputData.dataDict["elfield"])

        #SOC
        QMsinglet.outputData.dataDict["SOC"] = QMtriplet.outputData.dataDict["SOC"]

        #N ROOTS
        nroots = QMsinglet.outputData.get('nroots') + QMtriplet.outputData.get('nroots')
        QMsinglet.outputData.dataDict["nroots"] = nroots
        
        #NAC
        if QMtriplet.outputData.dataDict["nac"] != {}:
            QMsinglet.outputData.dataDict["nac"] = shift_and_add_cross_terms(QMsinglet.outputData.dataDict["nac"], QMtriplet.outputData.dataDict["nac"], nsinglets)

        #eigenvectors


