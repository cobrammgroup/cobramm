#!/usr/bin/env python3
# coding=utf-8

#    COBRAMM
#    Copyright (c) 2019 ALMA MATER STUDIORUM - Università di Bologna

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#####################################################################################################
import os  # filesystem utilities
import shutil  # filesystem utilities
import subprocess  # run external program as child process
import fileinput  # fileinput is used for inplace editing of files
import shlex  # simple lexical analysis for unix syntax
import math  # import mathematical functions
import re  # process output with regular expressions
import copy  # shallow and deep copy operations
import time  # provides various time-related functions
import glob  # for searching file with a specific pattern
import gzip  # for unzipping files
import CBF
# imports of local modules

import logwrt  # write messages and output to log (std out or cobramm.log)
import constants  # physical and mathematical constants
import cobrammenv  # function to check if executables are available
from gaussianCalculator import GaussianOutput
from cobrammCalculator import CobrammCalculator, CobrammInput, CobrammOutput

# math libraries

import numpy as np  # numpy: arrays and math utilities

#try:
#    sys.path.append(os.path.join(os.environ['COBRAM_PATH'], 'cobramm'))
#except KeyError:
#    raise RuntimeError('cannot import cobramm module.\n'
#                       'Please check if $COBRAM_PATH is defined in your environment, if not run:\n'
#                       'export COBRAM_PATH=/path/to/COBRAMM')


#####################################################################################################

# create a dictionary to store all the relevant filepaths 
paths = {}

# filepaths output of cobramm-runpumpprobe.py
paths["PP"] = "PumpProbe"
paths["PP_low"] = paths["PP"]+"/inputs_from_step_{}" 

# filepaths of where WFO is computed
paths["WFO"] = "WFO"
paths["WFO_low"] = paths["WFO"]+"/step_{}"


# dictionary to store the names of the input wfo files + extras
name = {}
# Gaussian
name['gaussian'] = 'GA'
# NWChem
name['nwchem'] = 'NW'
# OpenMolcas
name['molcas'] = 'OM'
# WFO output name
name['WFO_out'] = 'wfo_results.dat'

# dictionary to store the wfo input keywords depending on the software (for extra flexibility)
key = {}
# Gaussian
key['gaussian'] = 'g16'
# NWChem 
key['nwchem'] = 'nw'
# Molcas
key['molcas'] = 'om'

# names of OpenMolcas files
fileOM = {}
fileOM['out'] = 'qmALL.log'
fileOM['RasOrb_gz'] = 'molcas_0.RasOrb.gz'
fileOM['molden_gz'] = 'molcas.rasscf_0.molden.gz'
       
#######################################################################################################3

def WFOPreparator(step, WFOpath, CIname, DFTsoftware):
    '''Manages the WFO computations, preparing/cutting/copying the due files.'''    
    
    # different actions depending on the method
    software_dft = DFTsoftware.lower()

    startdir=os.getcwd()
    
    fname="{}.out".format(name[software_dft])

    if software_dft == 'gaussian':

        # cut the total .log file, extracting the part of the current step
        RegExp = "Start QM calculation output of STEP : {}".format(step) + "(.*)" + \
            "End QM calculation output of STEP : {}".format(step)
        os.chdir("QM_data")
        with open("qmALL.log") as flog:
            f = flog.read()
        FileStep = re.findall(RegExp, f, re.DOTALL)[0]

        # needs to be absolute
        os.chdir(WFOpath)

        # Number of basis functions
        BSExp = "Standard basis: " + "(.*)" + "nuclear repulsion energy"
        BSsplit = re.findall(BSExp, FileStep, re.DOTALL)[0]
        with open("temp_{}".format(fname), "w") as f:
            f.write("\n\n{}\n\n------------------------------------------------------".format(BSsplit))

        # Geometry (Symbolic Z-matrix)
        XYZExp = "QM single-point calculation for COBRAMM" + "(.*)" + "MicOpt="
        XYZsplit = re.findall(XYZExp, FileStep, re.DOTALL)[0]
        with open("temp_{}".format(fname), "a") as f:
            f.write("\n\n{}\n\n------------------------------------------------------".format(XYZsplit))    

        # Population (Alpha occ. and Alpha virt. eigenvalues)
        POPExp = "Population analysis using the CI density." + "(.*)" + \
            "Full Mulliken population analysis:"
        POPsplit = re.findall(POPExp, FileStep, re.DOTALL)[0]
        with open("temp_{}".format(fname), "a") as f:
            f.write("\n\n{}\n\n------------------------------------------------------".format(POPsplit))  

        # attach the CI extracted from the dynamics
        with open(CIname) as fci:
            fCI=fci.read()
        with open("temp_{}".format(fname), "a") as f:
            f.write("\n\n{}\n\n------------------------------------------------------".format(fCI))      
        os.remove(CIname)

        shutil.move("temp_{}".format(fname), fname)

    os.chdir(startdir)

###############################################################################################

# find the line of the regex match
def line_match(m):
    return m.string.count("\n", 0, m.start())+1

###############################################################################################

def WFOLauncher(max_step, start_step, stepWFO, DFTsoftware, PT2software):
    ''' Extracts the relevant data in order to launch WFOverlap.py 
    Is usually called after the TDDFT dynamic has ended and after WFOpreparator().'''

    # launch in the traj directory
    startdir = os.getcwd()

    # either Gaussian or NWChem
    software_dft = DFTsoftware.lower()
    software_pt2 = PT2software.lower()

    WFOpathflex = paths["WFO_low"]
    PT2pathflex = paths["PP_low"]

    # initialize the list with the COBRAMM options of the Molcas calculation
    commandhard = CBF.makehard()
    commandhard.insert(0, '0')
    # get the content of the cobramm command file in the first step of PumpProbe
    cobcom = CBF.getCobrammCommand(os.path.join(PT2pathflex.format(start_step), 'cobram.command'))
    # merge command soft and hard
    key_soft = CBF.ReadCobramCommand(cobcom, 'keyword', 'keywords')
    commandhard = CBF.key2hard(key_soft, commandhard)
    command_soft = CBF.ReadCobramCommand(cobcom, 'command', 'commands')
    commandhard = CBF.soft2hard(command_soft, commandhard)
    # list of commands
    command_pt2 = commandhard

    # do the same as above but with the cobram.command of the dynamic 
    commandhard = CBF.makehard()
    commandhard.insert(0, '0')
    # get the content of the cobramm command file in the dynamic directory (will contain the info of the TDDFT calculation)
    cobcom = CBF.getCobrammCommand(os.path.join(startdir, 'cobram.command'))
    # merge command soft and hard
    key_soft = CBF.ReadCobramCommand(cobcom, 'keyword', 'keywords')
    commandhard = CBF.key2hard(key_soft, commandhard)
    command_soft = CBF.ReadCobramCommand(cobcom, 'command', 'commands')
    commandhard = CBF.soft2hard(command_soft, commandhard)
    # list of commands
    command_dft = commandhard

    # marks eventual old WFO calculations as OLD
    WFOupdater()

    # runs over the pre-pepared step directories
    for s in range(start_step,max_step+1,stepWFO):

        PT2path = PT2pathflex.format(s)
        # previous CASPT2 calc
        PT2pathprev = PT2pathflex.format(s-stepWFO)
        PT2pathnext = PT2pathflex.format(s+stepWFO)
        WFOpath = WFOpathflex.format(s)

        ## basis set (only three are supported for now)
        bs_storage=['cc-pvdz', '6-31g', '6-31g*', '6-31g**', 'ano-r', 'ano-l']

        # checks if the correspondent calculation ended 
        while os.path.isfile(os.path.join(PT2path,'QM_data/{}'.format(fileOM['RasOrb_gz']))) == False:
            time.sleep(10)

        with open(os.path.join(PT2path, "cobram.command")) as fcomm:
            InputCob=fcomm.read()

        # the three softwares for which the WFO tool works
        # taken from dictionary 'name'
        QM_variations = []
        for softw in name:
            QM_variations.append(name[softw])

        # dft output file
        with open(os.path.join(startdir, WFOpath, "{}.out".format(name[software_dft]))) as fdft:
            OutDFT=fdft.read()
       
        # the input for thw WFO code
        launcherSTR = "python3 /home2/alessandro/WFObranch/cobramm/util/cobramm-wfo.py "

        if software_pt2 == 'molcas':

            ## CASPT2 out file
            CASout = '{}.out'.format(name[software_pt2])
            shutil.copy(os.path.join(PT2path,'QM_data/{}'.format(fileOM['out'])),os.path.join(WFOpath,CASout))

            ## CASPT2 molden file (zipped)
            CASmol = '{}.molden'.format(name[software_pt2])
            shutil.copy(os.path.join(PT2path,'QM_data/{}'.format(fileOM['molden_gz'])),os.path.join(WFOpath,CASmol+'.gz'))

            ## CASPT2 excited states
            casnrExp="CIROOT"
            for m in re.finditer(casnrExp, InputCob):
                l=line_match(m)
                casnr=int(InputCob.splitlines()[l].split()[0])-1
            launcherSTR += "-casnr {} ".format(casnr)

            ## active electrons
            aeExp="NACTEL"
            for m in re.finditer(aeExp, InputCob):
                l=line_match(m)
                ae=InputCob.splitlines()[l].split()[0]
            launcherSTR += "-ae {} ".format(ae)

            ## active orbitals
            aoExp="ras2"
            for m in re.finditer(aoExp, InputCob):
                l=line_match(m)
                ao=InputCob.splitlines()[l].split()[0]    
            launcherSTR += "-ao {} ".format(ao)

            ## mode (RASSCF or RASPT2)
            mode="ras"; modeExp='MULTISTATE'
            # search for the 'MULTISTATE' keyword 
            for m in re.finditer(modeExp, InputCob):
                l=line_match(m)
                # if found, change mode from RASSCF to RASPT2
                mode="xms"  
            launcherSTR += "-c {} ".format(mode)


        # account for possible modification of the RASPT2{Molcas} (first) and DFT{Gaussian} (second) basis set (cc-pvdz is the only case for now)
        appendix = ['-mod']; bs_list = []; qm_softwares = ['Molcas', 'Gaussian']
        # molcas bs
        bs_list.append(command_pt2[197].lower())
        # gaussian bs
        bs_list.append(command_dft[197].lower())
        for index,bs in enumerate(bs_list):
            for a in appendix:
                # the appendix is present
                if bs.find(a) != -1:
                    # remove it
                    bs = bs[:bs.find(a)]+bs[bs.find(a)+len(a):]
            # check if the basis set selected is implemented
            if bs in bs_storage:
                    bs_list[index] = bs
            else:
                print("\n----The basis set chosen for the {} computations is not yet implemented in the WFO tool !!-----\n".format(qm_softwares[index]))
                exit()

        ## DFT excited state
        # choose the regexp to count to find the tdnr number
        if software_dft == 'gaussian':
            dftExp = 'Excited State'
        elif software_dft == 'nwchem':
            dftExp = 'singlet a'
        tdnr = OutDFT.count(dftExp)
        launcherSTR += "-tdnr {} ".format(tdnr)

        os.chdir(WFOpath)
    
        # almost always, but you never know
        if software_pt2 == 'molcas':    
            # unzip CASPT2 molden file
            script = shlex.split('gunzip -f {}.gz'.format(CASmol))
            subprocess.run(script)

        # list with every .out file in the current directory
        outfiles = glob.glob('*.out')

        # extension of the MO file (fchk for Gaussian, molden for NWChem and Molcas)
        extens_pt2 = 'molden'
        if software_dft == 'gaussian':
            extens_dft = 'fchk'
        elif software_dft == 'nwchem':
            extens_dft = 'molden'

        ## comp keyword
        launcherSTR += "-comp {}to{} ".format(name[software_dft],name[software_pt2])

        ## CI files
        launcherSTR += "-{}_ci {}.out ".format(key[software_dft], name[software_dft])
        launcherSTR += "-{}_ci {}.out ".format(key[software_pt2], name[software_pt2])    
        
        ## MO files
        launcherSTR += "-{}_mo {}.{} ".format(key[software_dft], name[software_dft], extens_dft)
        launcherSTR += "-{}_mo {}.{} ".format(key[software_pt2], name[software_pt2], extens_pt2)

        ## Basis sets
        launcherSTR += "-{}_bs {} ".format(key[software_dft], bs_list[1])   
        launcherSTR += "-{}_bs {} ".format(key[software_pt2], bs_list[0])

        # launcherSTR += " -{}_ao {}.out".format(key[software_pt2], name[software_pt2]) 
        # launcherSTR += " -{}_ao {}.out".format(key[software_dft], name[software_dft]) 

        with open(name['WFO_out'],'w') as fout:
            launch_ref = shlex.split(launcherSTR)
            subprocess.run(launch_ref, stdout=fout, stderr=subprocess.STDOUT) 
            fout.close()

        # print(launcherSTR)
        # exit()

        os.chdir(startdir)

###############################################################################################

def WFOupdater():
    """the whole point of this is to mark as 'OLD' all the calculation present in the WFO path
    running the wfo script updated the 'OLD' file to 'RECENT'"""
    upper=os.getcwd()
    WFOpath = paths["WFO"]
    
    os.chdir(WFOpath)
    startdir=os.path.join(upper,WFOpath)
    # folder runs over the directories inside WFOpath
    for folder, subdir, files in os.walk(startdir):
        os.chdir(folder)
        if os.path.isfile('NEW') == True:
            shutil.move('NEW','OLD')
        os.chdir(startdir)
    os.chdir(upper)