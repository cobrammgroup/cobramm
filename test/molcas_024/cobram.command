!keyword
type=ts 
nsteps=100
qm-type=molcas
qmem=2000MB
geomem=3000MB
numrlx=2
ricd=1
?keyword

!molcas
&RASSCF
LUMORB
NACTEL
4 0 0
CHARGE=0
ras1
0
ras2
3
ras3
0
CIROOT
2 2 1

&caspt2
IMAG=0.2
IPEA=0
MAXITER=200
MULTISTATE=3 1 2 3
NOMULT
?molcas

!ts
#p opt(ts,NewEstmFC,noeigentest,nolinear,nomicro,maxstep=30) iop(1/9=1,5/6=6)
?ts

