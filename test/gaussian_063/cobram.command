!keyword
type=mdv
qm-type=gauss
qmem=200MB
nsteps=10
tstep=1.0
tsshort=0.5
surhop=tully
rand=0.0001
velafterhop=GD
numrlx=6
?keyword

!gaussian
#p tda(nstates=5, root=5) sto-3g b3lyp nosym

Comment line

0 1 
?gaussian

