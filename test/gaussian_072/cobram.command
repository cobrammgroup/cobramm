!keyword
type=mdv 
nsteps=12
tstep=1
surhop=tully
DC=tdc
hoptogs=none
qm-type=gauss
qmem=200MB
savQMlog=2
numrlx=3
rand=0.0004
?keyword

!gaussian
#p td(50-50,nstates=4,root=2) sto-3g b3lyp nosym 

Comment line

0 1 
?gaussian

!sander
comment line
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 10
/
?sander

