!keyword
type=mdv 
nsteps=10
tstep=1
surhop=tully
DC=tdc
hoptogs=2
gsesth=50
qm-type=gauss
qmem=200MB
savQMlog=2
numrlx=2
?keyword

!gaussian
#p td(nstates=1,root=1) sto-3g b3lyp nosym 

Comment line

0 1 
?gaussian

!sander
comment line
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 10
/
?sander

