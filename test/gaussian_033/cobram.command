!keyword
type=optxg 
nsteps=3  
qm-type=gauss
qmem=200MB
geomem=300MB
numrlx=2
?keyword

!sander
comment line
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 10
/
?sander

!gaussian
#p td(nstates=2,root=1) sto-3g b3lyp nosym

Comment line

0 1 
?gaussian


