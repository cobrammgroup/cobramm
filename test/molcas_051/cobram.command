!keyword
type=freqxg
nsteps=500
basis=STO-3G
qm-type=molcas
qmem=5000MB
geomem=5000MB
numrlx=2
?keyword

!molcas
&RASSCF
LUMORB
SYMMETRY
1
SPIN
1
NACTEL
4 0 0
CHARGE=0
ras1
0
ras2
3
ras3
0
CIROOT
2 2 1
?molcas

