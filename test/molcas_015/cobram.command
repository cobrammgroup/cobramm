!keyword
type=optxg
nsteps=100
basis=STO-3G
qm-type=molcas
qmem=5000MB
geomem=5000MB
ricd=1
?keyword

!molcas
&RASSCF
LUMORB
NACTEL
4 0 0
CHARGE=0
ras1
0
ras2
3
ras3
0
CIROOT
3 3 1

&caspt2
IMAG=0.2
IPEA=0
MAXITER=200
XMULTISTATE=3 1 2 3
?molcas

