!keyword
type=freqxg
nsteps=500
basis=STO-3G
qm-type=molcas
qmem=5000MB
geomem=5000MB
?keyword

!sander 
comment line 
&cntrl 
imin   = 1, 
maxcyc = 0, 
ntb    = 0, 
igb    = 0,
ntr    = 0, 
ibelly = 1, 
cut    = 10 
/
?sander

!molcas
&scf

&mbpt2
?molcas

