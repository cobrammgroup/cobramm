!keyword
type=ts 
nsteps=100
qm-type=molcas
qmem=200MB
geomem=300MB
numrlx=1
?keyword

!molcas
&scf
?molcas

!ts
#p opt(ts,NewEstmFC,noeigentest,nolinear,nomicro,maxstep=30) iop(1/9=1,5/6=6)
?ts

