===================================================================================================================

               ######  #####                                                                                    
              # #  ####  *  #                                                                                   
              # #  *   #####                                                                                    
              # #     ############                                                                              
              ## ####        ## ###                                                                             
          #### ##                 #                                                                             
        ## #                      #                                                                             
       #  ##                      #                                                                             
      # ######                   #                                                                              
     ##### ##  ###            ###__ __                                                                          
    #     ##     ############      #  ###                                                                       
   #############___   #######      #__   #                                                                      
   #     ##    ##  ###   ##########   ###   ::.      .:.       ..             :.     :        :.    ..       .: 
   #######    ##    #####                 -#****.  .*****#   ******#         #*+    #**      #**   .**.     .** 
   #    ##   ##    ###                  +********.  +*+ **=  ***: +**       +**:   .***     #**=   +***    .*** 
   #__###########__#                   ***-    **:  **: **=  -*#  .*#      +***.   +*#*+   #***:  .****   .**** 
    ## ##    #   ##                    **=    .**: :*****+   **=  **=     +***#    **:**  ***#*:  -****+ .**:** 
     # #     #  ##                    #**.    ***. *******  .******=     +**+*#   =** #*.+** #*.  **:.**.**-.** 
     ###########                      #*%     **#  **.  *** ****:    .*****+**#.  #*. -*#**  #*  :**  #***- .** 
    ###     # #                       #*%    ***. =**   =*# **#*-    :****#****# -**   ***   **  #*=  :**=  :*# 
   #  #     ##                        ***. .***..=**: =***.-**=*#   .#**-   .**: **:   .-    ** .**    =:   :*# 
  ###########               ___  __    #*****+  -#****#+.  **+ ***.***=      **+ ##          ## -*+         .** 
 #   ##     #   ____########   #   ##_                          **#.       .**.                                 
 #   ##      ###____#______#####_____ ###                        .#**#+++#**#.                                  
 ###### _  __##    #    ##      #     # #                           .+###+.                                     
  #_  #  ##       #      #      #     ##                                                                        
    #####__      #      ##    /######                                                                           
           ###################                                                                                  

===================================================================================================================

                                                      COBRAMM                                                      

                         Calculations OBtained by Running Ab-initio and Molecular Mechanics                        
                         ^            ^^          ^       ^             ^         ^                                

                                       The cobram QMMM interface is made by:                                       
                                           Piero Altoe' and Marco Stenta                                           
                                      Universita' degli Studi di Bologna, 2007                                     

                                              Updates & extensions by                                              
                                                  Oliver Weingart                                                  
                                      Heinrich-Heine-Universitaet Duesseldorf                                      

                         Artur Nenov, Matteo Bonfanti, Davide Avagliano and Flavia Aleotti                         
                                         Universita' degli Studi di Bologna                                        
                                              Version v2.3 2024-12-06                                              

===================================================================================================================
Starting time: Wed, 18 Dec 2024 11:43:21
===================================================================================================================

Hostname: kali

The file /home/flavia/.cobramm_profile is present
It will be read to set the paths of third-party software

================================================================================
                                  FILE CONTROL
--------------------------------------------------------------------------------


A single point calculation is requested, with HIGH layer only.
QM third party software : MOLCAS / OPENMOLCAS

                            ***********************
                            COMMAND OPTIONS SUMMARY
                            ***********************

*** SYSTEM OPTIONS ***
* allocated memory for Gaussian optimizer: 5000MB
* allocated memory for QM computation: 5000MB
* nr. of cores to use for QM third-party software: 1
* nr. of cores to use for COBRAMM internal parallel numerics routine: 1
* use AMBER serial executable sander

*** GENERAL OPTIONS ***
* number of optimization steps / microsteps in each IRC step / MD steps: 1
* save QM log files to QM_data/qmALL.log every 1 steps
* save QM wavefunction file only for last step

*** MOLECULAR MODEL OPTIONS ***
* project the force of the link atoms on the border atoms

*** MOLCAS OPTIONS ***
* do not perform abrupt switching off of state-average
* allow only down-hop in CI vector rotation protocol (85 = 1)
* use spherical harmonics to construct the basis set
* threshold for discarding integrals when using Cholesky: 1.0E-4
* do not use Cholesky decomposition
* basis set definition (possible overwritten by !basisset): STO-3G



Max number of stpes is set to 1 for this calculation

================================================================================
                          INPUT MOLECULAR DESCRIPTION
--------------------------------------------------------------------------------

Geometry has been read from the file real_layers.xyz

 * 6 high layer atom(s):  1-6
 * no medium layer atom 
 * no low layer atom 

Calculation type is H, a QM calculation is then requested

----------------------------------------------------------------
   Atom     Atomic              Coordinates (Angstroms)
     ID      Label             X           Y           Z
 ----------------------------------------------------------------
      1          C        22.709000   21.982000   22.666000
      2          C        21.767000   22.541000   21.900000
      3          H        23.627000   22.562000   22.742000
      4          H        22.647000   21.011000   23.152000
      5          H        20.778000   22.089000   21.855000
      6          H        22.020000   23.364000   21.235000
----------------------------------------------------------------


================================================================================
                                 ATOMIC CHARGES
--------------------------------------------------------------------------------


  (1) TotCRG_real            =     0.000000  

  (2) TotCRG_model           =     0.000000  
  (3) TotCRG_pod             =     0.000000  
(ck1)       {(1)-[(2)+(3)]}  =     0.000000 ok: should be zero 
                                                it differs from zero less than 1.000000e-06 

  (7) TotCRG_emb             =     0.000000  
(ck3)       {[(3)-(7)]}  =     0.000000 ok: should be zero 
                                                it differs from zero less than 1.000000e-06 

  (8) TotCRG_HIGH            =     0.000000  
  (9) TotCRG_MEDIUM          =     0.000000  
 (10) TotCRG_LOW             =     0.000000  
(ck4)  {(1)-[(8)+(9)+(10)]}  =     0.000000 ok: should be zero 
                                                it differs from zero less than 1.000000e-06 

================================================================================
                               QM/MM SINGLE POINT
--------------------------------------------------------------------------------

QM Molcas calculation: requested calculation type is MBPT2
DEBUG: calctype MBPT2
DEBUG: readEfield True
DEBUG: Efieldstate 0
DEBUG: self.dataDict[optstate] None
DEBUG: setting optstate to 0
DEBUG: calctype MBPT2
DEBUG: readEfield True
DEBUG: Efieldstate 0
DEBUG: self.dataDict[optstate] 0
MP2 energy is -77.18820691 Hartree

Molcas orbital/wavefunction files to be saved: molcas.ScfOrb molcas.scf.molden 
                                 **************
                                 QM/MM ENERGIES
                                 **************

------------------------------------------------------------
                    STATE   1
------------------------------------------------------------
                    Energies (Hartrees)
   Model-H QM:        -77.18820691
   Real MM:             0.00000000
   Model-H MM:          0.00000000
   Emb-emb crg:         0.00000000
   QM Energy:         -77.18820691


E(tot)=E(Model-H QM)+(Real MM)-(Model-H MM)-(Emb-emb)=       -77.18820691

DEBUG: QMCalc.charges [-0.125, -0.1247, 0.0585, 0.0667, 0.0591, 0.0654]
DEBUG: QMCalc.dipole (0.014308763802787004, -0.012356996488362304, -0.01863758778194745, 0.02654773364443949)
                            ************************
                            ELECTROSTATIC PROPERTIES
                            ************************

Electrostatic properties - Mulliken atom charges and dipole moment - are extracted
from the QM calculation for the H part with H-saturated bonds

 ----------------------------------------------
    Atom     Atomic       Model+H Charges
      ID      Label           (a.u.)
 ----------------------------------------------
       1          C        -0.125000
       2          C        -0.124700
       3          H         0.058500
       4          H         0.066700
       5          H         0.059100
       6          H         0.065400
 ----------------------------------------------
              TOTAL        -0.000000
 ----------------------------------------------

 ----------------------------------------------
                         Dipole Moment
                     (a.u.)         (Debye)
 ----------------------------------------------
           X        0.014309       0.036370
           Y       -0.012357      -0.031409
           Z       -0.018638      -0.047373
 ----------------------------------------------
   magnitude        0.026548       0.067479
 ----------------------------------------------

================================================================================
                               CALCULATION TIMES
--------------------------------------------------------------------------------

==================================================================================================
 Code section  | tot WallTime/ s | tot CPUTime/ s  | Nr calls | avg WallTime/ s | avg CPUTime/ s  
--------------------------------------------------------------------------------------------------
total          |            2.03 |            0.06 |        1 |            2.03 |            0.06
QM section     |            1.88 |            0.04 |        1 |            1.88 |            0.04
QM run         |            1.86 |            0.04 |        1 |            1.86 |            0.04
QM input       |            0.00 |            0.00 |        1 |            0.00 |            0.00
xml output     |            0.00 |            0.00 |        2 |            0.00 |            0.00
QMMM section   |            0.00 |            0.00 |        1 |            0.00 |            0.00
MM section     |            0.00 |            0.00 |        1 |            0.00 |            0.00
==================================================================================================


================================================================================
Ending time: Wed, 18 Dec 2024 11:43:23
================================================================================

--- COBRAM calculation terminated normally ---
