import unittest
import os, shutil, sys
import subprocess

# define directory testRootDir = where files are stored
testRootDir = os.path.dirname(os.path.abspath(__file__))
testRunDir  = os.path.join(testRootDir,"test_run")

# import python file with the definition of COBRAMM run command
# and the functions to extract numbers from output files
parentdir = os.path.dirname(testRootDir)
if not parentdir in sys.path: 
    sys.path.insert(0,parentdir) 
import test_config

# define directory testRunDir = where calculation is run 
testTmpDir  = os.path.join(test_config.tmpPath, "test_cobramm_run" )

# List that defines the input files that need to be copied to run the test calculation
inputFileList  = ["cobram.command", "real_layers.xyz", "model-H.top", "real.top", "gaussian-QM.chk" ]

# name of the reference and test output files
logTest = os.path.join(testRunDir,"cobramm.log")
logRef  = os.path.join(testRootDir,"cobramm.log_ref")

class test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):

        # When overwrite option is used, first remove everything from previous test run
        if test_config.overWrite:
            if os.path.isdir(testRunDir): shutil.rmtree(testRunDir)
        # and clean temporary directory
        if os.path.isdir(testTmpDir):
            shutil.rmtree(testTmpDir)
        
        # run the test calculation only when the testRunDir directory is not present
        if not os.path.isdir(testRunDir):
            # create testRunDir directory and move there
            os.mkdir(testTmpDir); os.chdir(testTmpDir)
            # copy input file to the run directory
            for f in inputFileList:
                shutil.copyfile(os.path.join(testRootDir,f), os.path.join(testTmpDir,f))
            # run COBRAMM
            stdoutf = open("cobramm.log","w")
            subprocess.call(test_config.cobrammCommand, stderr=subprocess.STDOUT, stdout=stdoutf)
            stdoutf.close()
            # copy final output files to the test directory
            shutil.copytree(testTmpDir, testRunDir)

    @classmethod
    def tearDownClass(cls):
        # remove the directory in which the test calculation has been run
        if os.path.isdir(testTmpDir):
            shutil.rmtree(testTmpDir)


    def test_existwithsuccess(self):
       # check if the log file contains the successful run string
        with open(logTest) as f:
           self.assertTrue( "--- COBRAM calculation terminated normally ---" in f.read() )

    def test_optQMMMenergy_state1(self):
        # extract the energy of state 1 using the function in test_config
        EQMMM_Test = test_config.extract_QMMMEnergy_logfile( logTest, 1 ) 
        EQMMM_Ref  = test_config.extract_QMMMEnergy_logfile( logRef,  1 ) 
        # compare the last two values, which are the energies of the final optimization steps
        self.assertLess( test_config.energy_difference(EQMMM_Test[-1],EQMMM_Ref[-1]), test_config.energy_threshold )
          
    def test_optQMMMenergy_state2(self):
        # extract the energy of state 2 using the function in test_config
        EQMMM_Test = test_config.extract_QMMMEnergy_logfile( logTest, 2 )
        EQMMM_Ref  = test_config.extract_QMMMEnergy_logfile( logRef,  2 )
        # compare the last two values, which are the energies of the final optimization steps
        self.assertLess( test_config.energy_difference(EQMMM_Test[-1],EQMMM_Ref[-1]), test_config.energy_threshold )

    def test_optQMMMenergy_state3(self):
        # extract the energy of state 3 using the function in test_config
        EQMMM_Test = test_config.extract_QMMMEnergy_logfile( logTest, 3 )
        EQMMM_Ref  = test_config.extract_QMMMEnergy_logfile( logRef,  3 )
        # compare the last two values, which are the energies of the final optimization steps
        self.assertLess( test_config.energy_difference(EQMMM_Test[-1],EQMMM_Ref[-1]), test_config.energy_threshold )


if __name__ == '__main__':
    unittest.main(verbosity=2)

    
