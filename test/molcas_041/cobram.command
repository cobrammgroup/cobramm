!keyword
type=ci
nsteps=3
basis=STO-3G
qm-type=molcas
qmem=5000MB
geomem=5000MB
verbose=2
numrlx=2
ricd=1
BranchPlane=gmean
?keyword

!molcas
&RASSCF
LUMORB
CHARGE
1
NACTEL
6 0 0
INACTIVE
19
ras1
0
ras2
6
ras3
0
CIROOT
2 2 1

&caspt2
IMAG=0.2
IPEA=0
MAXITER=200
MULTISTATE=3 1 2 3
NOMULT
PROP
?molcas

