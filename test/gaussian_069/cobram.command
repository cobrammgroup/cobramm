!keyword
type=mdv 
nsteps=5
tstep=0.5
surhop=tully
DC=tdc
hoptogs=none
qm-type=gauss
qmem=200MB
savQMlog=2
numrlx=6
rand=0.0004
?keyword

!gaussian
#p td(nstates=5,root=5) sto-3g b3lyp nosym 

Comment line

0 1 
?gaussian

!sander
comment line
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 10
/
?sander

