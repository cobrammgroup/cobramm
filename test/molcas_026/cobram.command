!keyword
type=ts 
nsteps=3  
qm-type=molcas
qmem=200MB
geomem=300MB
?keyword


!sander
comment line
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 10
/
?sander

!molcas
&scf
?molcas

!ts
#p opt(ts,NewEstmFC,noeigentest,nolinear,nomicro,addredundant,maxstep=30) iop(1/9=1,5/6=6)
?ts

