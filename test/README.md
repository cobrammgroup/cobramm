
# COBRAMM TEST SUITE
          
the directory contains a collection of test cases that are 
intended to be used to test the normal execution of COBRAMM.

To test the correct installation of the code, please use
the run\_tests script that is located in the root directory
of COBRAMM:

    cd $COBRAM_PATH  
    ./run_tests

The following is the list of test cases. 
Each test is located in a subdirectory with name
                 `QM_CODE`_`TEST_#` 


 |  QM CODE   | TEST # |     DESCRIPTION                         |          TEST                          |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 001    | Hartree-Fock single point, no MM        | SP energies (GS)                       |
 |  Gaussian  | 002    | DFT B3LYP single point, no MM           | SP energies (GS)                       |
 |  Gaussian  | 003    | CIS single point, no MM                 | SP energies (GS + 5 ES)                |
 |  Gaussian  | 004    | SS-CASSCF single point, no MM           | SP energies (GS)                       |
 |  Gaussian  | 005    | SA-CASSCF single point, no MM           | SP energies (GS + 1 ES)                |
 |  Gaussian  | 006    | TD-DFT single point, no MM              | SP energies (GS + 5 ES)                |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 007    | Hartree-Fock optimization, no MM        | opt geometry and energy (GS)           |
 |  Gaussian  | 008    | DFT B3LYP optimization, no MM           | opt geometry and energy (GS)           |
 |  Gaussian  | 009    | SS-CASSCF optimization, no MM           | opt geometry and energy (GS)           |
 |  Gaussian  | 010    | SA-CASSCF optimization, no MM           | opt geometry and energy (GS + 1 ES)    |
 |  Gaussian  | 011    | TD-DFT optimization, no MM              | opt geometry and energy (GS + 3 ES)    |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 012    | TD-DFT TS opt (NewEstmFC), no MM        | opt geometry and energy (GS + 2 ES)    |
 |  Gaussian  | 013    | SA-CASSCF TS opt (NewEstmFC), no MM     | opt geometry and energy (GS + 1 ES)    |
 |  Gaussian  | 014    | SA-CASSCF TS opt (RCFC), no MM (\*)     | opt geometry and energy (GS + 1 ES)    |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 015    | TD-DFT IRC, no MM                       | IRC geometry and energy                |
 |  Gaussian  | 016    | SA-CASSCF IRC, no MM                    | IRC geometry and energy                |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 017    | HF frequency (serial), no MM            | frequencies                            |
 |  Gaussian  | 018    | DFT B3LYP frequency (serial), no MM     | frequencies                            |
 |  Gaussian  | 019    | TD-DFT frequency (serial), no MM        | frequencies                            |
 |  Gaussian  | 020    | SA-CASSCF frequency (serial), no MM     | frequencies                            |
 |  Gaussian  | 021    | HF frequency (parallel), no MM          | frequencies                            |
 |  Gaussian  | 022    | DFT B3LYP frequency (parallel), no MM   | frequencies                            |
 |  Gaussian  | 023    | TD-DFT frequency (parallel), no MM      | frequencies                            |
 |  Gaussian  | 024    | SA-CASSCF frequency (parallel), no MM   | frequencies                            |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 025    | TD-DFT molecular dynamics, no MM        | energies of the snapshots of the traj  |
 |  Gaussian  | 026    | SA-CASSCF molecular dynamics, no MM     | energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 027    | HF single point, with MM                | SP energies (GS)                       |
 |  Gaussian  | 028    | TD-DFT single point, with MM            | SP energies (GS + 2 ES)                |
 |  Gaussian  | 029    | SS-CASSCF single point, with MM         | SP energies (GS)                       |
 |  Gaussian  | 030    | SA-CASSCF single point, with MM         | SP energies (GS + 2 ES )               |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 031    | HF optimization, with MM                | opt geometry and energy (GS)           |
 |  Gaussian  | 032    | DFT optimization, with MM               | opt geometry and energy (GS)           |
 |  Gaussian  | 033    | TD-DFT optimization,  with MM           | opt geometry and energy (GS + 2 ES)    |
 |  Gaussian  | 034    | SA-CASSCF optimization, with MM         | opt geometry and energy (GS + 1 ES)    |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 035    | SA-CASSCF TS opt (NewEstmFC), with MM   | opt geometry and energy (GS + 1 ES)    |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 036    | TD-DFT frequency (parallel), with MM    | frequencies                            |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 037    | TD-DFT molecular dynamics, with MM      | energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 001    | HF single point, no MM                  | SP energies (GS)                       |
 |  Molcas    | 002    | MP2 single point, no MM                 | SP energies (GS)                       |
 |  Molcas    | 003    | CASSCF single point, no MM              | SP energies (GS + 2 ES)                |
 |  Molcas    | 004    | SS-CASPT2 single point, no MM           | SP energies (GS + 2 ES)                |
 |  Molcas    | 005    | XMS-CASPT2 single point, no MM          | SP energies (GS + 2 ES)                |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 006    | HF single point, with MM                | SP energies (GS)                       |
 |  Molcas    | 007    | MP2 single point, with MM               | SP energies (GS)                       |
 |  Molcas    | 008    | CASSCF single point, with MM            | SP energies (GS + 2 ES)                |
 |  Molcas    | 009    | SS-CASPT2 single point, with MM         | SP energies (GS + 2 ES)                |
 |  Molcas    | 010    | XMS-CASPT2 single point, with MM        | SP energies (GS + 2 ES)                |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 011    | HF optimization, no MM                  | opt geometry and energy (GS)           |
 |  Molcas    | 012    | MP2 optimization, no MM                 | opt geometry and energy (GS)           |
 |  Molcas    | 013    | CASSCF optimization, no MM              | opt geometry and energy (GS + 2 ES )   |
 |  Molcas    | 014    | SS-CASPT2 optimization, no MM           | opt geometry and energy (GS + 2 ES )   |
 |  Molcas    | 015    | XMS-CASPT2 optimization, no MM          | opt geometry and energy (GS + 2 ES )   |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 016    | HF optimization, with MM                | opt geometry and energy (GS)           |
 |  Molcas    | 017    | MP2 optimization, with MM               | opt geometry and energy (GS)           |
 |  Molcas    | 018    | CASSCF optimization, with MM            | opt geometry and energy (GS + 2 ES )   |
 |  Molcas    | 019    | SS-CASPT2 optimization, with MM         | opt geometry and energy (GS + 2 ES )   |
 |  Molcas    | 020    | XMS-CASPT2 optimization, with MM        | opt geometry and energy (GS + 2 ES )   |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 021    | HF TS opt, no MM                        | opt geometry and energy (GS)           |
 |  Molcas    | 022    | MP2 TS opt, no MM                       | opt geometry and energy (GS)           |
 |  Molcas    | 023    | CASSCF TS opt, no MM                    | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 024    | SS-CASPT2 TS opt, no MM                 | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 025    | XMS-CASPT2 TS opt, no MM                | opt geometry and energy (GS + 1 ES )   |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 026    | HF TS opt, with MM                      | opt geometry and energy (GS)           |
 |  Molcas    | 027    | MP2 TS opt, with MM                     | opt geometry and energy (GS)           |
 |  Molcas    | 028    | CASSCF TS opt, with MM                  | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 029    | SS-CASPT2 TS opt, with MM               | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 030    | XMS-CASPT2 TS opt, with MM              | opt geometry and energy (GS + 1 ES )   |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 031    | CASSCF IRC, no MM                       | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 032    | SS-CASPT2 IRC, no MM                    | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 033    | XMS-CASPT2 IRC, no MM                   | opt geometry and energy (GS + 1 ES )   |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 034    | CASSCF IRC, with MM                     | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 035    | SS-CASPT2 IRC, with MM                  | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 036    | XMS-CASPT2 IRC, with MM                 | opt geometry and energy (GS + 1 ES )   |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 037    | CASSCF CI opt, no MM (nac)              | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 038    | SS-CASPT2 CI opt, no MM (nac)           | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 039    | XMS-CASPT2 CI opt, no MM (nac)          | opt geometry and energy (GS + 1 ES )   |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 040    | CASSCF CI opt, no MM (gmean)            | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 041    | SS-CASPT2 CI opt, no MM (gmean)         | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 042    | XMS-CASPT2 CI opt, no MM (gmean)        | opt geometry and energy (GS + 1 ES )   |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 043    | CASSCF CI opt, with MM (nac)            | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 044    | SS-CASPT2 CI opt, with MM (nac)         | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 045    | XMS-CASPT2 CI opt, with MM (nac)        | opt geometry and energy (GS + 1 ES )   |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 046    | CASSCF CI opt, with MM (gmean)          | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 047    | SS-CASPT2 CI opt, with MM (gmean)       | opt geometry and energy (GS + 1 ES )   |
 |  Molcas    | 048    | XMS-CASPT2 CI opt, with MM (gmean)      | opt geometry and energy (GS + 1 ES )   |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 049    | HF frequency (serial), no MM            | frequencies                            |
 |  Molcas    | 050    | MP2 frequency (serial), no MM           | frequencies                            |
 |  Molcas    | 051    | CASSCF frequency (serial), no MM        | frequencies                            |
 |  Molcas    | 052    | SS-CASPT2 frequency (serial), no MM     | frequencies                            |
 |  Molcas    | 053    | XMS-CASPT2 frequency (serial), no MM    | frequencies                            |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 054    | HF frequency (parallel), no MM          | frequencies                            |
 |  Molcas    | 055    | MP2 frequency (parallel), no MM         | frequencies                            |
 |  Molcas    | 056    | CASSCF frequency (parallel), no MM      | frequencies                            |
 |  Molcas    | 057    | SS-CASPT2 frequency (parallel), no MM   | frequencies                            |
 |  Molcas    | 058    | XMS-CASPT2 frequency (parallel), no MM  | frequencies                            |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 059    | HF frequency (serial), with MM          | frequencies                            |
 |  Molcas    | 060    | MP2 frequency (serial), with MM         | frequencies                            |
 |  Molcas    | 061    | CASSCF frequency (serial), with MM      | frequencies                            |
 |  Molcas    | 062    | SS-CASPT2 frequency (serial), with MM   | frequencies                            |
 |  Molcas    | 063    | XMS-CASPT2 frequency (serial), with MM  | frequencies                            |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 064    | HF frequency (parallel), with MM        | frequencies                            |
 |  Molcas    | 065    | MP2 frequency (parallel), with MM       | frequencies                            |
 |  Molcas    | 066    | CASSCF frequency (parallel), nwitho MM  | frequencies                            |
 |  Molcas    | 067    | SS-CASPT2 frequency (parallel), with MM | frequencies                            |
 |  Molcas    | 068    | XMS-CASPT2 frequency (parallel), with MM| frequencies                            |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 069    | CASSCF molecular dynamics (no hop)      | energies of the snapshots of the traj  |
 |  Molcas    | 070    | SS-CASPT2 molecular dynamics (no hop)   | energies of the snapshots of the traj  |
 |  Molcas    | 071    | XMS-CASPT2 molecular dynamics (no hop)  | energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 072    | CASSCF molecular dynamics (FSSF,nac)    | energies of the snapshots of the traj  |
 |  Molcas    | 073    | SS-CASPT2 molecular dynamics (FSSF,nac) | energies of the snapshots of the traj  |
 |  Molcas    | 074    | XMS-CASPT2 molecular dynamics (FSSF,nac)| energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 075    | CASSCF molecular dynamics (FSSF,tdc)    | energies of the snapshots of the traj  |
 |  Molcas    | 076    | SS-CASPT2 molecular dynamics (FSSF,tdc) | energies of the snapshots of the traj  |
 |  Molcas    | 077    | XMS-CASPT2 molecular dynamics (FSSF,tdc)| energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 078    | CASSCF molecular dynamics with ISC      | energies of the snapshots of the traj  |
 |  Molcas    | 079    | SS-CASPT2 molecular dynamics with ISC   | energies of the snapshots of the traj  |
 |  Molcas    | 080    | XMS-CASPT2 molecular dynamics with ISC  | energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molpro    | 045    | Hartree-Fock optimization, no MM        | opt geometry and energy (GS)           |
 |  Molpro    | 046    | MCSCF optimization, no MM               | opt geometry and energy (GS + 2 ES )   |
 |  Molpro    | 047    | MCSCF conical intersection, no MM       | opt geometry and energy (GS + 2 ES )   |
 |  Molpro    | 048    | MCSCF IRC, no MM                        | IRC geometry and energy (GS + 2 ES )   |
 |  Molpro    | 049    | MCSCF Frequency, no MM                  | frequencies                            |
 |  Molpro    | 050    | MCSCF molecular dynamics, no MM         | energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molpro    | 051    | MP2 optimization, with MM               | opt geometry and energy (GS)           |
 |  Molpro    | 052    | MCSCF conical intersection,with MM (\*) | opt geometry and energy (GS + 3 ES )   |
 |  Molpro    | 053    | MCSCF IRC, with MM (\*)                 | opt geometry and energy (GS + 3 ES )   |
 |  Molpro    | 054    | MCSCF MD (CI ovlp), with MM             | energies of the snapshots of the traj  |
 |  Molpro    | 055    | MCSCF MD (FSSH + Persico), with MM      | energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 058    | TD-DFT opt, no MM, numer deriv, + displ | opt geometry, all energies             |
 |  Gaussian  | 059    | TD-DFT opt, no MM, numer deriv, +/- dis | opt geometry, all energies             |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 063    | TDDFT surface hopping, no MM (\*\*)     | energies along the traj (GS + 4 ES)    |
 |  Gaussian  | 064    | TDDFT hop to GS, no MM (\*\*)           | energies along the traj (GS + ES)      |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 069    | TDDFT surface hopping, with MM (\*\*)   | energies along the traj (GS + 5 ES)    |
 |  Gaussian  | 070    | TDDFT hop to GS, with MM (\*\*)         | energies along the traj (GS + ES)      |
 |  Gaussian  | 071    | TDDFT ISC surface hopping, no MM (\*\*) | energies along the traj (GS + 12 ES)   |
 |  Gaussian  | 072    | TDDFT ISC surface hopping, with MM(\*\*)| energies along the traj (GS + 8 ES)    |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 073    | TDDFT S2/S1 CI optimization, with MM    | opt geometry and energy (GS + 2 ES)    |
    
     (\*) The results of the test differ significantly depending on the version of Gaussian.
          In the test suite run, the calculation is automatically tested against an output
          obtained with Gaussian 16. For users that need to check Gaussian 09 results,
          the output for Gaussian 09 can be found in the test directory.

   (\*\*) Works only with Gaussian > 09 (TDA option is required to compute WF overlap with TD-DFT)

