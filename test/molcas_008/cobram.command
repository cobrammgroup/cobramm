!keyword
type=optxg
nsteps=sp
basis=STO-3G
qm-type=molcas
qmem=5000MB
geomem=5000MB
?keyword

!sander 
comment line 
&cntrl 
imin   = 1, 
maxcyc = 0, 
ntb    = 0, 
igb    = 0,
ntr    = 0, 
ibelly = 1, 
cut    = 10 
/
?sander

!molcas
&RASSCF
LUMORB
NACTEL
4 0 0
CHARGE=0
ras1
0
ras2
3
ras3
0
CIROOT
3 3 1
?molcas

