!keyword
type=optxg 
basis=STO-3G
nsteps=1000  
qm-type=gauss
qmem=200MB
geomem=300MB
numrlx=2
?keyword

!gaussian
#p casscf(2,2,nroot=2,stateaverage) nosym scf(conver=5)

Comment line

+1 1 
?gaussian

!gaussweights
0.50000000 0.50000000
?gaussweights

!redundant 
1 2 F
?redundant

!optxg
#p opt(noeigentest,nolinear,nomicro,addredundant,maxstep=30)
?optxg

