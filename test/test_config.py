
import sys
import os
import re
import numpy as np


# hijack $PYTHONPATH (sys.path) to find cobramm local modules
sys.path.append(os.path.join(os.getenv('COBRAM_PATH'),'cobramm'))
# now we can import output module
try:
    from output import Output
except ImportError:
    print( 'Error: cannot import cobramm output.py module.\n'
           'Please check if $COBRAM_PATH is defined in your environment, if not run:\n'
           'export COBRAM_PATH=/path/to/COBRAMM\n' )
    sys.exit(1)

################################################################################################

# define a set of variables from a configuration file named "test_data.py"

# cobrammPath : path to COBRAMM root directory
# tmpPath     : path to a scratch directory where COBRAMM test calcs are run
# overWrite   : bool variable, if True, re-run COBRAMM tests overwriting previous results
# checkGaussian, checkMolcas, checkMolpro, checkTurbomole : bool variables to
#                switch on/off the testing of the corresponding codes

# define default configuration values
cobrammPath = "/usr/local/cobramm-git"
tmpPath = "/tmp"
overWrite = False
checkGaussian  = True
checkPySOC     = True 
checkMolcas    = True 
checkMolpro    = True
checkTurbomole = False 
skipIRC        = False

# try to read user-defined configuration, that overwrite the variables above
try:
   from test_data_user import *
except:
   pass

# define the path of the COBRAMM executable
cobrammCommand = [ os.path.join(cobrammPath, "cobramm", "cobram.py" ) ]

################################################################################################
# common functions that are used to compute difference between list of numbers 
# and related threshold for test success
################################################################################################

def energy_difference( E1, E2 ):
   return abs(E1-E2)
energy_threshold = 2.E-4

def energy_vec_difference( E1, E2 ):
   return np.linalg.norm(np.array(E1)-np.array(E2))/len(E1)

def geometry_difference( geom1, geom2 ):
    return np.linalg.norm(np.array(geom1)-np.array(geom2))/len(geom1)
geometry_threshold = 6.E-4

def IRC_geom_difference( IRC1, IRC2 ):
    normDiff = 0.
    for step in zip(IRC1,IRC2):
       normDiff += np.linalg.norm(np.array(step[0])-np.array(step[1]))
    return(normDiff)
IRC_geom_threshold = 1.E-2

def IRC_energy_difference( IRC1, IRC2 ):
    return(np.linalg.norm(np.array(IRC1)-np.array(IRC2)))
IRC_energy_threshold = 1.E-3

def frequency_difference( freq1, freq2 ):
    return np.linalg.norm(np.array(freq1)-np.array(freq2))/len(freq1)
frequency_threshold = 1.

def traj_energy_difference( traj1, traj2 ):
    return(np.linalg.norm(np.array(traj1)-np.array(traj2)))
traj_energy_threshold = 2.E-4

################################################################################################
# common functions that are used to read data from output files
################################################################################################

# *** cobram.log file ***

def extract_QMMMEnergy_logfile(filename, iState):
   regex_QMMMEnergy = 'STATE *'+str(iState)+'.*?E\(tot\)\=E\(Model\-H QM\)\+\(Real MM\)\-\(Model\-H MM\)\-\(Emb\-emb\)\= *(-?[0-9]*.[0-9]*)'
   with open(filename, 'r') as f:
      text = f.read()
      EQMMM = re.findall( regex_QMMMEnergy, text, flags=re.DOTALL)
      f.close()
   return([ float(e) for e in EQMMM])

# *** cobram.xml file ***

def extract_optGeometry_outfile(filename):
    OUT = Output(filename=filename, parse=True)
    last_step = OUT.get_step(OUT.steps)
    geom = last_step.geometry.cartesian
    geom = np.reshape(geom, (len(geom[0])*3,), order='F')
    return geom
    #regex_realgeometry = 'G1 \: step .*? \[geometry \(AXYZ\_real\)\](.*?)G3 \: step'
    #with open(filename, 'r') as f:
    #    text = f.read()
    #    findText = re.findall( regex_realgeometry, text, flags=re.DOTALL )
    #    geom = []
    #    for s in findText[-1].split("\n"):
    #       if s:
    #          for coord in s.split()[1:]:
    #             geom.append( float(coord) )
    #    f.close()
    #return(geom)

# *** geometry.log file ***

def extract_IRCGeometry_geomfile(filename):
    regex_IRCpoints = "Optimized point #(.*?)Found\..*?"+"-"*60+"\n(.*?)"+"-"*60
    with open(filename, 'r') as f:
        text = f.read()
        findText = re.findall( regex_IRCpoints, text, flags=re.DOTALL )
        geomIRC = []
        for match in findText:
           geomIRC.append([ float(nr) for nr in match[1].split()[2::7]])
        f.close()
    return( geomIRC)

def extract_IRCEnergy_geomfile(filename):
    regex_IRCenergy = "Summary of reaction path following\:.*?"+"-"*60+"\n.*?\n(.*?)"+"-"*60
    with open(filename, 'r') as f:
        text = f.read()
        findText = re.findall( regex_IRCenergy, text, flags=re.DOTALL )
        lines = findText[-1].split("\n")
        Elist = []
        for n,l in zip(range(len(lines)),lines):
            try: 
                if int(l.split()[0]) != n+1:
                   break
            except ValueError:
                break
            Elist.append( float(l.split()[1]) ) 
        f.close()
    return(Elist)

def extract_frequencies_geomfile(filename):
    regex_frequency = "Frequencies -- +(.*)\n"
    with open(filename, 'r') as f:
        text = f.read()
        findText = re.findall( regex_frequency, text )
        freqList = []
        for match in findText:
            freqList += [ float(nr) for nr in match.split() ]
        f.close()
    return(freqList)

