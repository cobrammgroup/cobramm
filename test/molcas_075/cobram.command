!keyword
type=mdv
nsteps=10
tstep=1
surhop=tully
DC=tdc
numrlx=3
basis=STO-3G
qm-type=molcas
qmem=5000MB
geomem=5000MB
rand=0.001
ricd=1
?keyword

!sander 
comment line 
&cntrl 
imin   = 1, 
maxcyc = 0, 
ntb    = 0, 
igb    = 0,
ntr    = 0, 
ibelly = 1, 
cut    = 10 
/
?sander

!molcas
&RASSCF
LUMORB
NACTEL
4 0 0
CHARGE=0
ras1
0
ras2
3
ras3
0
CIROOT
4 4 1

?molcas

