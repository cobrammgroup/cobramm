#!/usr/bin/env python3

import unittest
import os
import re
import importlib

import test_config

# define the list of test_* directories
# list all the subdirectories
listDir = [ name for name in os.listdir(".") if os.path.isdir(os.path.join(".", name))]
# now extract those that match the pattern that corresponds to the programs to test
matchingDir = []
# define those directories that match the "gaussian_*" pattern
if test_config.checkGaussian:
    if test_config.checkPySOC: 
        matchingDir += [ name for name in listDir if re.match(r'.*gaussian\_[0-9]+.*', name)]
    else: 
        matchingDir += [ name for name in listDir if re.match(r'.*gaussian\_[0-9]+.*', name) and int(re.match(r'.*gaussian\_([0-9]+).*', name)[1]) not in (71, 72)]
# add those directories that match the "molcas_*" pattern
if test_config.checkMolcas: matchingDir += [ name for name in listDir if re.match(r'.*molcas\_[0-9]+.*', name)]
# add those directories that match the "molpro_*" pattern
if test_config.checkMolpro: matchingDir += [ name for name in listDir if re.match(r'.*molpro\_[0-9]+.*', name)]

# remove IRC tests if requested
if test_config.skipIRC:
    for irctest in ["gaussian_015", "gaussian_016", "molpro_048", "molpro_053"]:
        if irctest in matchingDir: matchingDir.remove(irctest)

# initialize the test suite
loader = unittest.TestLoader()
suite  = unittest.TestSuite()

# import classes from the gaussian_* directories and add tests to the test suite
for d in sorted(matchingDir):
    try: 
        impCl = importlib.import_module( d )
    except ImportError:
        continue
    suite.addTests( loader.loadTestsFromModule(impCl) )
    
# initialize a runner, pass it your suite and run it
runner = unittest.TextTestRunner(verbosity=3)
result = runner.run(suite)
