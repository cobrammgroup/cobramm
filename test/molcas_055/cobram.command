!keyword
type=freqxgp
nsteps=500
basis=STO-3G
qm-type=molcas
qmem=5000MB
geomem=5000MB
numproc=2
?keyword

!molcas
&scf
&mbpt2
?molcas

