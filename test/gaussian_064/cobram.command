!keyword
type=mdv
numproc=1
qm-type=gauss
qmem=500MB
savwfu=0
savQMlog=1
nsteps=6
tstep=1.0
tsshort=0.5
surhop=tully
ediff=20.0
backhop=off
rand=0.0001
verbose=2
velafterhop=GD
hoptogs=2
gsesth=30.0
numrlx=2
?keyword

!gaussian
#p STO-3G tda(nstates=1,root=1) b3lyp nosym

Comment line

0 1
?gaussian
