!keyword
type=sp
qm-type=gauss
qmem=200MB
?keyword


!sander
comment line
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 10
/
?sander


!gaussian
#p hf sto-3g nosym

Comment line

0 1 
?gaussian


!RATTLE
30 31
31 32
30 32
33 34
34 35
33 35
36 37
37 38
36 38
39 40
40 41
39 41
42 43
43 44
42 44
45 46
46 47
45 47
48 49
49 50
48 50
51 52
52 53
51 53
54 55
55 56
54 56
57 58
58 59
57 59
60 61
61 62
60 62
63 64
64 65
63 65
66 67
67 68
66 68
69 70
70 71
69 71
72 73
73 74
72 74
75 76
76 77
75 77
78 79
79 80
78 80
81 82
82 83
81 83
84 85
85 86
84 86
87 88
88 89
87 89
?RATTLE
