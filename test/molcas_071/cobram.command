!keyword
type=mdv 
nsteps=5
tstep=0.5
surhop=none
qm-type=molcas
qmem=200MB
savwfu=5
numrlx=3
ricd=1
?keyword

!molcas
&RASSCF
LUMORB
NACTEL
4 0 0
CHARGE=0
ras1
0
ras2
3
ras3
0
CIROOT
3 3 1

&caspt2
IMAG=0.2
IPEA=0
MAXITER=200
XMULTISTATE=3 1 2 3
?molcas


