!keyword
type=ci
verbose=2
nsteps=5
basis=STO-3G
qm-type=molcas
qmem=5000MB
geomem=5000MB
numrlx=2
ricd=1
BranchPlane=gmean
?keyword

!sander 
comment line 
&cntrl 
imin   = 1, 
maxcyc = 0, 
ntb    = 0, 
igb    = 0,
ntr    = 0, 
ibelly = 1, 
cut    = 10 
/
?sander

!molcas
&RASSCF
LUMORB
SYMMETRY
1
SPIN
1
NACTEL
4 0 0
INACTIVE
7
ras1
0
ras2
3
ras3
0
CIROOT
4 4 1

&caspt2
IMAG=0.2
IPEA=0
MAXITER=200
MULTISTATE=3 1 2 3
NOMULT
PROP
?molcas

