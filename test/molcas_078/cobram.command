!keyword
type=mdv
nsteps=20
surhop=tully
DC=tdc
numrlx=2
basis=STO-3G
qm-type=molcas
qmem=5000MB
geomem=5000MB
rand=0.0002
isc=diab
nsinglets=2
ntriplets=2
ricd=1
?keyword

!molcas
&RASSCF
LUMORB
NACTEL
4 0 0
CHARGE=0
ras1
0
ras2
3
ras3
0
CIROOT
2 2 1
?molcas

