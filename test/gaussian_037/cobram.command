!keyword
type=mdv 
nsteps=10  
tstep=0.5
surhop=none
qm-type=gauss
qmem=200MB
savQMlog=2
numrlx=3
?keyword

!gaussian
#p td(nstates=2,root=2) sto-3g b3lyp nosym 

Comment line

0 1 
?gaussian

!sander
comment line
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 10
/
?sander

