!keyword
type=optxg
qm-type=molcas
qmem=5000MB
nsteps=5
basis=STO-3G
ricd=1
?keyword

!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9
/
?sander

!molcas
&scf

&mbpt2
?molcas

