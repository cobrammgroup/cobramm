!keyword
type=optxg
qm-type=molcas
qmem=5000MB
nsteps=3
basis=STO-3G
ricd=1
numrlx=2
?keyword

!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9
/
?sander

!molcas
&RASSCF
LUMORB
NACTEL
4 0 0
CHARGE=0
ras1
0
ras2
3
ras3
0
CIROOT
3 3 1

&caspt2
IMAG=0.2
IPEA=0
MAXITER=200
MULTISTATE=3 1 2 3
NOMULT
PROP
?molcas

