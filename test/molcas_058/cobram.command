!keyword
type=freqxgp
nsteps=500
basis=STO-3G
qm-type=molcas
qmem=5000MB
geomem=5000MB
numrlx=2
ricd=1
numproc=2
?keyword

!molcas
&RASSCF
LUMORB
SYMMETRY
1
SPIN
1
NACTEL
4 0 0
CHARGE=0
ras1
0
ras2
3
ras3
0
CIROOT
2 2 1

&caspt2
IMAG=0.2
IPEA=0
MAXITER=200
XMULTISTATE=2 1 2
?molcas

