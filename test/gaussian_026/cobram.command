!keyword
type=mdv 
nsteps=10  
tstep=0.5
surhop=none
qm-type=gauss
qmem=200MB
numrlx=2
?keyword

!gaussian
#p casscf(2,2,nroot=2,stateaverage) sto-3g nosym scf(conver=5)

Comment line

+1 1 
?gaussian

!gaussweights
0.50000000 0.50000000
?gaussweights

