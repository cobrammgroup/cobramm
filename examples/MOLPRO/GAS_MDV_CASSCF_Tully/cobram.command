!keyword
type=mdv qm-type=molpro nsteps=250 nproc=1 qmem=200MW tstep=0.5 tsshort=0.25 surhop=persico ediff=20.0 basis=STO-3G
?keyword

!molpro
{multi;
occ,9;
closed,6;
wf,16,1,0;
state,3
CPMCSCF,GRAD,2.1,record=5100.1;
}
?molpro

