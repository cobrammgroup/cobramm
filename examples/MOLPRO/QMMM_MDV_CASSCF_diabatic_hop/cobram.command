!keyword
type=mdv qm-type=molpro nsteps=1000 qmem=180MW tsteo=0.25 surhop=cirot ediff=30.0
?keyword

!sander
MD Pentadien
 &cntrl
  imin   = 1,
  maxcyc = 0,
  ntb    = 0,
  igb    = 0,
  ntr    = 0,
  ibelly = 1,
  cut    = 9
 /
?sander

!molpro
 basis=STO-3G
 {multi;
 Closed,19;
 Occ,25;
 State,3;
 Weight,1,1,0;
 CPMCSCF,GRAD,2.1;
 }
?molpro

!rattle
39 40
39 41
40 41
42 43
42 44
43 44
?rattle

