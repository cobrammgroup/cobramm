!keyword
type=optxg qm-type=molpro nsteps=100 qmem=10GB basis=STO-3G
?keyword


!sander 
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9,
ntxo   = 1
/
?sander

!molpro
{multi;
occ,40;
closed,38;
wf,78,1,0;
state,3
}

{rs2,mix=3,root=2;
state,3;
OPTION,NSTATI=3;
}
?molpro

