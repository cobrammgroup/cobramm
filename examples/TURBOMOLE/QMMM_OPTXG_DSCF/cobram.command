!keyword
type=optxg qm-type=turbo nsteps=1000 
?keyword

!sander
minimizzazione
&cntrl
imin   = 1,
maxcyc = 2,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 12,
ntxo   = 1
/
?sander

!optxg
#p opt=nomicro
?optxg

