!keyword
type=mdv qm-type=molcas nsteps=500 qmem=1000MB tstep=0.5 tsshort=0.25 surhop=persico ediff=30.0 basis=6-31Gp
?keyword

!sander 
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9
/
?sander 


!molcas
 &RASSCF  &END
Symmetry
 1
Spin
 1
nActEl
 10 0 0
CIroot
 2 2 1
Inactive
50
RAS2
 10
RLXR
 2
LumOrb
End of Input
?molcas

!RATTLE
63 64
63 65
64 65
66 67
66 68
67 68
?RATTLE



