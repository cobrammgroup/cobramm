!keyword
type=optxg qm-type=molcas nsteps=100 qmeme=1000MB stepsize=10 molcasnum=yes ricd=yes basis=STO-3G
?keyword


##########################################################
# comments
##########################################################
# molcasnum=yes		activate Molcas numerical routines
# stepsize=10		maxstep for the Gaussian optimizer
##########################################################


!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9
/
?sander

!molcas
&RASSCF
LUMORB
SYMMETRY
1
NACTEL
2 0 0 
INACTIVE
38
ras2
2
CIROOT
3 3 1
MAXORB
1
RLXRoot
3
End of Input

&CASPT2
multistate
3 1 2 3
PROP
NOMULT

&ALASKA
NUME
DELTA
0.001889

?molcas

