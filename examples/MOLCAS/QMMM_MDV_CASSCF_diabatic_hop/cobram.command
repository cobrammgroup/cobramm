!keyword
type=mdv qm-type=molcas nsteps=500 tstep=0.5 surhop=cirot ediff=30.0 cirotbackhop=5 basis=6-31Gp
?keyword


##########################################################
# comment
##########################################################
# cirotbackhop=5	allow up-hops, wait 5 steps after 
                        down hop before testing for up hop
##########################################################

!sander 
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9,
ntxo   = 1
/
?sander 


!molcas
 &RASSCF  &END
Symmetry
 1
Spin
 1
nActEl
4 0 0
CIroot
 3 3 1
Inactive
50
RAS2
4 
RLXR
 2
LumOrb
End of Input
?molcas

!RATTLE
63 64
63 65
64 65
66 67
66 68
67 68
?RATTLE



