!keyword
type=irc qm-type=molcas ircpoint=5 ircstepsize=10 qmem=1000MB basis=STO-3G sstep=-1 nsteps=100
?keyword

!molcas
&RASSCF
LUMORB
SYMMETRY
1
NACTEL
4 0 0
INACTIVE
6
ras2
3
CIROOT
3 3 1
MAXORB
1
RLXRoot
2
End of Input
?molcas

!singlepoint
&RASSCF
LUMORB
SYMMETRY
1
NACTEL
4 0 0
INACTIVE
6
ras2
3
CIROOT
3 3 1
MAXORB
1

&CASPT2
multistate
3 1 2 3
NOMULT
?singlepoint
