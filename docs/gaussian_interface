The cobram Gaussian09 QM/MM interface works now with analytical charge
gradients using the electrostatic potential. The interface is invoked
with

 51 1        ## QM program to use, 1=Gaussian

Further commands control the number of Processors and memory:

  7 4        ## Use 4 Processors in Gaussian
 53 500MB    ## Memory in megabytes (don't forget the 'MB', otherwise it's WORDS)

The input goes into the !gaussian section of cobram command. HF, MP2,
DFT, TD-DFT-methods have been tested and should work out of the
box. Only method, basis set and chareg/multiplicity have to be
specified in the !gaussian section. Charge and efield options are
automatically added by cobram. An example input:

!gaussian 
#P MP2/6-31G** SCF=Tight NOSYM

0  1
?gaussian

A few notes:

Gaussian 09 shows some new and unexpected behaviour. By default it
expects all input charges to be in the standard orientation of your
molecule. This is however rarely the case. Therefore, the keyword
"NOSYM" has to be specified when doing QM/MM computations, otherwise
the charge locations are simply wrong and the results are useless. 

The program will actually stop working when it does not find the
"NOSYM" keyword in the !gaussian field of the command file.  

This can be overruled using command 192 1, but is not recommended
unless you REALLY work with symmetry and are sure to be ALWAYS in the
standard orientation with your molecule.

For geometry optimizations using the Gaussian interface the keyword
"SCF=Tight" should be specified (see above), otherwise the criterion
for SCF energy optimization is only 1.0e-5 instead of the usual
1.0e-08, leading to poor gradients.

