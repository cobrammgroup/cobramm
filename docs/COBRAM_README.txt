
#############################################################################
##### step by step COBRAMM tutorial for the  RNA adenine dimer example ######
#############################################################################



####################################################
#### Preparing RNA adenine dimer input files #######
####################################################


1) build nuc.in file in order to have a RNA adenine dimer
2) run nucgen to obtain the nuc.pdb file:
> nucgen -O -i nuc.in -o nuc.out -d $AMBERHOME/dat/leap/parm/nucgen.dat -p nuc.pdb

3) modify the nuc.pdb (to nuc2.pdb) in order to get rid of wrongly assigned Hydrogens and Uracil residues:
> grep -v H5T nuc.pdb | grep -v H3T | grep -v U3 | grep -v U5 > nuc2.pdb 

4) open xleap as (Note: for RNA use leaprc.rna.ff99 instead of leaprc.ff99):
> xleap -s -f leaprc.rna.ff99

5) build the .top, .crd and .pdb files via xleap commands:
> model = loadpdb nuc2.pdb
> edit model (to visualize and double check your model)
> addions model Na+ 0
> solvatebox model TIP3PBOX 10.0 (write down values of the "Total vdw box size")
> saveamberpam model AA.top AA.crd
> savepdb model AA.pdb 

6) run minimization and equilibration MD simulations by using the Amber Tutorial. This will  provide several structures to be analyzed with COBRAMM at the QM-MM level.

################################################################################################### 
#### Preparing COBRAMM input files: real_layers.xyz real.top model-H.top cobram.command ###########
################################################################################################### 

Let's see now how to build COBRAMM inputs by using a initial pdb structure (AA_init.pdb)

7) open the AA_init.pdb file with GaussView (i.e. gv3 AA_init.pdb) and define High, Medium and Low levels for the QM-MM calculations. Note: remember not to add Hydrogens atom with GaussView.

8) Following the COBRAMM tutorial save the AA_init.com file by doing:
- Open the "Select Layer" menu in GV.
- select one atom in the QM (i.e. High level) region
- Expand selection using "Bond" until you select the entire dinucleotide
- Set Layer to Medium and Apply
- select N atom at the periphery of the nucleotide base and expand selection until you reach (all or almost all) the QM (High level) region.
- Set layer to High and Apply (move some extra atom to the Medium layer if necessary) 
- Add a selection of water molecules H-bonded the base to the Medium Layer 
- close the "Select Layer" window by typing the "Close" button
- Open the "Calculate Menu" and choose "Gaussian"
- Go to Method and select the "Multilayer ONIOM model" option and click on Retain
- Save the molecule as .com file and "Write Cartesian" coordinates 

9) Extract fomr the AA_init.com file only the lines with XYZ coordinates and save it as real_layers.xyz (THE FIRST INPUT FOR COBRAMM IS READY!!!)

10) Extract the "model" (the High layer of real_layers.xyz) from the real_layers.xyz by typing:
> grep "[0-9] H" real_layers.xyz > model.xyz

Note: double check that grep worked fine extracting the high layer.

11)  Convert the model.xyz into model.com :
> echo '# hf opt' >> temp ; echo '            ' >> temp ; echo '  Title ' >> temp ; echo '            ' >> temp; echo '0 1' >> temp ; cat temp model.xyz > model.com ; rm temp 

12) Open model.com with GV and add Hydrogens (i.e. the atom links) to cap the periphery of the "model" region and create the new file model-H.com.
Note: VERY IMPORTANT- Add Hydrogens following strictly the order of the "model" atoms, e.g. add the first H to the first (chemically unsaturated) peripheral atom. To visulalize the atom numbering right-click on the screen and select "Lables"
 
13) Convert the model-H.com into model-H.mol2: to do that you need to save the xyz coordinates of the model-H.com file in a .xyz format file (containing the numer of atoms a title line and then the xyz coordinates), model-H.xyz, and then open the model-H.xyz file with molden (type just: > molden model-H.xyz). On the main Mebnu window of molden select "write" and then "Mol2". Save the mol2 file as model-H.mol2. 

14) Now you need to assign amber atom types and initial partial charges to the model-H system. Thus, let's run antechamber in the following way:
> antechamber -fi mol2 -i model-H.mol2 -fo mol2 -o model-H_out.mol2 -at amber -nc 0 -c bcc

This will assign AMBER atom types (-at) and AM1-BCC charges (-c). Note: Here the model-H system has total Zero charge, i.e. "-nc 0".

15) In the file model-H_out.mol2, Antechamber assigned AMBER atom types to the H link atoms but for those atoms we want to assign specific parameters (i.e. zeros parameters) so you need to change the atom types of the link atoms in order to have 'non-Amber' atom types: Modify the atom types of the H link atoms to 'AL'

16) You need to verify that the atom type of the "model" system assigned in model-H_out.mol2 are the same as those assigned in the real.top. In this specific example the ester Oxygen atoms in the Adenine residues have atom types 'OH' in model-H_out.mol2 while they have atom type 'OS' in the standard Adenine residues. So you need to change the 'OH' types into the 'OS' types and overwrite the model-H_out.mol2 file. To visually check the atom types in the standard residues and the model-H_out.mol2 you can always use xleap.

17) In order to build the topology file model-H.top you need to have the parameters for the non-Amber atom types now assigned to the atom links 'AL'. You can use the 'parmchk' command to create the parameter file:
> parmchk -i model-H_out.mol2 -f mol2 -p $AMBERHOME/dat/leap/parm/gaff_parm99m.dat -o model-H_gaff.parm

18) Now you can build the model-H.top file by using tleap:
> tleap -f leap.inp

where the leap.inp file is a text file containing the following lines:

source leaprc.ff99
source leaprc.gaff
loadamberparams model-H_gaff.parm
model = loadmol2 model-H_out.mol2
check model
saveamberparm model model-H.top model-H.crd
savepdb model model-H.pdb
quit

THE SECOND INPUT FOR COBRAMM IS READY!!!

19) now you need to build to prepare the real.top file, i.e. the topology file for the entire system. Due to the electrostatic embedding scheme implemented in COBRAMM, the topology file of the real system must contains a total charge for the "model" system equal to that of "model-H" (i.e. in this example equal to Zero). This is because COBRAMM redistributes the QM charges of the H atom links into the "model" atoms before using these QM charges for the calculation of the MM energy of the real system. In other words, COBRAMM updates the real.top file every optimization step with the constrain of maintaining the total charge of the "model" part constant, i.e. equal to that set for the QM calculation of the "model-H". However, in general, the "model" part is contained in one (or more than one) reisude(s) of the real system, e.g. the atoms of the Adenine base in the Adenine residue. Usually the total charge of the "model" part assigned by the ForceField (e.g. the sum of the partial charges of the atoms of the Adenine base) is not equal to that assigned to "model-H" in the QM calculation (which is always an integer number). For this reason, it is necessary to force the "model" part to have the integer charge of the "model-H" and, at the same time, to maintain the charge of the residue containing the "model" part (e.g. the Adenine residue charge has to be maintained even if the charge of the atome in the Adenine base is forced to be Zero). In order to meet this requirement it is necessary to redistribute the excess of charge derived from the constraint of the integer charge on the "model" part to the remaining MM atoms of the residue(s) (e.g. redistribute the excess of charge obtained by setting the charge of the Adenine base to zero into the remaining atoms (sugar + phospate) of the Adenine residue.A common way of redistributing the excess of charge of the "model" part(Qm) is to distribute this charge over the MM atoms in an heterogeneous way, by weigthing the charge correction on each MM atom(Qcori) according to the absolute value of its partial charge(Qi). Therefore, in order to avoid overpolarization of atoms that have large partial charges, it is desiderable to redestribute the excess of charge only over the atoms having small partial charges (i.e. avoid to redistribute charges over the Phosphate groups in RNA-DNA bases). The charge correction on the i-th atom (Qcori) is then calculated as: Qcori=|Qi| * Qm/sum(|Qi|), and the corrected atomic charge (Qfi) is simply equal to: Qfi= Qi + Qcori

20) Considering the necessary changes of the partial charges in the "model" part, it is necessary to create non-standard libraries for the residues of the "real" system that contain atoms of the "model" part (i.e. the Adenine residues whose bases are trated at the QM level need to have different charges from the standard residues and then require new non-standard libraries). In this example we simply need to modify the standard RA3.lib and RA5.lib libraries by changing the charges according to (19). The RA3.lib file can be easily obtained by typing:
> tleap -f leaprc.rna.ff99
> saveoff RA3 RA3.lib


21) Create the AD3.lib and AD5.lib files

22) Now you are ready to create the real.top by using tleap

> tleap -f leaprc.rna.ff99
> loadoff AD3.lib
> loadoff AD5.lib
> model = loadpdb real.pdb
> saveamberparm model real.top real.crd
> savepdb model real_check.pdb


### ENJOY !!! ### 

#### Ivan Rivalta, Irene Conti #####
#### June 2012 #####

   










